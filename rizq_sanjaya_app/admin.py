from django.contrib import admin
from .models import *

"""ACCOUNT USER"""
class UserAdmin(admin.ModelAdmin):
    list_display = ("email", "username", "role", "is_active", "is_verified", "is_staff")
admin.site.register(Account, UserAdmin)

"""CATEGORY"""
class CategoryAdmin(admin.ModelAdmin):
    list_display = ("nama",)

admin.site.register(Category, CategoryAdmin)

"""NEWS"""
class NewsAdmin(admin.ModelAdmin):
    list_display = ('title', 'thumbnail', 'content', 'slug', 'category', 'tags', 'created_by', 'last_updated_by',)

# admin.site.register(News, NewsAdmin)


"""CAROUSEL"""
class CarouselAdmin(admin.ModelAdmin):
    list_display = ('judul', 'subjudul', 'foto', 'link',)

admin.site.register(Carousel, CarouselAdmin)
