from django.apps import AppConfig


class CompanyProfileAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rizq_sanjaya_app'
