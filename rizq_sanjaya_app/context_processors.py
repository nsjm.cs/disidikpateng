import os
import numpy

from datetime import datetime
from .models.profile import AppSetting
from .helpers import get_list_berita
from .forms import *
from django.db.models import Q, Count

def global_variables(request):

    profilSKPD_home = ProfilSKPD.objects.exclude(typexs='Misi').values('typexs').annotate(count=Count('typexs')).order_by('typexs')
    menuinformasiterpadu = InfoTerpadu.objects.values('typexs').distinct()
    menulayanan = LayananDinas.objects.values('typexs').distinct()
    alert_pengaduan = PelayananPublik.objects.filter(status_pengaduan__isnull=True).count()
    # ADD JOEL ================================
    menu_PPID = modul_PPID.objects.filter(status='Publish').values('title','slug').order_by('id')
    # setting app - pak bas
    app_setting = AppSetting.objects.all()
    nama_website = alamat = alamat_link = embed_maps = telepon = telepon_text  = whatsapp = whatsapp_text = email = 'Belum Disetting'
    meta_keywords = meta_author = meta_property_title = meta_property_description = 'Belum Disetting'
    link_facebook = link_twitter = link_youtube = link_instagram = link_tiktok = 'Belum Disetting'
    is_carousel = False

    for appx in app_setting:
        if appx.nama == 'nama_website':
            nama_website = appx.keterangan
        elif appx.nama == 'alamat':
            alamat = appx.keterangan
        elif appx.nama == 'alamat_link':
            alamat_link = appx.keterangan
        elif appx.nama == 'embed_maps':
            embed_maps = appx.keterangan
        elif appx.nama == 'telepon':
            telepon = appx.keterangan
        elif appx.nama == 'telepon_text':
            telepon_text = appx.keterangan
        elif appx.nama == 'whatsapp':
            whatsapp = appx.keterangan
        elif appx.nama == 'whatsapp_text':
            whatsapp_text = appx.keterangan
        elif appx.nama == 'email':
            email = appx.keterangan
        elif appx.nama == 'meta_keywords':
            meta_keywords = appx.keterangan
        elif appx.nama == 'meta_author':
            meta_author = appx.keterangan
        elif appx.nama == 'meta_property_title':
            meta_property_title = appx.keterangan
        elif appx.nama == 'meta_property_description':
            meta_property_description = appx.keterangan
        elif appx.nama == 'link_facebook':
            link_facebook = appx.keterangan
        elif appx.nama == 'link_instagram':
            link_instagram = appx.keterangan
        elif appx.nama == 'link_youtube':
            link_youtube = appx.keterangan
        elif appx.nama == 'link_twitter':
            link_twitter = appx.keterangan
        elif appx.nama == 'link_tiktok':
            link_tiktok = appx.keterangan
        elif appx.nama == 'is_carousel':
            is_carousel = appx.keterangan


    return {
        'home_profilSKPD':profilSKPD_home,
        'menuinformasiterpadu' : menuinformasiterpadu,
        'menulayanan' : menulayanan,
        'menu_PPID': menu_PPID,

        'nama_website': nama_website,
        'alamat': alamat,
        'alamat_link': alamat_link,
        'embed_maps': embed_maps,
        'telepon': telepon,
        'telepon_text': telepon_text,
        'whatsapp': whatsapp,
        'whatsapp_text': whatsapp_text,
        'email': email,
        'meta_keywords': meta_keywords,
        'meta_author': meta_author,
        'meta_property_title': meta_property_title,
        'meta_property_description': meta_property_description,
        'link_facebook': link_facebook,
        'link_youtube': link_youtube,
        'link_twitter': link_twitter,
        'link_instagram': link_instagram,
        'link_tiktok': link_tiktok,
        'is_carousel': is_carousel,
        'alert_pengaduan': alert_pengaduan,
    }


    
    
    
