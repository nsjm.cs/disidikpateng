import os
import numpy
from datetime import datetime
from .forms import *
from django.db import connections
from .helpers import dictfetchall
from datetime import datetime

def global_variables(request):
	# ## CREATE TAHAPAN SAAT AWAL LOAD HALAMAN BERANDA FONTEND =======================================
	set_tahapanPPDB(request)
	# ## GET DATA SETTING SAAT LOAD HALAMAN BERANDA FONTEND ==========================================
	cinta = {}
	jml_sma = 0
	jml_smk = 0
	jml_slb = 0
	tot_jml = 0
	dtx = ppdb_AppSetting.objects.all()
	for x in dtx:
		cinta[x.nama] = x.keterangan

	# ## GET DATA PERSEN INFOGRAFIS FONTEND ==========================================
		if x.nama == 'Info_tingkat_sma':
			jml_sma = int(x.keterangan)
		if x.nama == 'Info_tingkat_smk':
			jml_smk = int(x.keterangan)
		if x.nama == 'Info_tingkat_slb':
			jml_slb = int(x.keterangan)

	tot_jml = jml_sma+jml_smk+jml_slb
	try:
		persen_sma = round((jml_sma/tot_jml)*100)
	except Exception as e:
		persen_sma = 0 

	try:
		persen_smk = round((jml_smk/tot_jml)*100)
	except Exception as e:
		persen_smk = 0

	try:
		persen_slb = round((jml_slb/tot_jml)*100)
	except Exception as e:
		persen_slb = 0 


	return { 'ppdb_conf': cinta, 'persen_sma':persen_sma, 'persen_smk':persen_smk, 'persen_slb':persen_slb}


def set_tahapanPPDB(request):
	dtx = ppdb_konten.objects.filter(typexs='tahapan')
	arrDT = [{'title':'Yuk Baca ketentuan PPDB!', 'description':'Belum Disetting', 'typexs':'tahapan', 'if_link':'-', 'status':'Publish'},
		{'title':'Verifikasi Rapor', 'description':'Belum Disetting', 'typexs':'tahapan', 'if_link':'-', 'status':'Publish'},
		{'title':'Pengambilan PIN', 'description':'Belum Disetting', 'typexs':'tahapan', 'if_link':'-', 'status':'Publish'},
		{'title':'Tahapan PPDB Papua Tengah', 'description':'Belum Disetting', 'typexs':'tahapan', 'if_link':'-', 'status':'Publish'}]

	no = 0
	if len(dtx) < 4:
		for z in arrDT:
			with connections['default'].cursor() as cursor:
				cursor.execute("""SELECT COUNT(title) as jml FROM rizq_sanjaya_app_ppdb_konten WHERE title = %s""",[z['title']])
				jml_dt = dictfetchall(cursor)[0]

			if jml_dt['jml'] == 0:
				form = ppdb_TahapanForm(data=arrDT[no])
				if form.is_valid():
					new_dt = form.save(commit=False)
					new_dt.subtitle = 'tahapan_'+str(no)
					new_dt.save()
					form.save_m2m()

			no = no+1


def get_list_ppdb_berita(limit):
    listdata = ppdb_News.objects.filter(deleted_at=None).order_by('-created_at')[:limit]
    total = int(len(listdata))
    child = []
    content = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien.</p>'
    if len(listdata) == 0:
        for x in range(limit):
            child.append({
                'title':'Berita PPDB Dinas Pendidikan Provinsi Papua Tengah', 
                'thumbnail':{'url':'/media/profile/images/ppdb/artikel/no-image.jpg'}, 
                'created_at':datetime.now(), 
                'slug':'lorem-ipsum', 'seen':0,
                'created_by':{'first_name':'Admin', 'last_name':'Website'},
                'content':content,
            })
        dt_news = child

    else:
        sisay = []
        hasilx = []
        imagx = {}
        thmbx = {}
        thumbnil = {}
        sisax = 0

        for i in range(0,total,limit):
            sisax = abs((i+limit) - total)

        for y in range(sisax):
            thmbx = {'url':'/media/profile/images/ppdb/artikel/no-image.jpg'}
            title_xx = 'Berita PPDB Dinas Pendidikan Provinsi Papua Tengah'
            sisay.append({'title':title_xx, 'thumbnail':thmbx, 'created_at':datetime.now(), 'slug':'lorem-ipsum', 
                'seen':0, 'created_by':{'first_name':'Admin', 'last_name':'Website'},'content':content,})

        for z in listdata:
            thumbnil = {'url':z.thumbnail.url.replace('artikel/', 'artikel/')} 
            hasilx.append({'title':z.title, 'thumbnail':thumbnil, 'created_at':z.created_at, 'slug':z.slug, 
                'seen':z.seen, 'created_by':z.created_by, 'content':z.content,})

        dt_news = numpy.append(numpy.array(hasilx),numpy.array(sisay))

    return dt_news




	
	
	
