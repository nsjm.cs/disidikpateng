from django import forms
from ..models import *
from PIL import Image
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from captcha.fields import CaptchaField

	

###################FORM PROFIL SKPD###################
class Profilskpd(forms.ModelForm):
	class Meta:
		model = ProfilSKPD
		fields = ('title','typexs','description')

class ProfilskpdForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput(), required=False)
	y = forms.FloatField(widget=forms.HiddenInput(), required=False)
	width = forms.FloatField(widget=forms.HiddenInput(), required=False)
	height = forms.FloatField(widget=forms.HiddenInput(), required=False)

	class Meta:
		model = ProfilSKPD
		fields = ('foto', 'x', 'y', 'width', 'height',)

	def save(self):
		list_video = ['video/x-flv', 'video/MP2T', 'video/3gpp', 'video/quicktime', 'video/x-msvideo', 'video/x-ms-wmv', 'video/mp4']
		dt_img = super(ProfilskpdForm, self).save()
		
		if self.cleaned_data.get('foto').content_type not in list_video:
			x = self.cleaned_data.get('x')
			y = self.cleaned_data.get('y')
			w = self.cleaned_data.get('width')
			h = self.cleaned_data.get('height')
			
			image = Image.open(dt_img.foto.path)
			cropped_image = image.crop((x, y, w + x, h + y))
			cropped_image.save(f"{settings.MEDIA_ROOT}{dt_img.foto.name}")
			
		
		return dt_img

###################FORM INFO TERPADU###################
class InfoTerpaduForm(forms.ModelForm):
	class Meta:
		model = InfoTerpadu
		fields = ('title','start_event_date','end_event_date','typexs','description')

class InfoTerpaduImg(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())

	class Meta:
		model = InfoTerpadu
		fields = ('foto', 'x', 'y', 'width', 'height',)

	def save(self):
		dt_img = super(InfoTerpaduImg, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(dt_img.foto)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{dt_img.foto.name}")

		return dt_img

###################FORM LAYANAN DINAS###################
class LayananDinasForm(forms.ModelForm):
	class Meta:
		model = LayananDinas
		fields = ('title','typexs','description',)


class LayananDinasImg(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())

	class Meta:
		model = LayananDinas
		fields = ('foto', 'x', 'y', 'width', 'height',)

	def save(self):
		dt_img = super(LayananDinasImg, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(dt_img.foto)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{dt_img.foto.name}")

		return dt_img

###################FORM AKSESBILITAS LINK###############
class AksesbilitasLink(forms.ModelForm):
	class Meta:
		model = Aksesbilitas
		fields = ('title','link',)

###################FORM KATEGORI#########################
class CategoryForm(forms.ModelForm):
	class Meta:
		model = Category
		fields = ('id', 'nama','typexs',)

###################FORM BERITA#########################
class NewsForm(forms.ModelForm):
	class Meta:
		model = News
		fields = ('title', 'content', 'category', 'tags',)

class RnWForm(forms.ModelForm):
	class Meta:
		model = RnW
		fields = ('jenis', 'deskripsi',)

###################FORM THUMBNAIL BERITA#########################
class NewsThumbnailForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())

	class Meta:
		model = News
		fields = ('thumbnail', 'x', 'y', 'width', 'height',)

	def save(self):
		news = super(NewsThumbnailForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(news.thumbnail)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{news.thumbnail.name}")

		return news

# class NewsThumbnailForm(forms.ModelForm):
# 	x = forms.FloatField(widget=forms.HiddenInput())
# 	y = forms.FloatField(widget=forms.HiddenInput())
# 	width = forms.FloatField(widget=forms.HiddenInput())
# 	height = forms.FloatField(widget=forms.HiddenInput())

# 	class Meta:
# 		model = News
# 		fields = ('thumbnail', 'x', 'y', 'width', 'height',)

# 	def save(self):
# 		news = super(NewsThumbnailForm, self).save()
# 		x = self.cleaned_data.get('x')
# 		y = self.cleaned_data.get('y')
# 		w = self.cleaned_data.get('width')
# 		h = self.cleaned_data.get('height')

# 		image = Image.open(news.thumbnail)
# 		image.save(f"{settings.MEDIA_ROOT}{news.thumbnail.name}")

# 		## SAVE THUMBNAIL
# 		# image = Image.open(news.thumbnail)
# 		# cropped_image = image.crop((x, y, w + x, h + y))
# 		# cropped_image.save(f"{settings.MEDIA_ROOT}{news.thumbnail.name}")
# 		lokasi = f"{news.thumbnail.name}"
# 		hassss = lokasi.replace('artikel/', 'artikel/thumb/')

# 		tumbnil = image.crop((x, y, w + x, h + y))
# 		tumbnis = tumbnil.resize((w, h), Image.ANTIALIAS)
# 		tumbnis.save(f"{settings.MEDIA_ROOT}"+hassss)

# 		return news


class NewsFormTanpaFile(forms.ModelForm):
	class Meta:
		model = News
		fields = ('title', 'content', 'category', 'tags',)

class GalleryForm(forms.ModelForm):
	class Meta:
		model = Gallery
		fields = ('title', 'keterangan', 'video', 'tags',)

class GalleryFotoForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())
	
	class Meta:
		model = Gallery
		fields = ('foto',)

	def save(self):
		gallery = super(GalleryFotoForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(gallery.foto)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{gallery.foto.name}")

		return gallery

class AvatarForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())

	class Meta:
		model = Account
		fields = ('avatar', 'x', 'y', 'width', 'height',)

	def save(self):
		account = super(AvatarForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(account.avatar)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{account.avatar.name}")

		return account

class AccountForm(forms.ModelForm):
	class Meta:
		model = Account
		fields = ('first_name', 'last_name', 'phone', 'date_of_birth')

class RoleForm(forms.ModelForm):
	class Meta:
		model = Account
		fields = ('role',)

class CustomUserCreationForm(UserCreationForm):
	class Meta:
		model = Account
		fields = ('email', 'username', 'first_name', 'last_name', 'role', 'phone', 'date_of_birth',)

class RunningTextForm(forms.ModelForm):
	class Meta:
		model = RunningText
		fields = ('text',)

class LinkForm(forms.ModelForm):
	class Meta:
		model = Link
		fields = ('nama', 'link')

class AppSettingForm(forms.ModelForm):
	class Meta:
		model = AppSetting
		fields = ('nama', 'keterangan')

class CarouselForm(forms.ModelForm):
	class Meta:
		model = Carousel
		fields = ('judul', 'subjudul')

class CarouselFotoForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())
	
	class Meta:
		model = Carousel
		fields = ('foto',)

	def save(self):
		carousel = super(CarouselFotoForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(carousel.foto)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{carousel.foto.name}")

		return carousel

class RunningLinkForm(forms.ModelForm):
	class Meta:
		model = RunningLink
		fields = ('nama', 'link')

class RunningLinkFotoForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())
	
	class Meta:
		model = RunningLink
		fields = ('logo',)

	def save(self):
		running_link = super(RunningLinkFotoForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(running_link.logo)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{running_link.logo.name}")

		return running_link

class MyCaptcha(forms.Form):
   captcha=CaptchaField()

class AboutMeForm(forms.ModelForm):
	class Meta:
		model = AboutMe
		fields = ('judul', 'deskripsi', 'youtube', 'jenis', 'section',) 

class LayananForm(forms.ModelForm):
	class Meta:
		model = Layanan
		fields = ('judul', 'keterangan', 'icon', 'slug',)

class ProductForm(forms.ModelForm):
	class Meta:
		model = Product
		fields = ('product_name', 'category', 'best_seller', 'description', 'tags', 'harga')

class ProductDiskonForm(forms.ModelForm):
    diskon = forms.CharField(required=False)
    persen_diskon = forms.CharField(required=False)

    class Meta:
        model = Product
        fields = ('diskon', 'persen_diskon')


class ProductImagesForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())
	
	class Meta:
		model = Product
		fields = ('images', 'x', 'y', 'width', 'height',)

	def save(self):
		product = super(ProductImagesForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(product.images)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{product.images.name}")

		return product


"""PPID DISDIK JOEL =================================================================================="""
class PPIDForm(forms.ModelForm):
	class Meta:
		model = modul_PPID
		fields = ('title', 'description', 'tags')


# ### ============================================================================================================
# ### JOEL PPDB ==================================================================================================
# ### ============================================================================================================

class ppdb_BerandaForm(forms.ModelForm):
	class Meta:
		model = ppdb_konten
		fields = ('title', 'subtitle', 'description', 'typexs', 'status')

class ppdb_KontenImg(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())

	class Meta:
		model = ppdb_konten
		fields = ('foto', 'x', 'y', 'width', 'height',)

	def save(self):
		dt_img = super(ppdb_KontenImg, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(dt_img.foto)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{dt_img.foto.name}")

		return dt_img

class ppdb_TahapanForm(forms.ModelForm):
	class Meta:
		model = ppdb_konten
		fields = ('title', 'description', 'typexs', 'status', 'if_link')

class ppdb_AppSettingForm(forms.ModelForm):
	class Meta:
		model = ppdb_AppSetting
		fields = ('nama', 'keterangan')

class ppdb_NewsForm(forms.ModelForm):
	class Meta:
		model = ppdb_News
		fields = ('title', 'content', 'tags',)

class ppdb_NewsThumb(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())

	class Meta:
		model = ppdb_News
		fields = ('thumbnail', 'x', 'y', 'width', 'height',)

	def save(self):
		news = super(ppdb_NewsThumb, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(news.thumbnail)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{news.thumbnail.name}")

		return news

		
class katPengaduanForm(forms.ModelForm):
	class Meta:
		model = katPengaduan
		fields = ('nama_kategori', 'jenis_kategori')
