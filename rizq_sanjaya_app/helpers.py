import os
import numpy
import pprint
import re


from .models import LogVisitor
import urllib.request
import xml.etree.ElementTree as ET
from django.db import connections
from itertools import islice
from django.conf import settings
from .models.profile import News, Product
from datetime import datetime

def contains_html(value):
    return bool(re.search(r'<.*?>', value))

def type_posting():
    array_s = [
        {"val":"Sejarah", "nama":"Sejarah"},
        {"val":"Visi", "nama":"Visi"},
        {"val":"Misi", "nama":"Misi"},
        {"val":"Tupoksi", "nama":"Tupoksi"},
        {"val":"Tempat Kedudukan", "nama":"Tempat Kedudukan"},
        {"val":"Struktur Organisasi", "nama":"Struktur organisasi"},
        {"val":"Gambaran SKPD", "nama":"Gambaran SKPD"},
        {"val":"Profil Pejabat", "nama":"Profil Pejabat"},
        {"val":"Sambutan Kepala Dinas", "nama":"Sambutan Kepala Dinas"},
    ]

    return array_s

def jenis_layanan():
    array_l = [
        {"val":"", "nama":"Pilih Jenis Layanan"},
        {"val":"Standar Layanan", "nama":"Standar Layanan"},
        {"val":"Pengaduan Publik", "nama":"Pengaduan Publik"},
        {"val":"Layanan Akses Informasi Publik", "nama":"Layanan Akses Informasi Publik"},
        {"val":"Layanan Permohonan Informasi", "nama":"Layanan Permohonan Informasi"},
    ]

    return array_l


def get_user_info(request):
    device_type = browser_type = browser_version = os_type = os_version = ip = '-'

    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    # try:
    #     ip = requests.get("https://api.ipify.org/").text
    # except Exception as e:
    #     pass

    if request.user_agent.is_mobile:
        device_type = "Mobile"
    if request.user_agent.is_tablet:
        device_type = "Tablet"
    if request.user_agent.is_pc:
        device_type = "PC"
    browser_type = request.user_agent.browser.family
    browser_version = request.user_agent.browser.version_string
    os_type = request.user_agent.os.family
    os_version = request.user_agent.os.version_string

    return {
        'ip' : ip,
        'device_type' : device_type,
        'browser_type' : browser_type,
        'browser_version' : browser_version,
        'os_type' : os_type,
        'os_version' : os_version,
    }

def insert_log_visitor(request):
    user_info = get_user_info(request)
    log = LogVisitor()
    log.ip = user_info['ip']
    log.device_type = user_info['device_type']
    log.browser_type = user_info['browser_type']
    log.browser_version = user_info['browser_version']
    log.os_type = user_info['os_type']
    log.os_version = user_info['os_version']
    log.save()

def dictfetchall(cursor):
    # Return all rows from a cursor as a dict
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def querytags(jenis):
    with connections['default'].cursor() as cursor:
        cursor.execute("""
            SELECT COUNT(a.tag_id) as jml, c.name, c.slug, b.model FROM taggit_taggeditem a
                LEFT JOIN django_content_type b ON a.content_type_id = b.id
                LEFT JOIN taggit_tag c ON a.tag_id = c.id
            WHERE b.model = %s
            GROUP BY a.tag_id, c.name, c.slug, b.model
            ORDER BY c.name;""",[jenis])
        dt_tags = dictfetchall(cursor)
    return dt_tags

def get_category(typexs):
    
    if type(typexs) is str:
        with connections['default'].cursor() as cursor:
            cursor.execute("""
                SELECT * FROM rizq_sanjaya_app_category
                WHERE typexs = %s ORDER BY id;""",[typexs])
            hasile = dictfetchall(cursor)
    else:
        filter_ = ','.join(f"'{w}'" for w in typexs)
        in_arg = f'({filter_})'

        with connections['default'].cursor() as cursor:
            cursor.execute('''
                SELECT * FROM rizq_sanjaya_app_category
                WHERE typexs in {in_arg} ORDER BY id;'''.format(in_arg = in_arg))
            # print(cursor.query)
            hasile = dictfetchall(cursor)
    return hasile


def xmlreader(urlxml):
    files = urllib.request.urlopen(urlxml)
    tree = ET.parse(files)
    datax = tree.getroot()

    return datax

def read_boxicons_css():
    array = []
    fh = open(settings.STATICFILES_DIRS[0]+'/profile/front/vendor/boxicons/css/boxicons.css',"r")

    with fh as f:
        for line in islice(f, 524, None):
            if "bx" in line: 
                array.append(line.split(':')[0].split(".")[1])
    return array

def read_fontAwesome_css():
    array = []
    fh = open(settings.STATICFILES_DIRS[0]+'/profile/front/vendor/fontawesome-free/css/fontawesome.css',"r")

    with fh as f:
        for line in islice(f, 520, None):
            if ".fa-" in line: 
                if ".fa-sr" not in line: 
                    array.append(line.split(':')[0].split(".")[1])
    return array[:-2]

def read_icon(section):
    if section == 'about' or section == 'why-us':
        hasil = read_boxicons_css()
    else:
        hasil = read_fontAwesome_css()

    return hasil

def set_categori_galeri(categori, objectid, aksi):
    with connections['default'].cursor() as cursor:
        cursor.execute("SELECT id FROM django_content_type WHERE model = 'gallery'")
        cont_tpy_id = cursor.fetchone()[0]

    if aksi.lower() == 'edit':
        with connections['default'].cursor() as cursor:
            cursor.execute("DELETE FROM rizq_sanjaya_app_categoryitem WHERE object_id = %s ",[objectid])

    for x in categori:
        with connections['default'].cursor() as cursor:
            cursor.execute("INSERT INTO rizq_sanjaya_app_categoryitem (object_id, content_type_id, categori_id) "\
                "VALUES (%s,%s,%s)",[objectid, cont_tpy_id, int(x)])

def get_list_berita(limit):
    listdata = News.objects.filter(deleted_at=None).order_by('-created_at')[:limit]
    total = int(len(listdata))
    child = []
    content = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien.</p>'
    if len(listdata) == 0:
        for x in range(limit):
            child.append({
                'title':'Berita Dinas Pendidikan Provinsi Papua Tengah', 
                'thumbnail':{'url':'/media/profile/images/artikel/thumbnail-berita-kecil.png'}, 
                'created_at':datetime.now(), 
                'slug':'lorem-ipsum', 'seen':0,
                'created_by':{'first_name':'Admin', 'last_name':'Website'},
                'content':content,
            })
        dt_news = child

    else:
        sisay = []
        hasilx = []
        imagx = {}
        thmbx = {}
        thumbnil = {}
        sisax = 0

        for i in range(0,total,limit):
            sisax = abs((i+limit) - total)

        for y in range(sisax):
            thmbx = {'url':'/media/profile/images/artikel/thumbnail-berita-kecil.png'}
            title_xx = 'Berita Dinas Pendidikan Provinsi Papua Tengah'
            sisay.append({'title':title_xx, 'thumbnail':thmbx, 'created_at':datetime.now(), 'slug':'lorem-ipsum', 
                'seen':0, 'created_by':{'first_name':'Admin', 'last_name':'Website'},'content':content,})

        for z in listdata:
            thumbnil = {'url':z.thumbnail.url.replace('artikel/', 'artikel/')} 
            hasilx.append({'title':z.title, 'thumbnail':thumbnil, 'created_at':z.created_at, 'slug':z.slug, 
                'seen':z.seen, 'created_by':z.created_by, 'content':z.content,})

        dt_news = numpy.append(numpy.array(hasilx),numpy.array(sisay))

    return dt_news

def get_list_galeri(limit):
    listdata = Product.objects.filter(deleted_at=None).order_by('-created_by')[:limit]

    for x in listdata:
        x.is_images = os.path.isfile(settings.MEDIA_ROOT+str(x.images))
        if not x.is_images:
            x.images = '/media/profile/images/produk/titlelogo.png'

    if len(listdata) == 0:
        images_ = {}
        child = []
        for x in range(limit):
            images_['url']= '/media/profile/images/produk/titlelogo.png'
            child.append({'product_name':'', 'category':'', 'option':'', 'images':images_, 'best':''})

        return child

    else:   
        ### belakang dummy jika kurang dari 5 data
        total = int(len(listdata))
        sisay = []
        hslex = []
        imagx = {}
        sisax = 0

        for i in range(0,total,limit):
            sisax = abs((i+limit) - total)

        for y in range(sisax):
            imagx['url'] = '/media/profile/images/produk/titlelogo.png'
            sisay.append({'product_name':'', 'category':'', 'option':'', 'images':imagx, 'best':''})

        for z in listdata:
            hslex.append({'product_name':z.product_name, 'category':z.category.nama, 
                'option':z.category.nama.lower(), 'images':z.images, 'best':z.best_seller
            })

        ls_data = numpy.append(numpy.array(hslex),numpy.array(sisay))

        return ls_data


def infogempa():

    # https://isnaininurulkurniasari.medium.com/web-scrapping-dengan-xml-tree-e92d547958a4
    # https://towardsdatascience.com/processing-xml-in-python-elementtree-c8992941efd2

    cinta = {}
    nganu = []
    datax = xmlreader('https://data.bmkg.go.id/DataMKG/TEWS/gempaterkini.xml')
    for x in datax:
        for elem in datax.findall('gempa/'):
            if elem.tag == 'point':
                for chil in elem:
                    cinta[elem.tag] = chil.text
            else:
                cinta[elem.tag] = elem.text

            nganu.append(cinta)

    return nganu

def perkiraancuaca():
    arr_cuaca = {'0':'Cerah|clear-day', '1':'Cerah Berawan|partly-cloudy-day', '2':'Cerah Berawan|partly-cloudy-day',
        '3':'Berawan|cloudy', '4':'Berawan Tebal|cloudy', '5':'Udara Kabur|wind', '10':'Asap|fog',
        '45':'Kabut|fog', '60':'Hujan Ringan|rain', '61':'Hujan Sedang|rain', '63':'Hujan Lebat|sleet',
        '80':'Hujan Lokal|rain', '95':'Hujan Petir', '97':'Hujan Petir'
    }

    # "clear-day", "clear-night", "partly-cloudy-day",
    #             "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
    #             "fog"

    cinta = {}
    cuaca = []
    temps = []
    datax = xmlreader('https://data.bmkg.go.id/DataMKG/MEWS/DigitalForecast/DigitalForecast-Papua.xml')
    for x in datax.findall("./forecast/area/[@id='5014309']/parameter/[@id='weather']/"):
        cinta = x.attrib 
        for z in x:
            cinta['kd_val'] = z.text
            cinta['value'] = arr_cuaca[z.text] 
        cuaca.append(cinta)

    # for x in datax.findall("./forecast/area/[@id='5014309']/parameter/[@id='t']/"):
    #     temps.append(x.attrib)

    return {'cuaca':cuaca, 'temps':temps }
