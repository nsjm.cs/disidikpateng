# Generated by Django 3.2 on 2022-08-16 17:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rizq_sanjaya_app', '0022_remove_gallery_category'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categoryitem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('object_id', models.IntegerField(default=0)),
                ('content_type_id', models.IntegerField(default=0)),
                ('categori_id', models.IntegerField(default=0)),
            ],
        ),
    ]
