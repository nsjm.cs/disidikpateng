# Generated by Django 3.2 on 2022-09-11 11:32

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('rizq_sanjaya_app', '0032_remove_paketwisata_images'),
    ]

    operations = [
        migrations.AddField(
            model_name='paketwisata',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='paketwisata',
            name='deleted_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='paketwisata',
            name='updated_at',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
