# Generated by Django 3.2 on 2022-09-21 12:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rizq_sanjaya_app', '0034_auto_20220911_1905'),
    ]

    operations = [
        migrations.AddField(
            model_name='layanan',
            name='jenis',
            field=models.CharField(default='', max_length=100),
        ),
    ]
