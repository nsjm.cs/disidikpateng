# Generated by Django 3.2.6 on 2023-05-26 08:37

from django.db import migrations, models
import rizq_sanjaya_app.models.profile


class Migration(migrations.Migration):

    dependencies = [
        ('rizq_sanjaya_app', '0052_kata'),
    ]

    operations = [
        migrations.AddField(
            model_name='appsetting',
            name='image',
            field=models.ImageField(null=True, upload_to='profile/images/diskon/', validators=[rizq_sanjaya_app.models.profile.validate_file_gambar, rizq_sanjaya_app.models.profile.validate_file_size_gambar]),
        ),
    ]
