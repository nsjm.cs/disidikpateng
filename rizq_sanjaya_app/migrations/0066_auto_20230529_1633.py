# Generated by Django 3.2.6 on 2023-05-29 09:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rizq_sanjaya_app', '0065_remove_destinasi_foto'),
    ]

    operations = [
        migrations.RenameField(
            model_name='foto',
            old_name='image',
            new_name='images',
        ),
        migrations.AddField(
            model_name='destinasi',
            name='images',
            field=models.ManyToManyField(related_name='destinasi_images', to='rizq_sanjaya_app.Foto'),
        ),
    ]
