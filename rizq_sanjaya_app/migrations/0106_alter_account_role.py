from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rizq_sanjaya_app', '0105_pelayananpublik_catatan'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='role',
            field=models.CharField(choices=[('admin', 'Admin'), ('posting', 'Posting'), ('admin_basiswa', 'Admin Beasiswa')], default='posting', max_length=50),
        ),
    ]
