from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from django.utils.text import slugify
import string
import random

from requests import request
from ckeditor_uploader.fields import RichTextUploadingField
from taggit.managers import TaggableManager
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.contrib.auth import get_user_model
from django.utils import timezone
import datetime
from ckeditor.fields import RichTextField


""""==============================VALIDATOR======================================"""
def validate_file_pdf(value):
	pass

"""VALIDASI UNTUK FILE DOKUMEN"""
def validate_file_dokumen(value):
	dokumen =[
			# 'application/vnd.ms-excel', 
			# 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 
			'application/pdf', 
			'image/jpeg',
			'image/png',
			# 'text/csv',
			# 'application/msword',
			# 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			]
	if value.file.content_type not in dokumen:
		# raise ValidationError(u'Pastikan ekstensi file adalah .csv, .doc, .docx, .pdf, .xls atau .xlxs.')
		raise ValidationError(u'Pastikan ekstensi file adalah .jpeg, .png, .pdf')

"""VALIDASI UNTUK FILE GAMBAR"""
def validate_file_gambar(value):
	dokumen =[
				'image/jpeg',
				'image/png',
				'image/webp',
				'image/jpg',
				'video/mp4',
        		'video/mpeg',
        		'video/quicktime',
				'video/x-flv',
				'video/MP2T',
				'video/3gpp',
				'video/x-msvideo',
				'video/x-ms-wmv',
				'image/gif'
			]
	if value.file.content_type not in dokumen:
		raise ValidationError(u'Pastikan ekstensi file adalah .jpg, .jpeg atau .png.')

"""VALIDASI UNTUK UKURAN MAKSIMAL UKURAN FILE"""
def validate_file_size_dokumen(value):
	filesize= value.size
	
	# if filesize > 5242880:
	if filesize > 5242880:
		raise ValidationError("Pastikan ukuran File dibawah 5 MB.")
	else:
		return value

"""VALIDASI UNTUK UKURAN MAKSIMAL UKURAN GAMBAR"""
def validate_file_size_gambar(value):
	filesize= value.size
	
	# if filesize > 5242880:
	if filesize > 2242880:
		raise ValidationError("Pastikan ukuran File dibawah 2 MB.")
	else:
		return value


""""==============================FUNGSI======================================"""
"""MEMBERIKAN NILAI RANDOM UNTUK SLUG KALO DUPLIKAT"""
def rand_slug():
	rand = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(8))
	return rand.lower()

COLOR_CHOICES = [
	('#0d6efd', 'Biru'),
	('#6610f2', 'Indigo'),
	('#6f42c1', 'Ungu'),
	('#d63384', 'Merah Muda'),
	('#dc3545', 'Merah'),
	('#fd7e14', 'Oranye'),
	('#ffc107', 'Kuning'),
	('#198754', 'Hijau'),
	('#20c997', 'Teal'),
	('#0dcaf0', 'Cyan'),
	('#adb5bd', 'Abu-Abu'),
	('#000', 'Hitam'),
]
ANNOUNCEMENT_CHOICES = [
	('ASN', 'ASN'),
	('Umum', 'Umum'),
]
ROLE_CHOICES = [
	('admin', 'Admin'),
	('posting', 'Posting'),
	('admin_beasiswa', 'Admin Beasiswa'),
]


""""==============================MODEL TABEL======================================"""
"""BASE MODEL BIAR GA NGULANG2 MASUKIN 3 FIELD KETERANGAN WAKTU INI"""
class Time(models.Model):
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	deleted_at = models.DateTimeField(null=True, blank=True)

	class Meta:
		abstract = True
		
class AccountManager(BaseUserManager):
	use_in_migrations = True

	def _create_user(self, email, username, phone, password, **extra_fields):
		values = [email, username, phone,]
		field_value_map = dict(zip(self.model.REQUIRED_FIELDS, values))
		for field_name, value in field_value_map.items():
			if not value:
				raise ValueError('The {} value must be set'.format(field_name))

		email = self.normalize_email(email)
		user = self.model(
			email=email,
			username=username,
			phone=phone,
			**extra_fields
		)
		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_user(self, email, username, phone, password=None, **extra_fields):
		extra_fields.setdefault('is_staff', False)
		extra_fields.setdefault('is_superuser', False)
		return self._create_user(email, username, phone, password, **extra_fields)

	def create_superuser(self, email, username, phone, password=None, **extra_fields):
		extra_fields.setdefault('is_staff', True)
		extra_fields.setdefault('is_superuser', True)
		extra_fields.setdefault('is_verified', True)

		if extra_fields.get('is_staff') is not True:
			raise ValueError('Superuser must have is_staff=True.')
		if extra_fields.get('is_superuser') is not True:
			raise ValueError('Superuser must have is_superuser=True.')
		if extra_fields.get('is_verified') is not True:
			raise ValueError('Superuser must have is_verified=True.')

		return self._create_user(email, username, phone, password, **extra_fields)


"""TABEL AKUN UNTUK SELAIN BAWAANNYA DJANGO YANG DIPAKAI"""
class Account(AbstractBaseUser, PermissionsMixin):
	email = models.EmailField(unique=True)
	username = models.CharField(unique=True, max_length=50)
	first_name = models.CharField(max_length=50)
	last_name = models.CharField(max_length=50)
	is_staff = models.BooleanField(default=False)
	is_active = models.BooleanField(default=True)
	is_verified = models.BooleanField(default=False)
	date_joined = models.DateTimeField(default=timezone.now)
	last_login = models.DateTimeField(null=True)
	phone = models.CharField(max_length=15)
	date_of_birth = models.DateField(blank=True, null=True)
	avatar = models.ImageField(blank=True, null=True, upload_to='profile/images/avatar/', validators=[validate_file_gambar, validate_file_size_gambar],)
	role = models.CharField(max_length=50, choices=ROLE_CHOICES, default='posting')
	email_verification_token = models.CharField(max_length=100, default='')
	
	objects = AccountManager()

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['username', 'phone', 'role']

	def get_full_name(self):
		return f"{self.first_name} {self.last_name}"

	def get_short_name(self):
		return self.first_name

"""TABEL CATEGORY UNTUK BERITA dan GALERI"""
class Categoryitem(models.Model):
	object_id = models.IntegerField(default=0)
	content_type_id = models.IntegerField(default=0)
	categori_id = models.IntegerField(default=0)

class Category(models.Model):
	nama = models.CharField(max_length=100, null=False, blank=False, unique=True)
	typexs = models.CharField(max_length=100,default='product')
	slug = models.SlugField(unique=True, max_length=150)
	foto = models.ImageField(null=True, upload_to='profile/images/kategori', validators=[validate_file_gambar, validate_file_size_gambar])

	def __str__(self):
		return self.nama

	def __init__(self, *args, **kwargs):
		super(Category, self).__init__(*args, **kwargs)
		self.old_nama = self.nama

	def save(self, *args, **kwargs):
		if self.old_nama != self.nama:
			slug = slugify(self.nama)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = Category.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except Category.DoesNotExist:
					self.slug = slug
					break
		super(Category, self).save(*args, **kwargs)

class RnW(Time):
	jenis = models.CharField(max_length=100)
	deskripsi = RichTextUploadingField()

"""TABEL NEWS UNTUK MENYIMPAN BERITA"""
class News(Time):
	title = models.CharField(max_length=255)
	thumbnail = models.ImageField(upload_to='profile/images/artikel/', validators=[validate_file_gambar, validate_file_size_gambar], default='profile/images/artikel/no-image.jpg')
	content = RichTextUploadingField()
	slug = models.SlugField(unique=True, max_length=300)
	category = models.ForeignKey(Category, on_delete=models.RESTRICT)
	tags = TaggableManager()
	created_by = models.ForeignKey(Account, on_delete=models.RESTRICT)
	last_updated_by = models.CharField(max_length=5, null=True, blank=True)
	seen = models.SmallIntegerField(default=0)
	jenis = models.CharField(max_length=255, null=True)

	def __init__(self, *args, **kwargs):
		super(News, self).__init__(*args, **kwargs)
		self.old_title = self.title

	def save(self, *args, **kwargs):
		if self.old_title != self.title:
			slug = slugify(self.title)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = News.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except News.DoesNotExist:
					self.slug = slug
					break
		super(News, self).save(*args, **kwargs)


"""TABEL GALERI UNTUK MENYIMPAN FOTO DAN LINK YOUTUBE VIDEO"""
class Gallery(Time):
	title = models.CharField(max_length=255)
	keterangan = models.TextField(max_length=600)
	foto = models.ImageField(upload_to='profile/images/galeri/', validators=[validate_file_gambar, validate_file_size_gambar], null=True, blank=True)
	video = models.TextField(null=True, blank=True, max_length=500)
	slug = models.SlugField(unique=True, max_length=300)
	tags = TaggableManager()
	created_by = models.ForeignKey(Account, on_delete=models.RESTRICT)
	categori = models.CharField(max_length=255, default='')
	last_updated_by = models.CharField(max_length=5, null=True, blank=True)

	def __init__(self, *args, **kwargs):
		super(Gallery, self).__init__(*args, **kwargs)
		self.old_title = self.title

	def save(self, *args, **kwargs):
		if self.old_title != self.title:
			slug = slugify(self.title)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = Gallery.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except Gallery.DoesNotExist:
					self.slug = slug
					break
		super(Gallery, self).save(*args, **kwargs)

class RunningText(Time):
	text=models.CharField(max_length=255)

class RunningLink(Time):
	nama = models.CharField(max_length=100)
	logo=models.ImageField(upload_to='profile/images/running_link/', validators=[validate_file_gambar, validate_file_size_gambar])
	link=models.CharField(max_length=500)

class Link(Time):
	nama=models.CharField(max_length=100, unique=True)
	link=models.CharField(max_length=500)

class AppSetting(Time):
	nama=models.CharField(max_length=100, unique=True)
	keterangan=models.TextField(null=True)
	tanggal = models.DateField(null=True)
	image = models.ImageField(upload_to='profile/images/diskon/', validators=[validate_file_gambar, validate_file_size_gambar], null=True)	

class Kata(Time):
	judul = models.TextField(null=True)
	keterangan = models.TextField(null=True)
	image = models.ImageField(upload_to='profile/images/kata/', validators=[validate_file_gambar, validate_file_size_gambar])
	 
class Carousel(Time):
	judul = models.TextField(null=True, blank=True)
	subjudul = models.TextField(null=True)
	foto = models.ImageField(null=True, upload_to='profile/images/carousel/', validators=[validate_file_gambar, validate_file_size_gambar])
	video = models.FileField(upload_to='profile/images/carousel/video/', null=True, blank=True)
	link=models.CharField(max_length=200, null=True, blank=True)
	kondisi = models.TextField(default=True, null=True)
	keterangan = RichTextField(null=True)
	harga = models.IntegerField(null=True)
	diskon = models.IntegerField(null=True)
	persen_diskon = models.IntegerField(null=True)

class Foto(models.Model):
    images = models.ImageField(upload_to='profile/images/destinasi/', validators=[validate_file_gambar, validate_file_size_gambar], null=True)

class Destinasi(models.Model):
	created_at = models.DateTimeField(auto_now_add=True, null=True)
	updated_at = models.DateTimeField(auto_now=True, null=True)
	deleted_at = models.DateTimeField(null=True, blank=True)
	nama = models.TextField(null=True)
	fasilitas = models.TextField(null=True)
	deskripsi = models.TextField(null=True)
	paket = models.ForeignKey(Carousel, on_delete=models.CASCADE)
	images = models.ManyToManyField(Foto, related_name='destinasi_images')

class DestinasiImage(models.Model):
    destinasi = models.ForeignKey(Destinasi, default=None, on_delete=models.CASCADE)
    images = models.ImageField(upload_to='profile/images/destinasi/', validators=[validate_file_gambar, validate_file_size_gambar], null=True)
    

class LogVisitor(models.Model):
	ip = models.CharField(max_length=20, default='-')
	device_type = models.CharField(max_length=20, default='-')
	browser_type = models.CharField(max_length=30, default='-')
	browser_version = models.CharField(max_length=30, default='-')
	os_type = models.CharField(max_length=30, default='-')
	os_version = models.CharField(max_length=30, default='-')
	waktu = models.DateTimeField(auto_now_add=True)

class LogAdmin(models.Model):
	ip = models.CharField(max_length=20, default='-')
	device_type = models.CharField(max_length=20, default='-')
	browser_type = models.CharField(max_length=30, default='-')
	browser_version = models.CharField(max_length=30, default='-')
	os_type = models.CharField(max_length=30, default='-')
	os_version = models.CharField(max_length=30, default='-')
	aktivitas = models.CharField(max_length=30, default='-')
	waktu = models.DateTimeField(auto_now_add=True)

class AboutMe(models.Model):
	judul = models.CharField(max_length=255, default='Tentang Kami.')
	deskripsi = models.CharField(max_length=255, default='-')
	youtube = models.CharField(max_length=255, null=True, default='')
	icon = models.CharField(max_length=255, null=True, default='')
	images = models.ImageField(upload_to='profile/images/about/', validators=[validate_file_gambar, validate_file_size_gambar], default='profile/images/produk/no-image.jpg') 
	jenis = models.CharField(max_length=30, default='header')
	section = models.CharField(max_length=30, default='tentang')

class Layanan(models.Model):
	judul = models.CharField(max_length=100, default='Lorem Ipsum')
	keterangan = models.CharField(max_length=255, default='Lorem ipsum dolor sit amet')
	icon = models.CharField(max_length=255, null=True, default='')
	slug = models.CharField(max_length=255, default='')

class FAQ(models.Model):
	pertanyaan = models.TextField(null=True)
	jawaban = models.TextField(null=True)
	highlight = models.TextField(null=True)

class Product(Time):
	product_name = models.CharField(max_length=255)
	images = models.ImageField(upload_to='profile/images/produk/', validators=[validate_file_gambar, validate_file_size_gambar], default='profile/images/produk/no-image.jpg')
	description = RichTextUploadingField()
	slug = models.SlugField(unique=True, max_length=300)
	category = models.ForeignKey(Category, on_delete=models.RESTRICT)
	harga = models.IntegerField(null=True)
	diskon = models.IntegerField(null=True)
	persen_diskon = models.IntegerField(null=True)
	tags = TaggableManager()
	created_by = models.ForeignKey(Account, on_delete=models.RESTRICT)
	last_updated_by = models.CharField(max_length=5, null=True, blank=True)
	best_seller = models.BooleanField(max_length=1, default=False)
	
	def __init__(self, *args, **kwargs):
		super(Product, self).__init__(*args, **kwargs)
		self.old_product_name = self.product_name

	def save(self, *args, **kwargs):
		if self.old_product_name != self.product_name:
			slug = slugify(self.product_name)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = Product.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except Product.DoesNotExist:
					self.slug = slug
					break
		super(Product, self).save(*args, **kwargs)

class Announcement(Time):
    title=models.CharField(max_length=255)
    slug = models.SlugField(unique=True, max_length=300)
    description=models.TextField(max_length=1000)
    jenis=models.CharField(max_length=10, choices=ANNOUNCEMENT_CHOICES, default='Umum')
    created_by=models.ForeignKey(Account, on_delete=models.RESTRICT)
    last_updated_by = models.CharField(max_length=5, null=True, blank=True)
    dokumen = models.FileField(upload_to='profile/announcement/', validators=[validate_file_dokumen, validate_file_size_dokumen], null=True, blank=True)

    def __init__(self, *args, **kwargs):
        super(Announcement, self).__init__(*args, **kwargs)
        self.old_title = self.title

    def save(self, *args, **kwargs):
        if self.old_title != self.title:
            slug = slugify(self.title)
            slug_exists = True
            counter = rand_slug()
            self.slug = slug
            while slug_exists:
                try:
                    slug_exits = Announcement.objects.get(slug=slug)
                    if slug_exits and slug_exits.id == self.id:
                        self.slug = slug
                        break
                    elif slug_exits:
                        slug = self.slug + '-' + str(counter)
                        counter = rand_slug()
                except Announcement.DoesNotExist:
                    self.slug = slug
                    break
        super(Announcement, self).save(*args, **kwargs)

"""TABEL AGENDA UNTUK MENYIMPAN ACARA DAN KEGIATAN DI TANGGALAN"""
class Agenda(Time):
    name=models.CharField(max_length=255)
    slug = models.SlugField(unique=True, max_length=300)
    start_date=models.DateField()
    finish_date=models.DateField()
    start_time = models.TimeField(null=True, blank=True)
    finish_time = models.TimeField(null=True, blank=True)
    description=models.TextField(max_length=600)
    every_year=models.CharField(max_length=5)
    color=models.CharField(max_length=10, choices=COLOR_CHOICES, default='#fd7e14')
    created_by=models.ForeignKey(Account, on_delete=models.RESTRICT)
    last_updated_by = models.CharField(max_length=5, null=True, blank=True)

    def __init__(self, *args, **kwargs):
        super(Agenda, self).__init__(*args, **kwargs)
        self.old_name = self.name

    def save(self, *args, **kwargs):
        if self.old_name != self.name:
            slug = slugify(self.name)
            slug_exists = True
            counter = rand_slug()
            self.slug = slug
            while slug_exists:
                try:
                    slug_exits = Agenda.objects.get(slug=slug)
                    if slug_exits and slug_exits.id == self.id:
                        self.slug = slug
                        break
                    elif slug_exits:
                        slug = self.slug + '-' + str(counter)
                        counter = rand_slug()
                except Agenda.DoesNotExist:
                    self.slug = slug
                    break
        super(Agenda, self).save(*args, **kwargs)

"""TABEL LAYANAN DINAS DIGUNAKAN UNTUK MENYIMPAN LAYANAN DINAS BERDASARKAN JENIS LAYANAN"""

class LayananDinas(models.Model):
	title = RichTextUploadingField()
	typexs = models.CharField(max_length=500,default='Standar Pelayanan')
	status = models.CharField(max_length=500,default='Publish')
	description = RichTextUploadingField()
	foto = models.ImageField(null=True, upload_to='profile/images/layanan', validators=[validate_file_gambar, validate_file_size_gambar])

"""TABEL AKSESBILITAS EXTERNAL LINK DIGUNAKAN UNTUK MENYIMPAN AKSESBILITAS EXTERNAL LINK"""
class Aksesbilitas(models.Model):
	title = models.TextField(null=True)
	link = models.TextField(null=True)
	status = models.CharField(max_length=500,default='Publish')
	image = models.ImageField(upload_to='profile/images/aksesbilitas/', validators=[validate_file_gambar, validate_file_size_gambar])

"""TABEL INFORMASI TERPADU DIGUNAKAN UNTUK MENYIMPAN INFORMASI TERPADU"""
class InfoTerpadu(models.Model):
	title = RichTextUploadingField()
	create_date = models.DateTimeField(auto_now_add=True)
	start_event_date=models.DateField()
	end_event_date=models.DateField()
	typexs = models.CharField(max_length=500,default='infografis')
	description = RichTextUploadingField()
	foto = models.ImageField(null=True, upload_to='profile/images/infoterpadu', validators=[validate_file_gambar, validate_file_size_gambar])
	dokumen = models.FileField(upload_to='profile/announcement/infoterpadu/', validators=[validate_file_dokumen, validate_file_size_dokumen], null=True, blank=True)
	slug = models.SlugField(unique=True, max_length=150)
	status = models.CharField(max_length=500,default='Publish')
	def __init__(self, *args, **kwargs):
		super(InfoTerpadu, self).__init__(*args, **kwargs)
		self.old_title = self.title

	def save(self, *args, **kwargs):
		if self.old_title != self.title:
			slug = slugify(self.title)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = InfoTerpadu.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except InfoTerpadu.DoesNotExist:
					self.slug = slug
					break
		super(InfoTerpadu, self).save(*args, **kwargs)

"""TABEL PROFILSKPD DIGUNAKAN UNTUK MENYIMPAN SEJARAH,VISI&MISI, TUPOKSI, STRUKTUR ORGANISASI, PROFIL PEJABAT,SAMBUTAN KEPALA DINAS"""
class ProfilSKPD(models.Model):
	title = RichTextUploadingField()
	create_date = models.DateTimeField(auto_now_add=True)
	typexs = models.CharField(max_length=500,default='Sejarah')
	description = RichTextUploadingField()
	foto = models.FileField(null=True, upload_to='profile/images/profilSKPD/', validators=[validate_file_gambar, validate_file_size_gambar])
	dokumen = models.FileField(upload_to='profile/announcement/profilSKPD/', validators=[validate_file_dokumen, validate_file_size_dokumen], null=True, blank=True)
	slug = models.SlugField(unique=True, max_length=150)
	status = models.CharField(max_length=500,default='Publish')
	def __init__(self, *args, **kwargs):
		super(ProfilSKPD, self).__init__(*args, **kwargs)
		self.old_title = self.title

	def save(self, *args, **kwargs):
		if self.old_title != self.title:
			slug = slugify(self.title)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = ProfilSKPD.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except ProfilSKPD.DoesNotExist:
					self.slug = slug
					break
		super(ProfilSKPD, self).save(*args, **kwargs)


"""TABEL PPID DISDIK JOEL =================================================================================="""
class modul_PPID(Time):
	title = models.CharField(max_length=255)
	description = RichTextUploadingField()
	slug = models.SlugField(unique=True, max_length=255)
	create_date = models.DateTimeField(auto_now_add=True)
	tags = TaggableManager()
	created_by = models.ForeignKey(Account, on_delete=models.RESTRICT, null=True)
	last_updated_by = models.CharField(max_length=5, null=True, blank=True)
	status = models.CharField(max_length=500,default='Publish')
	def __init__(self, *args, **kwargs):
		super(modul_PPID, self).__init__(*args, **kwargs)
		self.old_title = self.title

	def save(self, *args, **kwargs):
		if self.old_title != self.title:
			slug = slugify(self.title)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = modul_PPID.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except modul_PPID.DoesNotExist:
					self.slug = slug
					break
		super(modul_PPID, self).save(*args, **kwargs)


# ### ============================================================================================================
# ### JOEL PPDB ==================================================================================================
# ### ============================================================================================================

# UNTUK Halaman Beranda, Tahapan dan FAQ.........................................................................
class ppdb_konten(Time):
	title = models.CharField(max_length=500)
	subtitle = models.CharField(max_length=500, null=True)
	description = RichTextUploadingField()
	typexs = models.CharField(max_length=100, default='beranda') #### beranda|tahapan|faq
	if_link = models.CharField(max_length=500, null=True)
	foto = models.ImageField(null=True, upload_to='profile/images/konten/', validators=[validate_file_gambar, validate_file_size_gambar])
	dokumen = models.FileField(null=True, blank=True, upload_to='profile/documents/konten/', validators=[validate_file_dokumen, validate_file_size_dokumen])
	slug = models.SlugField(unique=True, max_length=255)
	tags = TaggableManager()
	status = models.CharField(max_length=100, default='Publish')
	def __init__(self, *args, **kwargs):
		super(ppdb_konten, self).__init__(*args, **kwargs)
		self.old_title = self.title

	def save(self, *args, **kwargs):
		if self.old_title != self.title:
			slug = slugify(self.title)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = ppdb_konten.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except ppdb_konten.DoesNotExist:
					self.slug = slug
					break
		super(ppdb_konten, self).save(*args, **kwargs)


class ppdb_AppSetting(Time):
	nama = models.CharField(max_length=255, unique=True)
	keterangan = models.TextField(null=True)


class ppdb_News(Time):
	title = models.CharField(max_length=255)
	thumbnail = models.ImageField(upload_to='profile/images/ppdb/artikel/', validators=[validate_file_gambar, validate_file_size_gambar], default='profile/images/artikel/no-image.jpg')
	content = RichTextUploadingField()
	slug = models.SlugField(unique=True, max_length=300)
	tags = TaggableManager()
	created_by = models.ForeignKey(Account, on_delete=models.RESTRICT)
	last_updated_by = models.CharField(max_length=5, null=True, blank=True)
	seen = models.SmallIntegerField(default=0)

	def __init__(self, *args, **kwargs):
		super(ppdb_News, self).__init__(*args, **kwargs)
		self.old_title = self.title

	def save(self, *args, **kwargs):
		if self.old_title != self.title:
			slug = slugify(self.title)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = ppdb_News.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except ppdb_News.DoesNotExist:
					self.slug = slug
					break
		super(ppdb_News, self).save(*args, **kwargs)


class katPengaduan(Time):
	nama_kategori = models.TextField(null=True, blank=True)
	jenis_kategori = models.TextField(blank=True)
	slug = models.SlugField(unique=True, max_length=300)
	status = models.CharField(max_length=100, default='Publish')

	def __init__(self, *args, **kwargs):
		super(katPengaduan, self).__init__(*args, **kwargs)
		self.old_nama_kategori = self.nama_kategori

	def save(self, *args, **kwargs):
		if self.old_nama_kategori != self.nama_kategori:
			slug = slugify(self.nama_kategori)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = katPengaduan.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except katPengaduan.DoesNotExist:
					self.slug = slug
					break
		super(katPengaduan, self).save(*args, **kwargs)


class PelayananPublik(Time):
    no_identitas = models.CharField(max_length=255)
    nama = models.CharField(max_length=255)
    email = models.CharField(max_length=100, default='')
    no_hp = models.CharField(max_length=100, default='')
    status = models.CharField(max_length=100, default='perorangan')
    alamat = models.CharField(max_length=255)
    nama_instansi = models.CharField(max_length=255, null=True, blank=True)
    kategori = models.ForeignKey(katPengaduan, on_delete=models.RESTRICT)
    deskripsi = models.TextField(null=True, blank=True)
    catatan = models.TextField(null=True, blank=True)
    lampiran = models.ImageField(upload_to='profile/images/pengaduan/', validators=[validate_file_gambar, validate_file_size_gambar], default='profile/images/artikel/no-image.jpg')
    status_pengaduan = models.BooleanField(null=True)



