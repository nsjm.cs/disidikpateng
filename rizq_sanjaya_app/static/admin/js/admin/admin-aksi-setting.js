function restore_(nama, id, url, jenis,){
    Swal.fire({
        title: `Apakah anda yakin ingin memulihkan ${nama}?`,
        showCancelButton: true,
        confirmButtonText: 'Pulihkan',
        cancelButtonText: 'Batal',
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: `/manage_panel/setting/restore/${url}/${id}`,
                data: {},
                dataType: "html",
                timeout: 10000,
                beforeSend: function() {
                    Swal.fire({
                        title: 'Memproses.........',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                        onOpen: () => {
                            swal.showLoading();
                        }
                    });
                },
                success: function(response){
                    Swal.fire(`${jenis} berhasil dipulihkan!`, '', 'success');
                    setTimeout(function(){
                        window.location.href = `/manage_panel/setting/${url}/`;
                    }, 2000); 
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    Swal.fire(`${jenis} gagal dipulihkan!`, '', 'error');
                }
            });
            
        }
    });
}
function delete_(action, nama,  id, url, jenis){
    let pesan = action == 'soft_delete' ? "menyembunyikan" : "menghapus";
    let pesan2 = action == 'soft_delete' ? "sembunyikan" : "hapus";
    let confirmation = action == 'soft_delete' ? "Sembunyikan" : "Hapus";
    Swal.fire({
        title: `Apakah anda yakin ingin ${pesan} ${nama}?`,
        showCancelButton: true,
        confirmButtonText: confirmation,
        cancelButtonText: 'Batal',
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: `/manage_panel/setting/${action}/${url}/${id}`,
                data: {},
                dataType: "json",
                timeout: 10000,
                beforeSend: function() {
                    Swal.fire({
                        title: 'Memproses.........',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                    });
                    Swal.showLoading();
                },
                success: function(response){
                    if(response.message == 'integrity error'){
                        Swal.fire(`${jenis} gagal di${pesan2} karena masih digunakan oleh data lain, hapus data yang berkaitan terlebih dahulu!`, '', 'error');
                    }else{
                        Swal.fire(`${jenis} berhasil di${pesan2}!`, '', 'success');
                        setTimeout(function(){
                            window.location.href =  `/manage_panel/setting/${url}/`;
                        }, 2000); 
                    }
                    
                },
                error: function (response, xhr, ajaxOptions, thrownError) {
                    console.log(response);
                    Swal.fire(`${jenis} gagal di${pesan2}!`, '', 'error');
                }
            });
            
        }
    });
}