function restore_(nama, slug, url, jenis){
    Swal.fire({
        title: `Apakah anda yakin ingin memulihkan ${nama}?`,
        showCancelButton: true,
        confirmButtonText: 'Pulihkan',
        cancelButtonText: 'Batal',
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: `/manage_panel/${url}/restore/${slug}`,
                data: {},
                dataType: "html",
                timeout: 10000,
                beforeSend: function() {
                    Swal.fire({
                        title: 'Memproses.........',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                        onOpen: () => {
                            swal.showLoading();
                        }
                    });
                },
                success: function(response){
                    Swal.fire(`${jenis} berhasil dipulihkan!`, '', 'success');
                    setTimeout(function(){
                        window.location.href = `/manage_panel/${url}/`;
                    }, 2000); 
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    Swal.fire(`${jenis} gagal dipulihkan!`, '', 'error');
                }
            });
            
        }
    });
}
function delete_(action, nama,  slug, url, jenis){
    let pesan = action == 'soft_delete' ? "menyembunyikan" : "menghapus";
    let pesan2 = action == 'soft_delete' ? "sembunyikan" : "hapus";
    let confirmation = action == 'soft_delete' ? "Sembunyikan" : "Hapus";
    Swal.fire({
        title: `Apakah anda yakin ingin ${pesan} ${nama}?`,
        showCancelButton: true,
        confirmButtonText: confirmation,
        cancelButtonText: 'Batal',
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: `/manage_panel/${url}/${action}/${slug}`,
                data: {},
                dataType: "html",
                timeout: 10000,
                beforeSend: function() {
                    Swal.fire({
                        title: 'Memproses.........',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                    });
                    Swal.showLoading();
                },
                success: function(response){
                    Swal.fire(`${jenis} berhasil di${pesan2}!`, '', 'success');
                    setTimeout(function(){
                        window.location.href =  `/manage_panel/${url}/`;
                    }, 2000); 
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    Swal.fire(`${jenis} gagal di${pesan2}!`, '', 'error');
                }
            });
            
        }
    });
}

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}