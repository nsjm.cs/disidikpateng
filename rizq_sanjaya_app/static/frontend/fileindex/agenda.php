<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Dinas Pendidikan - Provinsi Papua Tengah</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Dinas Pendidikan - Provinsi Papua Tengah" name="keywords">
    <meta content="Dinas Pendidikan - Provinsi Papua Tengah" name="description">
    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700;800&family=Rubik:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <!-- Spinner Start -->
    <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
        <div class="spinner"></div>
    </div>
    <!-- Spinner End -->
    <!-- Navbar Start -->
    <div class="container-fluid position-relative p-0">
        <?php include 'menu.php'; ?>
        <!-- start banner  - berita -->
        <div class="container-fluid bg-primary py-5 bg-header" style="margin-bottom: 90px;">
            <div class="row py-5">
                <div class="col-12 pt-lg-5 mt-lg-5 text-center">
                    <h5 class="display-5 text-white animated zoomIn"><i class="far fa-calendar-alt"></i> Agenda Kegiatan</h5>
                    <a href="" class="h5 text-white">Dinas Pendidikan & Kebudayaan Provinsi Papua Tengah</a>
                </div>
            </div>
        </div>
        <!-- end banner  - berita -->
    </div>
    <!-- Navbar End -->

    <!-- Full Screen Search Start -->
    <div class="modal fade" id="searchModal" tabindex="-1">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content" style="background: rgba(9, 30, 62, .7);">
                <div class="modal-header border-0">
                    <button type="button" class="btn bg-white btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body d-flex align-items-center justify-content-center">
                    <div class="input-group" style="max-width: 600px;">
                        <input type="text" class="form-control bg-transparent border-primary p-3" placeholder="Type search keyword">
                        <button class="btn btn-primary px-4"><i class="bi bi-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Full Screen Search End -->


    <!-- Blog Start -->
    <div class="container-fluid wow fadeInUp" data-wow-delay="0.1s">
        <div class="container">
            <div class="container py-1">
                <div class="section-title section-title-sm position-relative pb-3 mb-4">
                    <h5 class="mb-0"><span class="fa fa-list"></span> Daftar Agenda Kegiatan Dinas Pendidikan dan Kebudayaan Provinsi Papua Tengah</h5>
                </div>
                <hr>
                    <div class="row g-5 pb-5">
                        <div class="col-lg-4">
                        <img class="img-fluid" src="img/blog/agenda2.png">  
                        <small class="me-3 mb-3"><i class="fa fa-eye text-primary me-2"></i> 1.235 Views</small>
                        <small><i class="fa fa-clock text-primary me-2 mb-3" ></i>Pukul : 09:00 WIT - selesai</small>  
                        
                        <div class="bg-light rounded border-top border-5 border-primary d-flex flex-column  p-5">
                                <h6><i class="fa fa-calendar"></i> 30 Juni 2023</h6>
                                <p class="mb-0">Pelatihan Guru Bahasa Papua</p>
                        </div>
                            
                        </div>
                        <div class="col-lg-4">
                        <img class="img-fluid" src="img/blog/agenda1.png">    
                        <small class="me-3 mb-3"><i class="fa fa-eye text-primary me-2"></i> 1.235 Views</small>
                        <small><i class="fa fa-clock text-primary me-2 mb-3" ></i>Pukul : 09:00 WIT - selesai</small>
                        <div class="bg-light rounded border-top border-5 border-primary d-flex flex-column  p-5">
                                <h6><i class="fa fa-calendar"></i> 1 Juli 2023</h6>
                                <p class="mb-0">Seminar Pendidikan Inklusif</p>
                                
                            </div>
                            
                        </div>
                        <div class="col-lg-4">
                        <img class="img-fluid" src="img/blog/agenda3.png">    
                        <small class="me-3 mb-3"><i class="fa fa-eye text-primary me-2"></i> 1.235 Views</small>
                        <small><i class="fa fa-clock text-primary me-2 mb-3" ></i>Pukul : 09:00 WIT - selesai</small>
                        <div class="bg-light rounded border-top border-5 border-primary d-flex flex-column  p-5">
                                <h6><i class="fa fa-calendar"></i> 10 Juli 2023</h6>
                                <p class="mb-0">Festival Seni dan Budaya Papua Tengah</p>
                                
                            </div>
                            
                        </div>
                    </div>
            </div>
            
            <div class="row">
                <!-- Blog list Start -->
                <div class="col-lg-8">
                    <!-- Blog Detail Start -->
                    <div class="mb-5">
                        
                        <div class="d-flex align-items-center border-bottom pt-5 pb-4">
                            <i class="fa fa-clock fa-2x" style="margin-right:10px;"></i>    
                            <h1 class="text-primary"  style="margin-top:5px; font-family:arial;">31</h1>
                                <small class="text-uppercase">DESEMBER 2023</small>
                                <div class="ps-1"><h1 style="font-family:arial;">|</h1></div>
                                <div class="ps-4">
                                    <a href="detail_blog.php">
                                        <h4 class="text-primary mb-1" style="font-size:18px;">Inovasi Pendidikan di Indonesia: Mendorong Transformasi Digital di Sekolah</h4>
                                    </a>
                                        <small class="me-3 mb-3"><i class="fa fa-eye text-primary me-2"></i> 1.235 Views</small>
                                        <small><i class="fa fa-clock text-primary me-2 mb-3" ></i>Pukul : 09:00 WIT - selesai</small>
                                </div>
                        </div>
                            <div class="d-flex align-items-center border-bottom pt-5 pb-4">
                                <i class="fa fa-clock fa-2x" style="margin-right:10px;"></i>    
                                <h1 class="text-primary"  style="margin-top:5px; font-family:arial;">31</h1>
                                    <small class="text-uppercase">DESEMBER 2023</small>
                                    <div class="ps-1"><h1 style="font-family:arial;">|</h1></div>
                                    <div class="ps-4">
                                        <a href="detail_blog.php">
                                            <h4 class="text-primary mb-1" style="font-size:18px;">Inovasi Pendidikan di Indonesia: Mendorong Transformasi Digital di Sekolah</h4>
                                        </a>
                                        <small class="me-3 mb-3"><i class="fa fa-eye text-primary me-2"></i> 1.235 Views</small>
                                        <small><i class="fa fa-clock text-primary me-2 mb-3" ></i>Pukul : 09:00 WIT - selesai</small>
                                    </div>
                            </div>
                            <div class="d-flex align-items-center border-bottom pt-5 pb-4">
                                <i class="fa fa-clock fa-2x" style="margin-right:10px;"></i>    
                                <h1 class="text-primary"  style="margin-top:5px; font-family:arial;">31</h1>
                                    <small class="text-uppercase">DESEMBER 2023</small>
                                    <div class="ps-1"><h1 style="font-family:arial;">|</h1></div>
                                    <div class="ps-4">
                                        <a href="detail_blog.php">
                                            <h4 class="text-primary mb-1" style="font-size:18px;">Inovasi Pendidikan di Indonesia: Mendorong Transformasi Digital di Sekolah</h4>
                                        </a>
                                        <small class="me-3 mb-3"><i class="fa fa-eye text-primary me-2"></i> 1.235 Views</small>
                                        <small><i class="fa fa-clock text-primary me-2 mb-3" ></i>Pukul : 09:00 WIT - selesai</small>
                                    </div>
                            </div>
                            <div class="d-flex align-items-center border-bottom pt-5 pb-4">
                                <i class="fa fa-clock fa-2x" style="margin-right:10px;"></i>    
                                <h1 class="text-primary"  style="margin-top:5px; font-family:arial;">31</h1>
                                    <small class="text-uppercase">DESEMBER 2023</small>
                                    <div class="ps-1"><h1 style="font-family:arial;">|</h1></div>
                                    <div class="ps-4">
                                        <a href="detail_blog.php">
                                            <h4 class="text-primary mb-1" style="font-size:18px;">Inovasi Pendidikan di Indonesia: Mendorong Transformasi Digital di Sekolah</h4>
                                        </a>
                                        <small class="me-3 mb-3"><i class="fa fa-eye text-primary me-2"></i> 1.235 Views</small>
                                        <small><i class="fa fa-clock text-primary me-2 mb-3" ></i>Pukul : 09:00 WIT - selesai</small>
                                    </div>
                            </div>
                            <div class="d-flex align-items-center border-bottom pt-5 pb-4">
                                <i class="fa fa-clock fa-2x" style="margin-right:10px;"></i>    
                                <h1 class="text-primary"  style="margin-top:5px; font-family:arial;">31</h1>
                                    <small class="text-uppercase">DESEMBER 2023</small>
                                    <div class="ps-1"><h1 style="font-family:arial;">|</h1></div>
                                    <div class="ps-4">
                                        <a href="detail_blog.php">
                                            <h4 class="text-primary mb-1" style="font-size:18px;">Inovasi Pendidikan di Indonesia: Mendorong Transformasi Digital di Sekolah</h4>
                                        </a>
                                        <small class="me-3 mb-3"><i class="fa fa-eye text-primary me-2"></i> 1.235 Views</small>
                                        <small><i class="fa fa-clock text-primary me-2 mb-3" ></i>Pukul : 09:00 WIT - selesai</small>
                                    </div>
                            </div>
                            <div class="d-flex align-items-center border-bottom pt-5 pb-4">
                                <i class="fa fa-clock fa-2x" style="margin-right:10px;"></i>    
                                <h1 class="text-primary"  style="margin-top:5px; font-family:arial;">31</h1>
                                    <small class="text-uppercase">DESEMBER 2023</small>
                                    <div class="ps-1"><h1 style="font-family:arial;">|</h1></div>
                                    <div class="ps-4">
                                        <a href="detail_blog.php">
                                            <h4 class="text-primary mb-1" style="font-size:18px;">Inovasi Pendidikan di Indonesia: Mendorong Transformasi Digital di Sekolah</h4>
                                        </a>
                                        <small class="me-3 mb-3"><i class="fa fa-eye text-primary me-2"></i> 1.235 Views</small>
                                        <small><i class="fa fa-clock text-primary me-2 mb-3" ></i>Pukul : 09:00 WIT - selesai</small>
                                    </div>
                            </div>
                            <div class="pt-5">
                                <nav aria-label="Page navigation">
                                <ul class="pagination pagination-md m-0">
                                    <li class="page-item disabled">
                                    <a class="page-link rounded-0" href="#" aria-label="Previous">
                                        <span aria-hidden="true"><i class="bi bi-arrow-left"></i></span>
                                    </a>
                                    </li>
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                    <a class="page-link rounded-0" href="#" aria-label="Next">
                                        <span aria-hidden="true"><i class="bi bi-arrow-right"></i></span>
                                    </a>
                                    </li>
                                </ul>
                                </nav>
                            </div>
                    </div>
                    <!-- Blog Detail End -->   
                  
                </div>
                <!-- Blog list End -->

    
                <!-- Sidebar Start -->
                <div class="col-lg-4">
                    <!-- Image Start -->
                    <div class="mb-5 wow slideInUp" data-wow-delay="0.1s">
                        <img src="img/blog/agenda1.png" alt="" class="img-fluid rounded">
                    </div>
                    <!-- Image End -->
                    <!-- Search Form End -->
                      <!-- Recent Post Start -->
                      <div class="mb-5 wow slideInUp" data-wow-delay="0.1s">
                        <div class="section-title section-title-sm position-relative pb-3 mb-4">
                            <h5 class="mb-0">Berita Terbaru</h5>
                        </div>
                        <div class="d-flex rounded overflow-hidden mb-3">
                            <img class="img-fluid" src="img/blog/blog1.png" style="width: 100px; height: 100px; object-fit: cover;" alt="">
                            <a href="detail_blog.php" class="h5 fw-semi-bold d-flex align-items-center bg-light px-3 mb-0">Inovasi Pendidikan di Indonesia: Mendorong Transformasi Digital di Sekolah</a>
                        </div>

                        <div class="d-flex rounded overflow-hidden mb-3">
                            <img class="img-fluid" src="img/blog/blog2.png" style="width: 100px; height: 100px; object-fit: cover;" alt="">
                            <a href="detail_blog.php" class="h5 fw-semi-bold d-flex align-items-center bg-light px-3 mb-0">Implementasi Kurikulum 2023: Menghadirkan Pembelajaran Kontekstual</a>
                        </div>

                        <div class="d-flex rounded overflow-hidden mb-3">
                            <img class="img-fluid" src="img/blog/blog3.png" style="width: 100px; height: 100px; object-fit: cover;" alt="">
                            <a href="detail_blog.php" class="h5 fw-semi-bold d-flex align-items-center bg-light px-3 mb-0">Revitalisasi Perpustakaan Sekolah: Membangun Minat Baca Generasi Milenial</a>
                        </div>

                        <div class="d-flex rounded overflow-hidden mb-3">
                            <img class="img-fluid" src="img/blog/blog4.png" style="width: 100px; height: 100px; object-fit: cover;" alt="">
                            <a href="detail_blog.php" class="h5 fw-semi-bold d-flex align-items-center bg-light px-3 mb-0">Pendidikan Abad 21: Menyiapkan Siswa untuk Menghadapi Tantangan Global</a>
                        </div>

                        <div class="d-flex rounded overflow-hidden mb-3">
                            <img class="img-fluid" src="img/blog/blog5.png" style="width: 100px; height: 100px; object-fit: cover;" alt="">
                            <a href="detail_blog.php" class="h5 fw-semi-bold d-flex align-items-center bg-light px-3 mb-0">Inovasi Pembelajaran Jarak Jauh: Masa Depan Pendidikan di Era Digital</a>
                        </div>

                    </div>

                    <!-- Recent Post End -->
                   
                </div>
                <!-- Sidebar End -->
            </div>
        </div>
    </div>
    <!-- Blog End -->

    <?php include 'footer.php'; ?>
    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square rounded back-to-top"><i class="bi bi-arrow-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/wow/wow.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/counterup/counterup.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>
    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>