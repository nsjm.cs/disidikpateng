<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Dinas Pendidikan - Provinsi Papua Tengah</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Dinas Pendidikan - Provinsi Papua Tengah" name="keywords">
    <meta content="Dinas Pendidikan - Provinsi Papua Tengah" name="description">
    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700;800&family=Rubik:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <!-- Spinner Start -->
    <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
        <div class="spinner"></div>
    </div>
    <!-- Navbar & Carousel Start -->
        <div class="container-fluid position-relative p-0">
        <?php include 'menu.php'; ?>
            <div id="carouselVideoExample" class="tp-slider__area p-relative carousel slide carousel-fade" data-mdb-ride="carousel">
        <!-- Indicators -->
        <!-- Inner -->
            <div class="carousel-inner">
                <!-- Single item -->
                <div class="carousel-item active">
                <video class="img-fluid" autoplay loop muted>
                    <source src="img/slider/bg-video-web-afirev.mp4" type="video/mp4" />
                </video>
                </div>
                <!-- Single item -->
            </div>
        <!-- Inner -->
    <!-- Controls -->
        </div>
    </div>
    <!-- Navbar & Carousel End -->


    <!-- Full Screen Search Start -->
    <div class="modal fade" id="searchModal" tabindex="-1">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content" style="background: rgba(9, 30, 62, .7);">
                <div class="modal-header border-0">
                    <button type="button" class="btn bg-white btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body d-flex align-items-center justify-content-center">
                    <div class="input-group" style="max-width: 600px;">
                        <input type="text" class="form-control bg-transparent border-primary p-3" placeholder="Type search keyword">
                        <button class="btn btn-primary px-4"><i class="bi bi-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Full Screen Search End -->
    <!-- Facts Start -->
    <div class="container-fluid facts py-5 pt-lg-0">
        <div class="container py-5 pt-lg-0">
            <div class="row">
                <div class="col-lg-4 wow zoomIn pb-10" data-wow-delay="0.6s" style="padding-bottom:10px;">
                    <div class="shadow d-flex align-items-center justify-content-center p-4" style="background-color: rgb(26, 64, 135);
                            border-radius: 10px;
                            visibility: visible;
                            animation-duration: 1.5s;
                            animation-delay: 700ms;
                            animation-name: fadeInUp;">
                            <i class="fa fa-graduation-cap  text-white fa-3x"></i>
                        <div class="ps-4">
                            <h4 class="text-white mb-0" >PPBD</h4>
                            <h6 class="text-white mb-0">Informasi Pendaftaran Peserta Didik Baru</h6>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 wow zoomIn pb-10" data-wow-delay="0.3s" style="padding-bottom:10px;">
                    <div class="shadow d-flex align-items-center justify-content-center p-4" style="background-color: rgb(26, 64, 135);
                            border-radius: 10px;
                            visibility: visible;
                            animation-duration: 1.5s;
                            animation-delay: 700ms;
                            animation-name: fadeInUp;">
                            <i class="fas fa-balance-scale fa-3x text-white"></i>
                        <div class="ps-4">
                            <h4 class="mb-0 text-light" >Regulasi</h4>
                            <h6 class="text-light mb-0">Layanan Standar Pelayanan & Regulasi Dinas </h6>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 wow zoomIn pb-10" data-wow-delay="0.1s">
                    <div class="shadow d-flex align-items-center justify-content-center p-4" style="background-color: rgb(26, 64, 135);
                            border-radius: 10px;
                            visibility: visible;
                            animation-duration: 1.5s;
                            animation-delay: 700ms;
                            animation-name: fadeInUp;">
                            <i class="fas fa-headset fa-3x text-white"></i>
                        
                        <div class="ps-4">
                            <h4 class="mb-0 text-white">PPID</h4>
                            <h6 class="text-white mb-0">Pusat Pengelolahan Informasi Dinas Pendidikan & Kebudayaan</h6>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
     <!-- About Start -->
     <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s">
        <div class="container py-5">
            <div class="row g-5">
                <div class="col-lg-5">
                    <div class="section-title position-relative pb-3 mb-5">
                        <h5 class="fw-bold text-primary text-uppercase">Profil Kami</h5>
                        <h3 class="mb-0">Dinas Pendidikan dan Kebudayaan Provinsi Papua Tengah</h3>
                    </div>
                    <p class="mb-4">Dinas Pendidikan dan Kebudayaan Provinsi Papua Tengah Memiliki Tujuan Untuk Mewujudkan Pendidikan Yang Kuat Akan Normal & Pembentukan Mental Generasi Bangsa dan Menjadikan Papua Tengah Cerdas</p>
                    <div class="row g-0 mb-3">
                        <div class="link-animated d-flex flex-column justify-content-start">
                            <div class="col-sm-6 link-animated d-flex flex-column justify-content-start" data-wow-delay="0.2s">
                                <a href=""><h5 class="mb-2"><i class="bi bi-arrow-right text-primary me-2"></i>Visi & Misi</h5></a>
                                <a href=""><h5 class="mb-2"><i class="bi bi-arrow-right text-primary me-2"></i>Tugas & Fungsi</h5></a>
                            </div>
                            <div class="col-sm-6 link-animated d-flex flex-column justify-content-start" data-wow-delay="0.4s">
                                <a href=""><h5 class="mb-2"><i class="bi bi-arrow-right text-primary me-2"></i>Struktur Organisasi</h5></a>
                                <a href=""><h5 class="mb-2"><i class="bi bi-arrow-right text-primary me-2"></i>Profil Pejabat</h5></a>
                            </div>
                        </div>
                        
                    </div>
                    <a href="blog.php" class="btn btn-primary py-3 px-5 mt-3 wow zoomIn" data-wow-delay="0.9s">Informasi PPID</a>
                </div>
                <div class="col-lg-7" style="min-height: 500px;">
                    <div class="position-relative h-100">
                        <video  autoplay muted style="opacity: 0.7; width: 100%;">
                            <source src="vid/INFO PPBD.mp4" type="video/mp4">
                        </video>
                        <img src="vid/under-video.png" class="img-fluid">
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- About End -->
   

    <div class="tp-cta-4 pt-120  fix p-relative" data-background="img/slider/slide1.png" style="background: url(&quot;img/slider/slide1.png&quot;)  center center no-repeat fixed rgba(26, 64, 135, 0.63); ">
      <div class="tp-cta-4__shap "></div>
      <div class="container py-5 ">
            <div class="section-title text-center position-relative pb-3 mb-5 mx-auto" style="max-width: 600px;">
                <h5 class="fw-bold text-light text-uppercase">Layanan</h5>
                <h3 class="text-white mb-0">Dinas Pendidikan dan Kebudayaan Provinsi Papua Tengah</h3>
            </div>
            <div class="row g-5">
                <div class="col-lg-4">
                    <div class="row g-5">
                        <div class="col-12 wow zoomIn " data-wow-delay="0.2s" align="right">
                            <div class="bg-success rounded d-flex align-items-center justify-content-center mb-3" style="width: 60px; height: 60px;">
                                <h1 class="text-light">1</h1>
                            </div>
                            <h4 class="text-white">Standar Pelayanan</h4>
                            <p class="mb-0" style="color:white;">Daftar SOP ( Standar Operasional Prosedur ) Pelayanan Dinas Pendidikan dan Kebudayaan Provinsi Papua Tengah</p>
                        </div>
                        <div class="col-12 wow zoomIn" data-wow-delay="0.6s" align="right">
                            <div class="bg-success rounded d-flex align-items-center justify-content-center mb-3" style="width: 60px; height: 60px;">
                                <h1 class="text-light">2</h1>
                            </div>
                            <h4 class="text-white">Pengaduan Publik</h4>
                            <p class="mb-0" style="color:white;">Layanan Pengaduan Publik Dinas Pendidikan dan Kebudayaan Provinsi Papua Tengah</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4  wow zoomIn" data-wow-delay="0.9s" style="min-height: 350px;">
                    <div class="position-relative h-100">
                        <img class="position-absolute w-100 h-100 wow zoomIn" data-wow-delay="0.1s" src="img/logo/logo-animate.gif" style="object-fit: cover; border-radius: 10%;" >
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row g-5">
                        <div class="col-12 wow zoomIn" data-wow-delay="0.4s">
                            <div class="bg-success rounded d-flex align-items-center justify-content-center mb-3" style="width: 60px; height: 60px;"">
                                <h1 class="text-light">3</h1>
                            </div>
                            <h4 class="text-white">Layanan Akses Informasi Publik</h4>
                            <p class="mb-0" style="color:white;">Layanan Akses informasi Dinas Pendidikan dan Kebudayaan Provinsi Papua Tengah</p>
                        </div>
                        <div class="col-12 wow zoomIn" data-wow-delay="0.8s">
                            <div class="bg-success rounded d-flex align-items-center justify-content-center mb-3" style="width: 60px; height: 60px;"">
                                <h1 class="text-light">4</h1>
                            </div>
                            <h4 class="text-white">Layanan Permohonan Informasi</h4>
                            <p class="mb-0" style="color:white;">Formulir Pengajuan Informasi Dinas Pendidikan Provinsi Papua Tengah</p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="pb-3 mb-5"><center><a href="" class="btn btn-outline-light py-md-3 px-md-5 animated slideInRight" >Layanan Kami</a> <a href="contact.php" class="btn btn-outline-light py-md-3 px-md-5 animated slideInRight" >Hubungi Kami</a></center></div>
   </div>

   <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s">
   <div class="container py-5">
            <div class="section-title text-center position-relative pb-3 mb-5 mx-auto" style="max-width: 600px;">
                        <h3 class="text-dark mb-0">Berita Terkini Dinas Pendidikan</h3>
            </div>
                <h6 align='right'><a href="blog.php"><i class="fa fa-list"></i> Lihat Berita Selengkapnya</a></h6>
                <hr>
            <div class="row">
                <div class="col-md-8">
                    <div class="owl-carousel testimonial-carousel wow fadeInUp" data-wow-delay="0.6s">
                            <div class="testimonial-item bg-light my-4">
                                <div class="blog-item bg-light rounded overflow-hidden">
                                    <div class="blog-img position-relative overflow-hidden">
                                        <img class="img-fluid" src="img/blog/blog3.png" alt="">
                                    </div>
                                    <div class="p-4">
                                        <div class="d-flex mb-3">
                                            <small class="me-3"><i class="far fa-calendar-alt text-primary me-2"></i>01 Jan, 2023</small>
                                            <small class="me-3"><i class="fa fa-eye text-primary"></i> 1.256 Views</small>
                                        </div>
                                        <p>Dilantik Jadi Kepala Dinas Pendidikan Provinsi Papua Tengah, Ini Harapan Apolos Bagau</p>
                                        
                                        <a class="text-uppercase" href="">Baca Berita <i class="bi bi-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="testimonial-item bg-light my-4">
                                <div class="blog-item bg-light rounded overflow-hidden">
                                    <div class="blog-img position-relative overflow-hidden">
                                        <img class="img-fluid" src="img/blog/blog1.png" alt="">
                                    </div>
                                    <div class="p-4">
                                        <div class="d-flex mb-3">
                                            <small class="me-3"><i class="far fa-calendar-alt text-primary me-2"></i>01 Jan, 2023</small>
                                            <small class="me-3"><i class="fa fa-eye text-primary"></i> 1.256 Views</small>
                                        </div>
                                        <p>Dolor et eos labore stet justo sed est sed sed sed dolor stet amet</p>
                                        
                                        <a class="text-uppercase" href="detail_blog.php">Baca Berita <i class="bi bi-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>

                            <div class="testimonial-item bg-light my-4">
                                <div class="blog-item bg-light rounded overflow-hidden">
                                    <div class="blog-img position-relative overflow-hidden">
                                        <img class="img-fluid" src="img/blog/blog2.png" alt="">
                                    </div>
                                    <div class="p-4">
                                        <div class="d-flex mb-3">
                                            <small class="me-3"><i class="far fa-calendar-alt text-primary me-2"></i>01 Jan, 2023</small>
                                            <small class="me-3"><i class="fa fa-eye text-primary"></i> 1.256 Views</small>
                                        </div>
                                        <p>Dolor et eos labore stet justo sed est sed sed sed dolor stet amet</p>
                                        
                                        <a class="text-uppercase" href="detail_blog.php">Baca Berita <i class="bi bi-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>

                            <div class="testimonial-item bg-light my-4">
                                <div class="blog-item bg-light rounded overflow-hidden">
                                    <div class="blog-img position-relative overflow-hidden">
                                        <img class="img-fluid" src="img/blog/blog7.png" alt="">
                                    </div>
                                    <div class="p-4">
                                        <div class="d-flex mb-3">
                                            <small class="me-3"><i class="far fa-calendar-alt text-primary me-2"></i>01 Jan, 2023</small>
                                            <small class="me-3"><i class="fa fa-eye text-primary"></i> 1.256 Views</small>
                                        </div>
                                        <p>Dolor et eos labore stet justo sed est sed sed sed dolor stet amet</p>
                                        
                                        <a class="text-uppercase" href="detail_blog.php">Baca Berita <i class="bi bi-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="d-flex rounded overflow-hidden mb-3">
                        <img class="img-fluid" src="img/blog/blog1.png" style="width: 100px; height: 100px; object-fit: cover;" alt="">
                        
                        <a href="detail_blog.php" class="h6 fw-semi-bold d-flex align-items-center bg-light px-3 mb-0">
                        Dilantik Jadi Kepala Dinas Pendidikan Provinsi Papua Tengah, Ini Harapan Apolos Bagau</a>
                        
                    </div>
                    <div class="d-flex rounded overflow-hidden mb-3">
                        <img class="img-fluid" src="img/blog/blog2.png" style="width: 100px; height: 100px; object-fit: cover;" alt="">
                        
                        <a href="detail_blog.php" class="h6 fw-semi-bold d-flex align-items-center bg-light px-3 mb-0">
                        Dilantik Jadi Kepala Dinas Pendidikan Provinsi Papua Tengah, Ini Harapan Apolos Bagau</a>
                        
                    </div>
                    <div class="d-flex rounded overflow-hidden mb-3">
                        <img class="img-fluid" src="img/blog/blog3.png" style="width: 100px; height: 100px; object-fit: cover;" alt="">
                        
                        <a href="detail_blog.php" class="h6 fw-semi-bold d-flex align-items-center bg-light px-3 mb-0">
                        Dilantik Jadi Kepala Dinas Pendidikan Provinsi Papua Tengah, Ini Harapan Apolos Bagau</a>
                        
                    </div>
                    <div class="d-flex rounded overflow-hidden mb-3">
                        <img class="img-fluid" src="img/blog/blog4.png" style="width: 100px; height: 100px; object-fit: cover;" alt="">
                        
                        <a href="detail_blog.php" class="h6 fw-semi-bold d-flex align-items-center bg-light px-3 mb-0">
                        Dilantik Jadi Kepala Dinas Pendidikan Provinsi Papua Tengah, Ini Harapan Apolos Bagau</a>
                        
                    </div>
                    <div class="d-flex rounded overflow-hidden mb-3">
                        <img class="img-fluid" src="img/blog/blog5.png" style="width: 100px; height: 100px; object-fit: cover;" alt="">
                        
                        <a href="detail_blog.php" class="h6 fw-semi-bold d-flex align-items-center bg-light px-3 mb-0">
                        Dilantik Jadi Kepala Dinas Pendidikan Provinsi Papua Tengah, Ini Harapan Apolos Bagau</a>
                        
                    </div>
            </div>
        </div>
      </div>    
    </div>

    <!-- Service Start -->
    <div class="tp-cta-4 pt-120  fix p-relative" data-background="img/bg-service.png" style="background: url(&quot;img/bg-service.png&quot;)  center center no-repeat fixed rgba(26, 64, 135, 0.63); ">
     
        <div class="container py-5">
                <div class="section-title text-center position-relative pb-3 mb-5 mx-auto" style="max-width: 600px;">
                    <h3 class="text-white mb-0">Aksesbilitas Aplikasi Terintegrasi</h3>
                </div>
                <div class="row g-5">
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.3s" >
                        <div class="service-item rounded d-flex flex-column align-items-center justify-content-center text-center" style="background: rgb(34,193,195);
                        background: linear-gradient(0deg, rgba(34,193,195,1) 0%, rgba(253,187,45,1) 100%);">
                            <img src="img/logo-500x500.png" class="img-fluid" width="40%"><br>
                            <!-- <div class="service-icon">
                                <i class="fa fa-shield-alt text-white"></i>
                            </div> -->
                            <h5 class="text-white mb-3">PORTAL RESMI PEMPROV PAPUA TENGAH</h5>
                            <!-- <p class="m-0">PAPUA TENGAH</p> -->
                            <a class="btn btn-lg btn-primary rounded" href="">
                                <i class="bi bi-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
                        <div class="service-item  rounded d-flex flex-column align-items-center justify-content-center text-center" style="background-color: #045de9;
                        background-image: linear-gradient(315deg, #045de9 0%, #09c6f9 74%);">
                            <!-- <div class="service-icon">
                                <i class="fa fa-chart-pie text-white"></i>
                            </div> -->
                            <img src="img/logo/logo-kemendikbud.png" class="img-fluid" width="40%"><br>
                            <h5 class="text-white mb-3">PORTAL RESMI KEMENDIKBUDRISTEK</h5>
                            <!-- <p class="m-0">Amet justo dolor lorem kasd amet magna sea stet eos vero lorem ipsum dolore sed</p> -->
                            <a class="btn btn-lg btn-primary rounded" href="">
                                <i class="bi bi-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.9s">
                        <div class="service-item  rounded d-flex flex-column align-items-center justify-content-center text-center" style="background-color: #045de9;
                        background-image: linear-gradient(315deg, #045de9 0%, #09c6f9 74%);">
                            <!-- <div class="service-icon">
                                <i class="fa fa-code text-white"></i>
                            </div> -->
                            <img src="img/logo/logo-kemendikbud.png" class="img-fluid" width="40%"><br>
                            <h5 class="text-white mb-3">DATA POKOK PENDIDIKAN</h5>
                            <p class="text-white m-0">DIKDASMEN KEMENDIKBUDRISTEK</p>
                            <a class="btn btn-lg btn-primary rounded" href="">
                                <i class="bi bi-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.3s">
                        <div class="service-item  rounded d-flex flex-column align-items-center justify-content-center text-center" style="background-color: #045de9;
                        background-image: linear-gradient(315deg, #045de9 0%, #09c6f9 74%);">
                            <!-- <div class="service-icon">
                                <i class="fab fa-android text-white"></i>
                            </div> -->
                            <img src="img/logo/logo-kemendikbud.png" class="img-fluid" width="40%"><br>
                            <h5 class="text-white mb-3">DIRJEN VOKASI</h5>
                            <p class="text-white m-0">KEMENDIKBUDRISTEK</p>
                            <a class="btn btn-lg btn-primary rounded" href="">
                                <i class="bi bi-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
                        <div class="service-item rounded d-flex flex-column align-items-center justify-content-center text-center" style="background-color: #045de9;
                        background-image: linear-gradient(315deg, #045de9 0%, #09c6f9 74%);">
                            <!-- <div class="service-icon">
                                <i class="fa fa-search text-white"></i>
                            </div> -->
                            <img src="img/logo/logo-kemendikbud.png" class="img-fluid" width="40%"><br>
                            <h5 class="text-white mb-3">PDSP KEMENDIKBUDRISTEK</h5>
                            <!-- <p class="m-0">Amet justo dolor lorem kasd amet magna sea stet eos vero lorem ipsum dolore sed</p> -->
                            <a class="btn btn-lg btn-primary rounded" href="">
                                <i class="bi bi-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
                        <div class="service-item rounded d-flex flex-column align-items-center justify-content-center text-center" style="background-color: #045de9;
                        background-image: linear-gradient(315deg, #045de9 0%, #09c6f9 74%);">
                            <!-- <div class="service-icon">
                                <i class="fa fa-search text-white"></i>
                            </div> -->
                            <img src="img/logo/logo-kemendikbud.png" class="img-fluid" width="40%"><br>
                            <h5 class="text-white mb-3">BOS KEMENDIKBUDRISTEK</h5>
                            <!-- <p class="m-0">Amet justo dolor lorem kasd amet magna sea stet eos vero lorem ipsum dolore sed</p> -->
                            <a class="btn btn-lg btn-primary rounded" href="">
                                <i class="bi bi-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    
                
                </div>
            </div>           
        
   </div>
   <!-- Blog Start -->
   <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s">
        <div class="container py-5">
            <div class="section-title text-center position-relative pb-3 mb-5 mx-auto" style="max-width: 600px;">
                <!-- <h5 class="fw-bold text-primary text-uppercase">Dinas Pendidikan dan Kebudayaan Provinsi Papua Tengah</h5> -->
                <h3 class="mb-0">Agenda Kegiatan </h3>
                
            </div>
            <h6 align='right'><a href="agenda.php"><i class="fa fa-list"></i> Lihat Agenda Selengkapnya</a></h6>
                <hr>
            <div class="row g-5">
                <div class="col-lg-4 wow slideInUp" data-wow-delay="0.3s">
                    <div class="blog-item bg-light rounded overflow-hidden">
                        <div class="blog-img position-relative overflow-hidden">
                            <img class="img-fluid" src="img/blog/agenda1.png" alt="">
                            <!-- <a class="position-absolute top-0 start-0 bg-primary text-white rounded-end mt-5 py-2 px-4" href="">Web Design</a> -->
                        </div>
                        <div class="p-4">
                            <div class="d-flex mb-3">
                                <small class="me-3"><i class="far fa-user text-primary me-2"></i>Admin</small>
                                <small><i class="far fa-calendar-alt text-primary me-2"></i>01 Jan, 2023</small>
                            </div>
                            <h4 class="mb-3">Webinar Digitalisasi Sekolah</h4>
                            <p>Pemanfaatan TIK di Sekolah Dalam Rangka Digitalisasi Pendidikan Era Merdeka Belajar</p>
                            <a class="text-uppercase" href="agenda.php">Baca Selengkapnya <i class="bi bi-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 wow slideInUp" data-wow-delay="0.6s">
                    <div class="blog-item bg-light rounded overflow-hidden">
                        <div class="blog-img position-relative overflow-hidden">
                            <img class="img-fluid" src="img/blog/agenda2.png" alt="">
                            <!-- <a class="position-absolute top-0 start-0 bg-primary text-white rounded-end mt-5 py-2 px-4" href="">Web Design</a> -->
                        </div>
                        <div class="p-4">
                            <div class="d-flex mb-3">
                                <small class="me-3"><i class="far fa-user text-primary me-2"></i>Admin</small>
                                <small><i class="far fa-calendar-alt text-primary me-2"></i>01 Jan, 2023</small>
                            </div>
                            <h4 class="mb-3">Pelatihan Fun Coding Untuk Anak - Anak</h4>
                            <p>Daftarkan Dirimu untuk mengikuti pelatihan fun coding anak-anak</p>
                            <a class="text-uppercase" href="agenda.php">Baca Selengkapnya <i class="bi bi-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 wow slideInUp" data-wow-delay="0.9s">
                    <div class="blog-item bg-light rounded overflow-hidden">
                        <div class="blog-img position-relative overflow-hidden">
                            <img class="img-fluid" src="img/blog/agenda3.png" alt="">
                            <!-- <a class="position-absolute top-0 start-0 bg-primary text-white rounded-end mt-5 py-2 px-4" href="">Web Design</a> -->
                        </div>
                        <div class="p-4">
                            <div class="d-flex mb-3">
                                <small class="me-3"><i class="far fa-user text-primary me-2"></i>Admin</small>
                                <small><i class="far fa-calendar-alt text-primary me-2"></i>01 Jan, 2023</small>
                            </div>
                            <h4 class="mb-3">Mendikbud Tetapkan Empat Pokok Kebijakan Pendidikan “Merdeka Belajar”</h4>
                            <p>Pokok - pokok Kebijakan Merdeka Belajar</p>
                            <a class="text-uppercase" href="agenda.php">Baca Selengkapnya <i class="bi bi-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Quote Start -->
 <div class="tp-cta-4 pt-120  fix p-relative" data-background="img/inovasi/bg-inovasi03.png" style="background: url(&quot;img/inovasi/bg-inovasi03.png&quot;)  center center no-repeat fixed rgba(26, 64, 135, 0.63); background-size: cover; background-position: center; ">
    <div class="container py-5">
        <div class="row g-5">
            
            <div class="col-lg-7">
                <div class="section-title position-relative pb-3 mb-5">
                    <h5 class="fw-bold text-primary text-uppercase">Info INOVASI</h5>
                    <h3 class="text-white mb-0">Pekan Inovasi Papua Tengah Cerdas 2023</h3>
                </div>
                <div class="row gx-3">
                    <div class="col-sm-6 wow zoomIn" data-wow-delay="0.2s">
                        <h5 class="text-white mb-4"><i class="fa fa-calendar text-primary me-3"></i>Reply within 24 hours</h5>
                    </div>
                    <div class="col-sm-6 wow zoomIn" data-wow-delay="0.4s">
                        <h5 class="text-white mb-4"><i class="fa fa-clock text-primary me-3"></i>24 hrs telephone support</h5>
                    </div>
                </div>
                <p class="mb-4 text-white">
                    Pekan Inovasi Pendidikan Papua Tengah adalah sebuah acara yang bertujuan untuk mendorong inovasi dalam dunia pendidikan dan menggali potensi yang ada di wilayah Papua Tengah. Acara ini akan menjadi wadah bagi para pendidik, peneliti, praktisi, dan pemangku kepentingan pendidikan untuk berbagi ide, pengetahuan, dan pengalaman dalam mewujudkan pendidikan yang lebih baik di wilayah ini.
                </p>
                <h5 class="text-white">Agenda Utama:</h5>
                <p>
                    <ol>
                        <li class="text-white">Pameran Inovasi Pendidikan: Temukan ide-ide kreatif dan praktik terbaik dalam pendidikan melalui pameran inovasi yang akan menampilkan berbagai proyek dan solusi inovatif.</li>
                        <li class="text-white">Seminar dan Diskusi Panel: Dengarkan pembicara-pembicara inspiratif dari dunia pendidikan yang akan berbagi pandangan dan pengetahuan tentang tren terbaru, tantangan, dan peluang di bidang pendidikan.</li>
                        <li class="text-white">Workshop dan Pelatihan: Ikuti workshop interaktif yang akan membantu meningkatkan keterampilan dan pengetahuan Anda dalam menerapkan inovasi dalam pembelajaran.</li>
                        <li class="text-white">Presentasi Penelitian: Simak presentasi penelitian terkini yang akan memberikan wawasan mendalam tentang isu-isu pendidikan yang relevan di Papua Tengah.</li>
                        <li class="text-white">Jaringan dan Kolaborasi: Manfaatkan kesempatan untuk menjalin hubungan, berkolaborasi, dan berbagi pengalaman dengan sesama profesional pendidikan.</li>
                    </ol>
                </p>

                <p  class="mb-4 text-white">
                    Jangan lewatkan kesempatan ini untuk bergabung dalam Pekan Inovasi Pendidikan Papua Tengah yang akan menginspirasi dan memberikan pemahaman yang lebih dalam tentang potensi pendidikan di wilayah ini. Bersama-sama kita dapat menciptakan perubahan positif dan membangun masa depan pendidikan yang lebih baik.
                </p>
                   
               
            </div>
            <div class="col-lg-5">
                <img src="img/inovasi/inovasi.png" class="img-fluid wow zoomIn rounded" data-wow-delay="0.9s">
            </div>
        </div>
    </div>
</div>
<!-- Quote End -->

    
<!-- Quote Start -->
<div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" style="background: url('img/bgippbd.png') no-repeat rgba(9, 30, 62, 0.7); visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
    <div class="container py-5">
        <div class="row g-5">
            <div class="col-lg-5">
                <!-- <div class="bg-info rounded h-100 d-flex align-items-center p-5 wow zoomIn" data-wow-delay="0.9s"> -->
                   <img src="img/blog/banner-ppbd.png" class="img-fluid wow zoomIn rounded" data-wow-delay="0.9s">
                <!-- </div> -->
            </div>
            <div class="col-lg-7">
                <div class="section-title position-relative pb-3 mb-5">
                    <h5 class="fw-bold text-white text-uppercase">Info PPDB</h5>
                    <h3 class="text-white mb-0">Informasi Penerimaan Peserta Didik Baru </h3>
                </div>
                <div class="row gx-3">
                    <div class="col-sm-6 wow zoomIn" data-wow-delay="0.2s">
                        <h5 class="text-white mb-4"><i class="fa fa-clock text-white me-3"></i>24 Juli 2023</h5>
                    </div>
                    <div class="col-sm-6 wow zoomIn" data-wow-delay="0.4s">
                        <h5 class="text-white mb-4"><i class="fa fa-phone text-white me-3"></i>+62 123 456 765</h5>
                    </div>
                </div>
                    <h5 class="mb-4 text-white">Penerimaan Peserta Didik Baru Provinsi Papua Tengah</h5>
                    <p class="mb-4 text-white">Penerimaan Peserta Didik Baru (PPDB) di Provinsi Papua Tengah adalah proses seleksi untuk menerima siswa baru dalam sistem pendidikan. Dalam rangka memberikan kesempatan yang adil bagi semua calon siswa, PPDB Provinsi Papua Tengah menyediakan beberapa jalur penerimaan, salah satunya adalah jalur Afirmasi (KETM).</p>
                    <p class="mb-4 text-white">Jalur Afirmasi (KETM) adalah jalur khusus dalam PPDB Provinsi Papua Tengah yang ditujukan untuk memberikan kesempatan kepada calon siswa dengan latar belakang yang kurang mampu atau berasal dari daerah terpencil. Melalui jalur ini, pemerintah daerah berupaya untuk memastikan bahwa setiap anak memiliki akses yang sama terhadap pendidikan berkualitas.</p>
                    <p class="mb-4 text-white">Dalam jalur Afirmasi (KETM), calon siswa akan melalui proses seleksi yang melibatkan verifikasi dan validasi dokumen pendukung. Calon siswa yang memenuhi kriteria dan syarat yang ditentukan akan mendapatkan prioritas dalam penerimaan.</p>
                    <a href="PPBD/" class="btn btn-outline-light py-md-3 px-md-5 animated slideInRight"><i class="fa fa-eye"></i> Lihat Selengkapnya PPBD</a>
                    
            </div>
            
        </div>
    </div>
</div>
<!-- Quote End -->
<?php include 'footer.php'; ?>
    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square rounded back-to-top"><i class="bi bi-arrow-up"></i></a>
    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/wow/wow.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/counterup/counterup.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>
    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>
</html>