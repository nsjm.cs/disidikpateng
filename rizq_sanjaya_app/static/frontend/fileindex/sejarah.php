<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Dinas Pendidikan - Provinsi Papua Tengah</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Dinas Pendidikan - Provinsi Papua Tengah" name="keywords">
    <meta content="Dinas Pendidikan - Provinsi Papua Tengah" name="description">
    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700;800&family=Rubik:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <!-- Spinner Start -->
    <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
        <div class="spinner"></div>
    </div>
    <!-- Spinner End -->
    <!-- Navbar Start -->
    <div class="container-fluid position-relative p-0">
        <?php include 'menu.php'; ?>
        <!-- start banner  - berita -->
        <div class="container-fluid bg-primary py-5 bg-header" style="margin-bottom: 90px;">
            <div class="row py-5">
                <div class="col-12 pt-lg-5 mt-lg-5 text-center">
                    <h5 class="display-5 text-white animated zoomIn">Sejarah Dinas</h5>
                    <a href="" class="h5 text-white">Dinas Pendidikan & Kebudayaan Provinsi Papua Tengah</a>
                </div>
            </div>
        </div>
        <!-- end banner  - berita -->
    </div>
    <!-- Navbar End -->

    <!-- Full Screen Search Start -->
    <div class="modal fade" id="searchModal" tabindex="-1">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content" style="background: rgba(9, 30, 62, .7);">
                <div class="modal-header border-0">
                    <button type="button" class="btn bg-white btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body d-flex align-items-center justify-content-center">
                    <div class="input-group" style="max-width: 600px;">
                        <input type="text" class="form-control bg-transparent border-primary p-3" placeholder="Type search keyword">
                        <button class="btn btn-primary px-4"><i class="bi bi-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Full Screen Search End -->
 <!-- About Start -->
 <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s">
        <div class="container py-5">
            <div class="row g-5">
                <div class="col-lg-7">
                    <div class="section-title position-relative pb-3 mb-5">
                        <h5 class="fw-bold text-primary text-uppercase">Sejarah</h5>
                        <h1 class="mb-0">Dinas Pendidikan & Kebudayaan Provinsi Papua Tengah</h1>
                    </div>
                    <p>Papua Tengah adalah sebuah provinsi di Indonesia yang telah dimekarkan dari Provinsi Papua pada tahun 2022.<sup>[5]</sup> Ibu kota provinsi ini berada di Kabupaten Nabire.<sup>[6][7]</sup> Papua Tengah dimekarkan dari Provinsi Papua bersama dua provinsi lainnya yakni Papua Pegunungan dan Papua Selatan pada 30 Juni 2022 berdasarkan Undang-Undang Nomor 15 Tahun 2022.<sup>[8]</sup> Cakupan wilayah Papua Tengah kira-kira sesuai dengan wilayah adat Mee Pago dan Saireri.<sup>[9][10][11]</sup></p>
                    <p>Kabupaten Nabire di bagian utara Papua Tengah merupakan dataran rendah berbatasan langsung dengan Taman Nasional Teluk Cenderawasih yang memiliki potensi pariwisata bahari seperti terumbu karang, pulau-pulau berpasir putih dan hiu paus.<sup>[12]</sup> Bagian tengah Papua Tengah terdapat kawasan Danau Paniai dan Pegunungan Jayawijaya. Di provinsi ini terdapat gunung tertinggi di Indonesia yaitu Puncak Jaya serta tambang emas Grasberg yang dioperasikan oleh Freeport Indonesia.<sup>[13][14]</sup> Bagian selatan Papua Tengah adalah Kabupaten Mimika dengan ibu kotanya di Timika, yang merupakan salah satu kota besar di Pulau Papua. Sedangkan topografi Mimika berupa rawa-rawa, sungai, dan pantai.<sup>[15]</sup></p>

                    <div class="d-flex align-items-center mb-4 wow fadeIn" data-wow-delay="0.6s">
                        <div class="bg-primary d-flex align-items-center justify-content-center rounded" style="width: 60px; height: 60px;">
                            <i class="fa fa-comment-alt text-white"></i>
                        </div>
                        <div class="ps-4">
                            <h5 class="mb-2">Dinas Pendidikan dan Kebudayaan Provinsi Papua</h5>
                        </div>
                    </div>
                   
                </div>
                <div class="col-lg-5" style="min-height: 500px;">
                    <div class="position-relative h-100">
                        <img class="position-absolute w-100 h-100 rounded wow zoomIn " data-wow-delay="0.9s" src="img/logo/logo-animate.gif" style="object-fit: cover; box-shadow: 0 4px 10px rgba(0, 0, 0, 0.4);">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About End -->

    
    <?php include 'footer.php'; ?>
    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square rounded back-to-top"><i class="bi bi-arrow-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/wow/wow.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/counterup/counterup.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>
    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>