

    <!-- Vendor Start -->
    <div class="container-fluid wow fadeInUp" data-wow-delay="0.1s">
        <div class="container" style="padding-top:30px;">
            <div class="bg-white fixed">
                <div class="owl-carousel vendor-carousel">
                    <img src="img/logo/FOOTER/1.png" class='img-fluid'>
                    <img src="img/logo/FOOTER/2.png" class='img-fluid'>
                    <img src="img/logo/FOOTER/3.png" class='img-fluid'>
                    <img src="img/logo/FOOTER/4.png" class='img-fluid'>
                    <img src="img/logo/FOOTER/5.png" class='img-fluid'>
                    <img src="img/logo/FOOTER/6.png" class='img-fluid'>
                    <img src="img/logo/FOOTER/7.png" class='img-fluid'>
                    <img src="img/logo/FOOTER/8.png" class='img-fluid'>
                    <img src="img/logo/FOOTER/9.png" class='img-fluid'>
                   
                </div>
            </div>
        </div>
    </div>
    <!-- Vendor End -->
    

    <!-- Footer Start -->
    <div class="container-fluid bg-dark text-light mt-5 wow fadeInUp" data-wow-delay="0.1s">
        <div class="container">
            <div class="row gx-5">
                <div class="col-lg-4 col-md-6">
                    <div class="d-flex flex-column align-items-center justify-content-center text-center h-100 p-4">
                        <a href="index.php" class="navbar-brand">
                            <img src="img/logo/logo-footer.png" alt="" class="img-fluid m-0">
                        </a>
                        <p class="mt-3 mb-4" align='left'>Jl. Pepera No.11, Siriwini, Nabire Papua, Kabupaten Nabire, Papua 98815</p>
                        <form action="">
                            <div class="input-group">
                                <input type="text" class="form-control border-white p-3" disabled placeholder="Layanan Pengaduan">
                                <a href="www.lapor.go.id" class="btn btn-danger" target="_blank">
                                  <img src="img/laporgoid.png" width="100">
                               </a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-8 col-md-6">
                    <div class="row gx-5">
                        <div class="col-lg-4 col-md-12 pt-5 mb-5">
                            <div class="section-title section-title-sm position-relative pb-3 mb-4">
                                <h6 class="text-light mb-0">Kontak Kami</h6>
                            </div>
                            <div class="d-flex mb-2">
                                <i class="bi bi-geo-alt text-primary me-2"></i>
                                <p class="mb-0" style="font-size:13px;">Jl. Pepera No.11, Siriwini, Nabire Papua Tengah 98815</p>
                            </div>
                            <div class="d-flex mb-2">
                                <i class="bi bi-envelope-open text-primary me-2"></i>
                                <p class="mb-0" style="font-size:13px;">disidikpateng@info.com</p>
                            </div>
                            <div class="d-flex mb-2">
                                <i class="bi bi-telephone text-primary me-2"></i>
                                <p class="mb-0" style="font-size:13px;">+62 85 226 213 902</p>
                            </div>
                            <div class="d-flex mt-4">
                                <a class="btn btn-primary btn-square me-2" href="#"><i class="fab fa-twitter fw-normal"></i></a>
                                <a class="btn btn-primary btn-square me-2" href="#"><i class="fab fa-youtube fw-normal"></i></a>
                                <a class="btn btn-primary btn-square me-2" href="#"><i class="fab fa-instagram fw-normal"></i></a>
                                <a class="btn btn-primary btn-square" href="#"><i class="fab fa-facebook-f fw-normal"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 pt-0 pt-lg-5 mb-5">
                            <div class="section-title section-title-sm position-relative pb-3 mb-4">
                                <h6 class="text-light mb-0">Quick Links</h6>
                            </div>
                            <div class="link-animated d-flex flex-column justify-content-start">
                                <a class="text-light mb-2" style="font-size:13px;" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Home</a>
                                <a class="text-light mb-2" style="font-size:13px;" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>PPID</a>
                                <a class="text-light mb-2" style="font-size:13px;" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>PPBD</a>
                                <a class="text-light mb-2" style="font-size:13px;" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Inovasi</a>
                                <a class="text-light mb-2" style="font-size:13px;" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Statistik</a>
                                <a class="text-light" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Kontak</a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 pt-0 pt-lg-5 mb-5">
                            <div class="section-title section-title-sm position-relative pb-3 mb-4">
                                <h6 class="text-light mb-0">Tautan Lainnya</h6>
                            </div>
                            <div class="link-animated d-flex flex-column justify-content-start">
                                <a class="text-light mb-2" style="font-size:13px;" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Home</a>
                                <a class="text-light mb-2" style="font-size:13px;" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Infografis</a>
                                <a class="text-light mb-2" style="font-size:13px;" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Kalender Pendidikan</a>
                                <a class="text-light mb-2" style="font-size:13px;" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Standar Pelayanan</a>
                                <a class="text-light mb-2" style="font-size:13px;" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Pengaduan Publik</a>
                                <a class="text-light" style="font-size:13px;" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Agenda Kegiatan</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid text-white" style="background: #061429;">
        <div class="container text-center">
            <div class="row justify-content-end">
                <div class="col-lg-12">
                    <div class="d-flex align-items-center justify-content-center" style="height: 75px;">
                        <p class="mb-0" style="font-size:13px;">&copy; Copyright 2023. All Rights Reserved. 
						
						Designed by <a  href="https://sanjayateknologi.com" target="_blank" >PT. Rizq Sanjaya Teknologi</a></p>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End -->