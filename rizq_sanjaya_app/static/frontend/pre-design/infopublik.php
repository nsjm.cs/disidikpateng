<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Dinas Pendidikan - Provinsi Papua Tengah</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Dinas Pendidikan - Provinsi Papua Tengah" name="keywords">
    <meta content="Dinas Pendidikan - Provinsi Papua Tengah" name="description">
    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700;800&family=Rubik:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <!-- Spinner Start -->
    <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
        <div class="spinner"></div>
    </div>
    <!-- Spinner End -->
    <!-- Navbar Start -->
    <div class="container-fluid position-relative p-0">
        <?php include 'menu.php'; ?>
        <!-- start banner  - berita -->
        <div class="container-fluid bg-primary py-5 bg-header" style="margin-bottom: 90px;">
            <div class="row py-5">
                <div class="col-12 pt-lg-5 mt-lg-5 text-center">
                    <h5 class="display-5 text-white animated zoomIn"><i class="fa fa-bullhorn "></i> Informasi Publik</h5>
                    <a href="" class="h5 text-white">Dinas Pendidikan & Kebudayaan Provinsi Papua Tengah</a>
                </div>
            </div>
        </div>
        <!-- end banner  - berita -->
    </div>
    <!-- Navbar End -->

    <!-- Full Screen Search Start -->
    <div class="modal fade" id="searchModal" tabindex="-1">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content" style="background: rgba(9, 30, 62, .7);">
                <div class="modal-header border-0">
                    <button type="button" class="btn bg-white btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body d-flex align-items-center justify-content-center">
                    <div class="input-group" style="max-width: 600px;">
                        <input type="text" class="form-control bg-transparent border-primary p-3" placeholder="Type search keyword">
                        <button class="btn btn-primary px-4"><i class="bi bi-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Full Screen Search End -->


    <!-- Blog Start -->
    <div class="container-fluid wow fadeInUp" data-wow-delay="0.1s">
        <div class="container py-5">
            <div class="row g-5">
                <div class="col-lg-7">
                    <div class="section-title-2 position-relative pb-3 mb-5">
                        <h3 class="mb-0">Informasi Publik Dinas Pendidikan & Kebudayaan Provinsi Papua Tengah</h3>
                    </div>
                    <p class="mb-4" align='justify'>
                        Laporan akses informasi publik memuat berbagai jenis layanan di masing-masing bidang Dinas Pendidikan dan Kebudayaan Provinsi Papua Tengah. Setiap jenis layanan memuat persyaratan, alur pengurusan serta contact person pegawai yang menangani pelayanan tersebut.
                    </p>
                    <p class="mb-4" align='justify'>
                        Informasi dan Laporan terkait data pendidikan disampaikan secara resmi dan didapatkan dari data Kementrian Pendidikan Kebudayaan Riset dan Teknologi RI.
                    </p>
                    <p class="mb-4" align='right'>
                        TTD<br>
                        Kepala Dinas Pendidikan dan Kebudayaan <br>
                        Provinsi Papua Tengah
                    </p>
                    
                    <p class="mb-4" align='justify'>
                        <a href="#"><i class="fa fa-download"></i> Silahkan Donwload Data Laporan Informasi Publik!</a>
                    </p>
                    
                    
                </div>
                <div class="col-lg-5" style="min-height: 400px;">
                    <div class="position-relative h-100">
                        <img class=" rounded wow zoomIn" data-wow-delay="0.9s" src="img/blog/infopublik.png" style="object-fit: cover; visibility: visible; animation-delay: 0.9s; animation-name: zoomIn;">
                    </div>
                </div>
                </div>
        </div>
    </div>
    <!-- Blog End -->
     

    <div class="tp-cta-4 pt-120  fix p-relative" data-background="img/blog/bg-infopublik.png" style="background: url(&quot;img/blog/bg-infopublik.png&quot;)  center center no-repeat fixed rgba(26, 64, 135, 0.63); ">
      <div class="tp-cta-4__shap "></div>
      <div class="container py-5 ">
            <div class="section-title text-center position-relative pb-3 mb-5 mx-auto" style="max-width: 600px;">
                <h5 class="fw-bold text-light text-uppercase"> Informasi Publik</h5>
                <h6 class="text-white mb-0">Dinas Pendidikan dan Kebudayaan Provinsi Papua Tengah</h6>
            </div>
            <div class="row g-5">
            <div class="col-md-6 wow zoomIn" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: zoomIn;">
               <div class="service-item rounded d-flex flex-column" style="background-color:#90c132cf;">
                  <div class="service-icon shadowBas" style="background-color:#90c132cf;">
                     <i class="fas fa-graduation-cap text-white fa-3x "></i>
                  </div>
                  <h5 class="mb-2 text-white">Data Peserta Didik 8 Kabupaten</h5>
                  <p class="m-1 text-white">Berdasarkan Data Pokok kemendikbud Peserta Didik Tahun Pengajaran Semester 2022/2023 Genap </p>
                  <h3 class="text-white"><b>238.978</b></h3>
                  <a class="btn btn-lg btn-primary rounded" href="https://dapo.kemdikbud.go.id/pd/1/360000" target="balnk">
                  <i class="bi bi-arrow-right"></i>
                  </a>
               </div>
            </div>
            <div class="col-md-6 wow zoomIn" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: zoomIn;">
                <div class="service-item rounded d-flex flex-column" style="background-color:#90c132cf;">
                  <div class="service-icon shadowBas" style="background-color:#90c132cf;">
                     <i class="fas fa-users text-white fa-3x "></i>
                  </div>
                  <h5 class="mb-2 text-white">Data Peserta Didik 8 Kabupaten Berdasarkan Jenis Kelamin</h5>
                  <p class="text-white">
                    Berdasarkan Data Pokok kemendikbud Peserta Didik Tahun Pengajaran Semester 2022/2023 Genap
                  </p>
                  <p class="text-white"><i class="fas fa-male fa-2x text-white"></i> <b> 131.116 </b> | <i class="fas fa-female fa-2x text-white"></i><b> 107.862</b></p>
                  <p class="text-white"> </p>
                    
                  <a class="btn btn-lg btn-primary rounded" href="https://dapo.kemdikbud.go.id/pd/1/360000" target="balnk">
                  <i class="bi bi-arrow-right"></i>
                  </a>
               </div>
            </div>
            
         </div>
            
        </div>
        <div class="pb-3 mb-5"><center><a href="https://dapo.kemdikbud.go.id/pd/1/360000" target="balnk" class="btn btn-outline-light py-md-3 px-md-5 animated slideInRight" >Layanan Kami</a> <a href="contact.php" class="btn btn-outline-light py-md-3 px-md-5 animated slideInRight" >Hubungi Kami</a></center></div>
   </div>

    <?php include 'footer.php'; ?>
    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square rounded back-to-top"><i class="bi bi-arrow-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/wow/wow.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/counterup/counterup.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>
    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>