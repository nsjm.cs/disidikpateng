<nav class="navbar navbar-expand-lg navbar-dark px-5 py-3 py-lg-0">
            <a href="index.php" class="navbar-brand p-0 text-dark">
                <img src="img/logo/logo-brand.png" alt="" class="img-fluid" width="50%">
                <!-- DISDIK PATENG -->
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                <span class="fa fa-bars"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarCollapse">
                <div class="navbar-nav ms-auto py-0">
                    <a href="index.php" class="nav-item nav-link active">Home</a>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Profil</a>
                        <div class="dropdown-menu m-0">
                            <a href="sejarah.php" class="dropdown-item">Sejarah</a>
                            <a href="visi-misi.php" class="dropdown-item">Visi & Misi</a>
                            <a href="visi-misi.php" class="dropdown-item">Tugas & Fungsi</a>
                            <a href="visi-misi.php" class="dropdown-item">Struktur Organisasi</a>
                            <a href="pejabat.php" class="dropdown-item">Profil Pejabat</a>
                        </div>
                    </div>
                    <a href="blog.php" class="nav-item nav-link">PPID</a>
                    <a href="PPBD/index.php" target="blank" class="nav-item nav-link">PPBD</a>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Layanan</a>
                        <div class="dropdown-menu m-0">
                            <a href="blog.php" class="dropdown-item">Standar Pelayanan</a>
                            <a href="blog.php" class="dropdown-item">Pengaduan Publik</a>
                            <a href="blog.php" class="dropdown-item">Layanan Akses Informasi Publik</a>
                            <a href="blog.php" class="dropdown-item">Layanan Permohonan Informasi</a>
                        </div>
                    </div>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Informasi</a>
                        <div class="dropdown-menu m-0">
                            <a href="infografis.php" class="dropdown-item">Infografis</a>
                            <a href="infopublik.php" class="dropdown-item">Informasi Publlik</a>
                            <a href="agenda.php" class="dropdown-item">Agenda Kegiatan Dinas</a>
                            <a href="regulasi.php" class="dropdown-item">Regulasi</a>
                            <a href="kalender.php" class="dropdown-item">Kalender Pendidikan</a>
                        </div>
                    </div>
                    <a href="inovasi.php" class="nav-item nav-link">Inovasi</a>
                    <a href="blog.php" class="nav-item nav-link">Berita</a>
                    <!-- <a href="galeri.php" class="nav-item nav-link">Galeri</a> -->
                    <a href="contact.php" class="nav-item nav-link">Kontak</a>
                </div>
                <a href="#" class="btn btn-primary py-2 px-4 ms-3">Statistik</a>
            </div>
        </nav>