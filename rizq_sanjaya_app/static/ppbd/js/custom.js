// $('ul.nav').find('a').click(function(){
//     var $href = $(this).attr('href');
//     var $anchor = $($href).offset();
//     $('html, body').animate({ scrollTop: $anchor.top }, 1000);  // added duration for smooth scrolling
//     return false;
// });


/**
   * Easy selector helper function
   */
  const select = (el, all = false) => {
    el = el.trim()
    if (all) {
      return [...document.querySelectorAll(el)]
    } else {
      return document.querySelector(el)
    }
  }

  /**
   * Easy event listener function
   */
  const on = (type, el, listener, all = false) => {
    let selectEl = select(el, all)
    if (selectEl) {
      if (all) {
        selectEl.forEach(e => e.addEventListener(type, listener))
      } else {
        selectEl.addEventListener(type, listener)
      }
    }
  }

  /**
   * Easy on scroll event listener 
   */
  const onscroll = (el, listener) => {
    el.addEventListener('scroll', listener)
  }

  /**
   * Navbar links active state on scroll
   */
let navbarlinks = select('#navbar .scrollto', true)
const navbarlinksActive = () => {
let position = window.scrollY + 200
navbarlinks.forEach(navbarlink => {
  if (!navbarlink.hash) return
  let section = select(navbarlink.hash)
  if (!section) return
  if (position >= section.offsetTop && position <= (section.offsetTop + section.offsetHeight)) {
    navbarlink.classList.add('active')
  } else {
    navbarlink.classList.remove('active')
  }
})
}
window.addEventListener('load', navbarlinksActive)
onscroll(document, navbarlinksActive)

/**
   * Scrolls to an element with header offset
   */
  const scrollto = (el) => {
    let header = select('#header')
    let offset = header.offsetHeight

    let elementPos = select(el).offsetTop
    window.scrollTo({
      top: elementPos - offset,
      behavior: 'smooth'
    })
  }
/**
* Scrool with ofset on links with a class name .scrollto
*/
on('click', '.scrollto', function(e) {
if (select(this.hash)) {
  e.preventDefault()

  console.log(this.hash)

  let navbar = select('#navbar')
  if (navbar.classList.contains('navbar-mobile')) {
    navbar.classList.remove('navbar-mobile')
    let navbarToggle = select('.mobile-nav-toggle')
    navbarToggle.classList.toggle('bi-list')
    navbarToggle.classList.toggle('bi-x')
  }
  scrollto(this.hash)

} else {
  var homee = this.hash;
  var linng = window.location.href.split('/');
  var getUrls = window.location;
  var baseUrl = "//"+ getUrls.host+"/"+getUrls.pathname.split('/')[1]+"/";

  if (linng.indexOf("paket") != -1){
    window.location.href = baseUrl+homee;
  }
}
}, true);