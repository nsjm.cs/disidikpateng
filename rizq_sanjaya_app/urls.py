from django.urls import path, include
from .views import *
from django.contrib.auth import views as auth_views


app_name = 'profile'
urlpatterns = [
    path('', dashboard.index, name='home'),
    path('sejarah/', profiledinas.index, name='sejarah'),
    path('visimisi/', profiledinas.VisiMisi, name='visimisi'),
    path('tugas/', profiledinas.Tugas, name='tugas'),
    path('struktur/', profiledinas.Struktur, name='struktur'),
    path('profilpejabat/', profiledinas.Pejabat, name='pejabat'),

     # ## ADD PAKBAS =======================================================
    path('infoterpadu/', fe_infoterpadu.index_infografis, name='infoterpadu_infografis'),
    path('infopublik/', fe_infoterpadu.index_infopublik, name='infoterpadu_infopublik'),
    path('infoagenda/', fe_infoterpadu.index_infoagenda, name='infoterpadu_agenda'),
    path('infoagenda/<str:slug>', fe_infoterpadu.admin_detail_agenda, name='infoterpadu_agenda_detail'),
    path('infokalender/', fe_infoterpadu.index_infokalender, name='infoterpadu_kalender_pendidikan'),
    path('infokalender/detail/<str:slug>', fe_infoterpadu.admin_detail_kalender, name='infoterpadu_det_kalender_pendidikan'),
    path('infoterpadu/regulasi/', fe_infoterpadu.index_inforegulasi, name='infoterpadu_regulasi'),
    
    

    # Berita =============================================
    path('berita/', berita.index, name='berita'),
    path('berita/<str:slug>', berita.cat_berita, name='kat_berita'),
    path('berita/tag/<str:slug>', berita.tag_berita, name='tag_berita'),
    path('berita/detail/<str:slug>', berita.detail_berita, name='detail_berita'),

    # Layanan ==============================================
    path('layanan/', layanan.index, name='layanan'),
    path('layanan/info_publik', layanan.info_publik, name='info_publik'),
    path('layanan/pengaduan', layanan.pengaduan, name='pengaduan'),
    path('layanan/permohonan', layanan.permohonan, name='permohonan'),


    path('layanan/input_pengaduan', pengaduan.Inputpengaduan, name='input_pengaduan'),


    path('inovasi/', fe_inovasi.index, name='inovasi'),
    path('det_inovasi/<str:slug>', fe_inovasi.admin_detail, name='detail_inovasi'),
    path('bycategory/<str:slug>', fe_inovasi.admin_by_category, name='detail_inovasi_bycategory'),
    path('contact/', fe_contact.index, name='contact'),

    # ## ADD JOEL =======================================================
    path('ppid/<str:slug>', ppid.index, name='ppid_index'),
    
    #INI ADALAH DISDIK PATENG
    # path('profile/', dashboard.profil, name='profil'),
    # path('paket/', dashboard.paket, name='paket'),
    # path('detail_paket/<int:id>', dashboard.detail_paket, name='detail_paket'),
    # path('cara_bayar/', dashboard.cara_bayar, name='cara_bayar'),
    # path('gallery/', dashboard.galeri, name='galeri'),
    # path('berita/', dashboard.berita, name='berita'),
    # path('category/<int:id>', dashboard.kategori, name='kategori'),
    # path('detail_berita/<str:slug>', dashboard.detail_berita, name='detail_berita'),
    # path('contact/', dashboard.kontak, name='kontak'),
    # path('prev/<int:id>/', dashboard.prev, name='prev'),
    # path('next/<int:id>/', dashboard.next, name='next'),
    # path('Category/', dashboard.ajax, name='ajax'),
    # path('Produk/<int:id>', dashboard.produk, name='produk'),
    # path('contact_us/', dashboard.contact, name='contact'),
    # path('supplier/', dashboard.supplier, name='supplier'),
    # path('render_laporan/', dashboard.render_laporan, name='render_laporan'),
    # DOKUMEN PUBLIKASI
    # path('dokumen/', documents.index, name='document'),
    # path('dokumen/<str:slug>', documents.detail, name='document_detail'),
    # ABOUT ==================
    # path('about/', halaman.index_about, name='idx_about'),
    # KONTAK PAJERO SPORT ==================
    # path('contact/', halaman.index_contact, name='idx_contact'),
    # === Layanan ========================
    # path('layanan/', layanan.index, name='layanan'),
    # path('layanan/<str:jenis>', layanan.detail, name='layanan'),
    # path('layanan/komoditas/<str:jenis>', layanan.detail_komoditas, name='detail_komoditas'),
    # NEWS ========================================
    # path('berita/', news.index, name='news'),
    # path('berita/<str:slug>', news.detail, name='news_detail'),
    # path('berita/category/<str:jenis>/<str:slug>', news.category, name='news_by_category'),
    # DOKUMEN PUBLIKASI
    # path('galeri/', gallery.index, name='gallery'),
    # path('galeri/<str:slug>', gallery.detail, name='gallery_detail'),
    # TAGS
    path('tags/<str:jenis>/<str:slug>', tags.index, name='tags'),


    ################################################# PATH URL UNTUK ADMIN################################################# 
    path('manage_panel/', include(
        [
            path('', auth.LoginViews, name='index_login'),
            path('logout/', auth.LogoutViews, name='logout_akun'),
            path('dashboard/', admin.index, name='admin_home'),
            path('fetch_data_pengaduan/', admin.Datapengaduan, name='admin_datapengaduan'),
            path('verification/', user.verification, name='admin_verification'),
            path('send_verification/', user.send_verification, name='admin_send_verification'),
            path('email/verify/<uidb64>/<token>/',user.email_verify, name='admin_email_verify'),

            # path('modal/<str:jenis>', admin.admin_modal_archive, name='admin_modal_archive'),

            #LAYANAN PROFIL SKPD===============================
            path('profilskpd/', include(
                [
                    path('', profilskpd.admin_index, name='admin_profilskpd'),
                    path('create/', profilskpd.create, name='admin_profilskpd_create'),
                    path('edit/<str:slug>', profilskpd.edit, name='admin_profilskpd_edit'),
                    path('soft_delete/<int:id>', profilskpd.softDelete, name='admin_profilskpd_soft_delete'),
                    path('restore/<int:id>', profilskpd.restore, name='admin_profilskpd_restore'),
                    
                ]
            )),

            #LAYANAN DINAS PENDIDIKAN===============================
            path('layanandinas/', include(
                [
                    path('', layanan_.admin_index, name='admin_layanan'),
                    path('create/', layanan_.create, name='admin_layanan_create'),
                    path('edit/<int:id>', layanan_.edit, name='admin_layanan_edit'),
                    path('soft_delete/<int:id>', layanan_.softDelete, name='admin_layanan_soft_delete'),
                    path('restore/<int:id>', layanan_.restore, name='admin_layanan_restore'),
                    
                ]
            )),

             #AKSESBILITAS EXTERNAL LINK DINAS PENDIDIKAN===============================
            path('aksesbilitas/', include(
                [
                    path('', aksesbilitas.admin_index, name='admin_aksesbilitas'),
                    path('create/', aksesbilitas.create, name='admin_aksesbilitas_create'),
                    path('edit/<int:id>', aksesbilitas.edit, name='admin_aksesbilitas_edit'),
                    path('soft_delete/<int:id>', aksesbilitas.softDelete, name='admin_aksesbilitas_soft_delete'),
                    path('restore/<int:id>', aksesbilitas.restore, name='admin_aksesbilitas_restore'),
                    
                ]
            )),

             #AKSESBILITAS EXTERNAL LINK DINAS PENDIDIKAN===============================
            path('kategori_pengaduan/', include(
                [
                    path('', kat_pengaduan.admin_index, name='admin_kat_pengaduan'),
                    path('create/', kat_pengaduan.create, name='admin_kat_pengaduan_create'),
                    path('edit/<str:slug>', kat_pengaduan.edit, name='admin_kat_pengaduan_edit'),
                    path('soft_delete/<str:slug>', kat_pengaduan.softDelete, name='admin_kat_pengaduan_soft_delete'),
                    path('restore/<str:slug>', kat_pengaduan.restore, name='admin_kat_pengaduan_restore'),
                    path('permanent_delete/<str:slug>', kat_pengaduan.permanentDelete, name='admin_kat_permanent_delete'),
                    
                ]
            )),

             #AKSESBILITAS EXTERNAL LINK DINAS PENDIDIKAN===============================
            path('pelayanan_pengaduan/', include(
                [
                    path('', pengaduan.admin_index, name='admin_pelayanan_pengaduan'),
                    path('status_pengaduan/<int:id>', pengaduan.Approve, name='admin_status_pengaduan'),
                    path('belum_selesai/<int:id>', pengaduan.NotApprove, name='admin_status_not_aprrove_pengaduan'),
                    path('input_catatan/<int:id>', pengaduan.inputCatatan, name='admin_input_catatan_alasan'),
                    
                ]
            )),

             #INFORMASI TERPADU DINAS PENDIDIKAN===============================
            path('infoterpadu/', include(
                [
                    # ROUTE INFOGRAFIS
                    path('infografis/', infoterpadu.admin_index_infografis, name='admin_infoterpadu_infografis'),
                    path('infografis/create/', infoterpadu.admin_index_infografis_create, name='admin_infoterpadu_infografis_cretae'),
                    path('infografis/edit/<str:slug>', infoterpadu.admin_index_infografis_edit, name='admin_infoterpadu_infografis_edit'),
                    # ROUTE INFOPUBLIK
                    path('infopublik/', infoterpadu.admin_index_infopublik, name='admin_infoterpadu_infopublik'),
                    path('infopublik/create/', infoterpadu.admin_index_infopublik_create, name='admin_infoterpadu_infopublik_cretae'),
                    path('infopublik/edit/<str:slug>', infoterpadu.admin_index_infopublik_edit, name='admin_infoterpadu_infopublik_edit'),
                    path('soft_delete/<int:id>', infoterpadu.softDelete, name='admin_infoterpadu_soft_delete'),
                    path('restore/<int:id>', infoterpadu.restore, name='admin_infoterpadu_restore'),
                    # ROUTE REGULASI
                    path('regulasi/', infoterpadu.admin_index_regulasi, name='admin_infoterpadu_regulasi'),
                    path('regulasi/create/', infoterpadu.admin_index_regulasi_create, name='admin_infoterpadu_regulasi_cretae'),
                    path('regulasi/edit/<str:slug>', infoterpadu.admin_index_regulasi_edit, name='admin_infoterpadu_regulasi_edit'),
                    path('soft_delete/<int:id>', infoterpadu.softDelete, name='admin_infoterpadu_soft_delete'),
                    path('restore/<int:id>', infoterpadu.restore, name='admin_infoterpadu_restore'),
                    # ROUTE KALENDER PENDIDIKAN
                    path('kalender_pendidikan/', infoterpadu.admin_index_kalender_pendidikan, name='admin_infoterpadu_kalender_pendidikan'),
                    path('kalender_pendidikan/create/', infoterpadu.admin_index_kalender_pendidikan_create, name='admin_kalender_pendidikan_create'),
                    path('kalender_pendidikan/edit/<str:slug>', infoterpadu.admin_index_kalender_pendidikan_edit, name='admin_infoterpadu_kalender_pendidikan_edit'),
                    path('soft_delete/<int:id>', infoterpadu.softDelete, name='admin_infoterpadu_soft_delete'),
                    path('restore/<int:id>', infoterpadu.restore, name='admin_infoterpadu_restore'),
                     # ROUTE AGENDA
                    path('agenda/', infoterpadu.admin_index_agenda, name='admin_infoterpadu_agenda'),
                    path('agenda/create/', infoterpadu.admin_index_agenda_create, name='admin_infoterpadu_agenda_cretae'),
                    path('agenda/edit/<str:slug>', infoterpadu.admin_index_agenda_edit, name='admin_infoterpadu_agenda_edit'),
                    path('soft_delete/<int:id>', infoterpadu.softDelete, name='admin_infoterpadu_soft_delete'),
                    path('restore/<int:id>', infoterpadu.restore, name='admin_infoterpadu_restore'),

                ]
            )),
           
            # BERITA================================================
            path('news/', include(
                [
                    path('', news.admin_index, name='admin_news'),
                    path('create/', news.create, name='admin_news_create'),
                    path('edit/<str:slug>', news.edit, name='admin_news_edit'),
                    path('detail/<str:slug>', news.admin_detail, name='admin_news_detail'),
                    path('soft_delete/<str:slug>', news.softDelete, name='admin_news_soft_delete'),
                    path('permanent_delete/<str:slug>', news.permanentDelete, name='admin_news_permanent_delete'),
                    path('restore/<str:slug>', news.restore, name='admin_news_restore'),
                ]
            )),

            path('kata_mereka/', include(
                [
                    path('', kata.admin_index, name='admin_KM'),
                    path('create/', kata.create, name='admin_KM_create'),
                    path('edit/<int:id>', kata.edit, name='admin_KM_edit'),
                    path('detail/<int:id>', kata.admin_detail, name='admin_KM_detail'),
                    path('soft_delete/<int:id>', kata.softDelete, name='admin_KM_soft_delete'),
                    path('permanent_delete/<int:id>', kata.permanentDelete, name='admin_KM_permanent_delete'),
                    path('restore/<int:id>', kata.restore, name='admin_KM_restore'),
                ]
            )),
            
            # GALLERY
            path('galeri/', include(
                [
                    path('', gallery.admin_index, name='admin_gallery'),
                    path('create/', gallery.create, name='admin_gallery_create'),
                    path('edit/<str:slug>', gallery.edit, name='admin_gallery_edit'),
                    path('detail/<str:slug>', gallery.admin_detail, name='admin_gallery_detail'),
                    path('soft_delete/<str:slug>', gallery.softDelete, name='admin_gallery_soft_delete'),
                    path('permanent_delete/<str:slug>', gallery.permanentDelete, name='admin_gallery_permanent_delete'),
                    path('restore/<str:slug>', gallery.restore, name='admin_gallery_restore'),
                ]
            )),
            # USERS
            path('user/', include(
                [
                    path('', user.admin_index, name='admin_user'),
                    path('create/', user.create, name='admin_user_create'),
                    path('password/edit/', user.edit_password, name='admin_user_password_edit'),
                    path('profile/edit/<str:username>', user.edit_profile, name='admin_user_edit'),
                    path('avatar/edit/<str:username>', user.edit_avatar, name='admin_user_avatar_edit'),
                    path('detail/', user.admin_detail, name='admin_user_detail'),
                    path('soft_delete/<str:username>', user.softDelete, name='admin_user_soft_delete'),
                    path('permanent_delete/<str:slug>', user.permanentDelete, name='admin_user_permanent_delete'),
                    path('restore/<str:username>', user.restore, name='admin_user_restore'),
                    path('edit_role/', user.edit_role, name='admin_user_edit_role'),
                    path('non_aktif/<int:id>', user.non_aktif, name='non_aktif'),
                    path('re_aktif/<int:id>', user.re_aktif, name='aktif'),
                ]
            )),

            # SETTING APLIKASI
            path('setting/', include(
                [
                    # path('', user.admin_index, name='admin_setting'),
                    path('category/', setting.admin_index_category, name='admin_setting_category'),
                    path('category/edit/', setting.admin_index_category_edit, name='admin_setting_category_edit'),
                    path('running_text/', setting.admin_index_running_text, name='admin_setting_running_text'),
                    path('running_text/edit/', setting.admin_index_running_text_edit, name='admin_setting_running_text_edit'),
                    path('link/', setting.admin_index_link, name='admin_setting_link'),
                    path('links/edit/', setting.admin_index_link_edit, name='admin_setting_link_edit'),
                    path('infografis/', setting.admin_index_infografis, name='admin_setting_infografis'),
                    path('cara_bayar/', setting.admin_index_carabayar, name='admin_setting_carabayar'),
                    path('carousel/', setting.admin_index_carousel, name='admin_setting_carousel'),
                    path('create/carousel/<str:jenis>', setting.create_carousel, name='admin_setting_create_carousel'),
                    path('edit/carousel/<int:id>', setting.edit_carousel, name='admin_setting_edit_carousel'),
                    path('running_link/', setting.admin_index_running_link, name='admin_setting_running_link'),
                    path('create/running_link/', setting.create_running_link, name='admin_setting_create_running_link'),
                    path('edit/running_link/<int:id>', setting.edit_running_link, name='admin_setting_edit_running_link'),
                    path('pages/', setting.admin_index_pages, name='admin_setting_pages'),
                    path('create/pages/', setting.create_pages, name='admin_setting_create_pages'),
                    path('edit/pages/<str:slug>', setting.edit_pages, name='admin_setting_edit_pages'),
                    path('detail/pages/<str:slug>', setting.detail_pages, name='admin_setting_detail_pages'),
                    path('soft_delete/<str:jenis>/<int:id>', setting.softDelete, name='admin_setting_soft_delete'),
                    path('permanent_delete/<str:jenis>/<int:id>', setting.permanentDelete, name='admin_setting_permanent_delete'),
                    path('restore/<str:jenis>/<int:id>', setting.restore, name='admin_setting_restore'),
                    path('me/<str:section>', setting.admin_index_aboutme, name='admin_setting_aboutme'),
                    path('create/me/<str:section>', setting.create_aboutme, name='admin_setting_create_aboutme'),
                    path('edit/me/<str:section>/<int:id>', setting.edit_aboutme, name='admin_setting_edit_aboutme'),
                    path('delete/<str:section>/<int:id>', setting.delete_aboutme, name='admin_setting_delete_aboutme'),
          
                ]
            )),
            
            # TAMBAHAN JOEL ====================================================================
            path('ppid/', include(
                [
                    path('', ppid.admin_index, name='admin_ppid'),
                    path('create/', ppid.create, name='admin_ppid_create'),
                    path('edit/<str:slug>', ppid.edit, name='admin_ppid_edit'),
                    # path('detail/<str:slug>', ppid.admin_detail, name='admin_ppid_detail'),
                    path('soft_delete/<str:slug>', ppid.softDelete, name='admin_ppid_soft_delete'),
                    path('permanent_delete/<str:slug>', ppid.permanentDelete, name='admin_ppid_permanent_delete'),
                    path('restore/<str:slug>', ppid.restore, name='admin_ppid_restore'),
                ]
            )),

            # TAMBAHAN JOEL PPDB ADMIN ====================================================================
            path('ppdb/', include(
                [
                    path('', ppbd_admin.adm_index, name='adm_ppdb_index'),
                    path('beranda/', ppbd_admin.adm_beranda, name='adm_ppdb_beranda'),
                    path('setting/', ppbd_admin.adm_setting, name='adm_ppdb_setting'),
                    # ## TAHAPAN ==========================================================================
                    path('tahapan/', ppbd_admin.adm_tahapan, name='adm_ppdb_tahapan'),
                    path('tahapan/edit/<str:slug>', ppbd_admin.adm_tahapan_edit, name='adm_ppdb_tahapan_edit'),
                    # ## JALUR ============================================================================
                    path('jalur/', ppbd_admin.adm_jalur, name='adm_ppdb_jalur'), #### BELUM SELESAI =======
                    # ## PENGUMUMAN =======================================================================
                    path('pengumuman/', ppbd_admin.adm_pengumuman, name='adm_ppdb_pengumuman'), #### BELUM SELESAI =======
                    # ## INFOGRAFIS =======================================================================
                    path('infografis/', ppbd_admin.adm_infografis, name='adm_ppdb_infografis'),
                    # ## FAQ ==============================================================================
                    path('faq/', ppbd_admin.adm_faq, name='adm_ppdb_faq'),
                    path('faq/create/', ppbd_admin.adm_faq_create, name='adm_ppdb_faq_create'),
                    path('faq/edit/<str:slug>', ppbd_admin.adm_faq_edit, name='adm_ppdb_faq_edit'),
                    path('faq/soft_delete/<str:slug>', ppbd_admin.softDelete, name='adm_ppdb_faq_soft_delete'),
                    path('faq/permanent_delete/<str:slug>', ppbd_admin.permanentDelete, name='adm_ppdb_faq_permanent_delete'),
                    path('faq/restore/<str:slug>', ppbd_admin.restore, name='adm_ppdb_faq_restore'),
                    # ## BERITA ===========================================================================
                    path('berita/', ppbd_berita.adm_berita, name='adm_ppdb_berita'),
                    path('berita/create/', ppbd_berita.adm_berita_create, name='adm_ppdb_berita_create'),
                    path('berita/edit/<str:slug>', ppbd_berita.adm_berita_edit, name='adm_ppdb_berita_edit'),
                    path('berita/soft_delete/<str:slug>', ppbd_berita.softDelete, name='adm_ppdb_berita_soft_delete'),
                    path('berita/permanent_delete/<str:slug>', ppbd_berita.permanentDelete, name='adm_ppdb_berita_permanent_delete'),
                    path('berita/restore/<str:slug>', ppbd_berita.restore, name='adm_ppdb_berita_restore'),
                ]
            )),

        ]
    )),

    # ############################################## PATH UNTUK PPBD - JOEL ======================================================
   path('ppdb/', include(
        [
            path('', ppbd.index, name='ppbd_home'),
            path('berita/', ppbd.index_berita, name='ppbd_berita'),
            path('berita/detail/<str:slug>', ppbd.detail_berita, name='ppbd_detail_berita'),
            path('berita/tags/<str:slug>', ppbd.tags_index, name='ppbd_tags'),
            
            # TAMBAHAN JOEL ====================================================================
            # path('ppid/', include(
            #     [
            #         path('', ppid.admin_index, name='admin_ppid'),
            #         path('create/', ppid.create, name='admin_ppid_create'),
            #         path('edit/<str:slug>', ppid.edit, name='admin_ppid_edit'),
            #         # path('detail/<str:slug>', ppid.admin_detail, name='admin_ppid_detail'),
            #         path('soft_delete/<str:slug>', ppid.softDelete, name='admin_ppid_soft_delete'),
            #         path('permanent_delete/<str:slug>', ppid.permanentDelete, name='admin_ppid_permanent_delete'),
            #         path('restore/<str:slug>', ppid.restore, name='admin_ppid_restore'),
            #     ]
            # )),

        ]
    )),

]