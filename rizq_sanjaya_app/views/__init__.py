from .admin import *
from .dashboard import *
from .gallery import *
from .news import *
from .tags import *
from .user import *
from .setting import *
from .layanan import *
from .services import *

from .halaman import *
from .product import *
from .kata import *
from .layanan_ import *
from .aksesbilitas import *
from .infoterpadu import *
from .profilskpd import *
from .profiledinas import *
from .fe_inovasi import *
from .fe_contact import *
from .berita import *
from .fe_infoterpadu import *
# JOEL ADD ====================
from .ppid import *

# ## ADD PPBD - JOEL ==========================
from .ppbd import *
from .ppbd_admin import *
from .ppbd_berita import *
from .kat_pengaduan import *
from .pengaduan import *
from .auth import *
