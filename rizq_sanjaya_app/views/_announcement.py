from django.shortcuts import render, HttpResponse, redirect
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import Announcement
from django.utils import timezone
import os
from ..decorators import *
from datetime import datetime
from django.conf import settings

@require_http_methods(["GET"])
def index(request):
    announcement = Announcement.objects.filter(deleted_at=None).order_by('-id')
    favorits = News.objects.filter(deleted_at=None).order_by('-seen')[:5]

    pengumuman = Announcement.objects.filter(deleted_at=None).order_by('-created_at')[:5]
    agenda = Agenda.objects.filter(deleted_at=None).order_by('-created_at')[:5]
    date_format = '%Y-%m-%d'
    for x in agenda:
        a = datetime.strptime(str(x.start_date), date_format)
        b = datetime.strptime(str(x.finish_date), date_format)
        delta = b - a
        x.days = delta.days + 1

    for x in announcement:
        ext = os.path.splitext(settings.MEDIA_ROOT+str(x.dokumen))[-1].lower()

        if ext == '.pdf':
            x.filex = 'file-pdf'
        elif ext in ['.jpg','.jpeg','.png']:
            x.filex = 'file-image'
        else:
            x.filex = 'dash'

    context = {
        'title' : 'Pengumuman',
        'announcements' : announcement,
        'favs' : favorits,
        'is_detail' : False,
        'agendas' : agenda,
        'pengumuman':pengumuman,
    }
    return render(request, 'profile/announcement/index.html', context)

@require_http_methods(["GET"])
def detail_front(request, slug):
    favorits = News.objects.filter(deleted_at=None).order_by('-seen')[:5]
    pengumuman = Announcement.objects.filter(deleted_at=None).order_by('-created_at')[:5]

    agenda = Agenda.objects.filter(deleted_at=None).order_by('-created_at')[:5]
    date_format = '%Y-%m-%d'
    for x in agenda:
        a = datetime.strptime(str(x.start_date), date_format)
        b = datetime.strptime(str(x.finish_date), date_format)
        delta = b - a
        x.days = delta.days + 1

    try:
        announcement = Announcement.objects.get(slug=slug)
        ext = os.path.splitext(settings.MEDIA_ROOT+str(announcement.dokumen))[-1].lower()

        if ext == '.pdf':
            announcement.filex = 'pdf'
        elif ext in ['.jpg','.jpeg','.png']:
            announcement.filex = 'img'
        else:
            announcement.filex = '-'

    except Announcement.DoesNotExist:
        announcement = None
    context = {
        'title' : 'Detail Pengumuman' if announcement != None else 'TIDAK DITEMUKAN',
        'announcement' : announcement,
        'favs' : favorits,
        'is_detail' : True,
        'agendas' : agenda,
        'pengumuman':pengumuman,
    }
    return render(request, 'profile/announcement/index.html', context)

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    announcement = Announcement.objects.all().order_by('-created_at')
    archives = Announcement.objects.filter(deleted_at__isnull=False).order_by('-created_at')
    context = {
        'title' : 'Announcement - Admin',
        'announcements' : announcement,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/announcement/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''
    if request.method == 'GET':
        form = AnnouncementForm()
        context = {
            'title' : 'ADD PENGUMUMAN',
            'form' : form,
        }
        template = 'profile/admin/announcement/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        form = AnnouncementForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            new_announcement = form.save(commit=False)
            new_announcement.created_by_id = request.user.id
            new_announcement.last_updated_by = request.user.id
            new_announcement.save()
            messages.success(request, 'Pengumuman berhasil disimpan.')
            return redirect('profile:admin_announcement',)
        messages.error(request, 'Pengumuman gagal disimpan.')
        return render(request, 'profile/admin/announcement/create.html', {'form': form})

@require_http_methods(["GET"])
def detail(request, slug):
    template = 'profile/announcement/detail.html'
    try:
        announcement = Announcement.objects.get(slug=slug)
    except Announcement.DoesNotExist:
        announcement = None
    context = {
        'title' : announcement.name if announcement != None else 'TIDAK DITEMUKAN',
        'announcement' : announcement,
    }
    return render(request, template, context)

@login_required
@is_verified()
def edit(request, slug):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        try:
            announcement = Announcement.objects.get(slug=slug)
            form = AnnouncementForm(instance=announcement)
            slug = announcement.slug
        except:
            announcement = None
            form = AnnouncementForm()
            slug = None
        context = {
            'title' : 'EDIT PENGUMUMAN',
            'form' : form,
            'edit' : 'true',
            'slug' : slug,
            'announcement' : announcement,
        }
        template = 'profile/admin/announcement/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        announcement = Announcement.objects.get(slug=slug)
        dokumen_old = bool(announcement.dokumen)
        if dokumen_old : 
            path_file_lama = f"{settings.MEDIA_ROOT}/{announcement.dokumen}"

        form = AnnouncementForm(data=request.POST, files=request.FILES, instance=announcement)
        if form.is_valid():
            if len(request.FILES) > 0:
                try:
                    os.remove(path_file_lama)
                except:
                    pass
            announcement.last_updated_by = request.user.id
            announcement.save()
            messages.success(request, 'Pengumuman berhasil disimpan.')
            return redirect('profile:admin_announcement')

        messages.error(request, 'Pengumuman gagal disimpan.')
        return render(request, 'profile/admin/announcement/create.html', {'form': form})

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_detail(request, slug):
    template = 'profile/admin/announcement/detail.html'
    try:
        announcement = Announcement.objects.get(slug=slug)
    except Announcement.DoesNotExist:
        announcement = None
    context = {
        'title' : announcement.name if announcement != None else 'TIDAK DITEMUKAN',
        'announcement' : announcement,
    }
    return render(request, template, context)

@login_required
@is_verified()
def softDelete(request, slug):
    message = ''
    try:
        sekarang = timezone.now()
        announcement = Announcement.objects.get(slug=slug)
        announcement.deleted_at = sekarang
        announcement.save()
        message = 'success'
    except Announcement.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, slug):
    message = ''
    try:
        announcement = Announcement.objects.get(slug=slug)
        try:
            announcement.dokumen.delete()
        except:
            pass
        announcement.delete()
        message = 'success'
    except Announcement.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, slug):
    message = ''
    try:
        announcement = Announcement.objects.get(slug=slug)
        announcement.deleted_at = None
        announcement.save()
        message = 'success'
    except Announcement.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)