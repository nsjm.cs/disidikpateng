from django.shortcuts import render, HttpResponse, HttpResponseRedirect, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import Complaint
import os
from django.conf import settings
from django.core.mail import EmailMessage, get_connection
from django.template.loader import get_template
from django.utils import timezone
from ..decorators import *

@require_http_methods(["GET", "POST"])
def index(request):
    if request.method == 'GET':
        favorits = News.objects.filter(deleted_at=None).order_by('-seen')[:5]
        form = ComplaintForm()
        context = {
            'title' : 'Pengaduan',
            'form' : form,
            'favs' : favorits,
            'captcha' : MyCaptcha(),
        }

        return render(request, 'profile/complaint/index.html', context)
    
    if request.method == 'POST':
        form = ComplaintForm(data=request.POST)
        captcha = MyCaptcha(request.POST)

        if captcha.is_valid():
            if form.is_valid():
                try:
                    message = get_template("profile/email/complaint.html").render({
                        'name': request.POST.get('name'),
                        'email': request.POST.get('email'),
                        'phone': request.POST.get('phone'),
                        'subject': request.POST.get('subject'),
                        'message': request.POST.get('message'),
                    })
                    connection = get_connection(host=settings.EMAIL_HOST, 
                                                port=settings.EMAIL_PORT, 
                                                username=settings.EMAIL_HOST_USER_PENGADUAN, 
                                                password=settings.EMAIL_HOST_PASSWORD_PENGADUAN, 
                                                use_tls=settings.EMAIL_USE_TLS)
                    mail = EmailMessage(
                        subject=request.POST.get('subject'),
                        body=message,
                        to=[
                            # masih perlu diupdate
                            'yoelaris@gmail.com',
                        ],
                        connection=connection,
                    )
                    form.save()
                    
                    mail.content_subtype = "html"
                    mail.send()

                    messages.success(request, 'Pengaduan anda berhasil dikirim.')
                    
                except:
                    messages.success(request, 'Pengaduan anda gagal dikirim.')
                
                return redirect('profile:complaint')
        else:
            messages.error(request, 'Pengaduan anda gagal dikirim. Captcha tidak sama.')
            return redirect('profile:complaint')

        return render(request, 'profile/complaint/index.html', {'form': form})

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    complaint = Complaint.objects.filter(deleted_at = None).order_by('-created_at')
    archives = Complaint.objects.filter(deleted_at__isnull=False).order_by('-created_at')
    context = {
        'title' : 'Pengaduan - Admin',
        'complaints' : complaint,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/complaint/index.html', context)

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_detail(request, slug):
    template = 'profile/admin/complaint/detail.html'
    try:
        complaint = Complaint.objects.get(slug=slug)
        complaint.read_status=True
        complaint.save()
    except Complaint.DoesNotExist:
        complaint = None
    context = {
        'title' : 'Pengaduan' if complaint != None else 'TIDAK DITEMUKAN',
        'complaint' : complaint,
    }
    return render(request, template, context)

@login_required
@is_verified()
@require_http_methods(["POST"])
def reply(request):
    if request.method == 'POST':
        try:
            complaint = Complaint.objects.get(slug=request.POST.get('slug'))
            message = get_template("profile/email/complaint_reply.html").render({
                'name': complaint.name,
                'subject': complaint.subject,
                'message': complaint.message,
                'reply' : request.POST.get('balasan'),
            })
            connection = get_connection(host=settings.EMAIL_HOST, 
                                        port=settings.EMAIL_PORT, 
                                        username=settings.EMAIL_HOST_USER_PENGADUAN, 
                                        password=settings.EMAIL_HOST_PASSWORD_PENGADUAN, 
                                        use_tls=settings.EMAIL_USE_TLS)
            mail = EmailMessage(
                subject=f"Balasan terhadap pengaduan: {complaint.subject}",
                body=message,
                to=[
                    complaint.email,
                ],
                connection=connection,
            )
            mail.content_subtype = "html"
            mail.send()

            complaint.reply_status = True
            complaint.save()

            messages.success(request, 'Balasan anda berhasil dikirim.')
            return redirect('profile:admin_complaint_detail', slug=complaint.slug)
            
        except:
            messages.success(request, 'Balasan anda gagal dikirim.')
            return redirect('profile:complaint')

@login_required
@is_verified()
def softDelete(request, slug):
    message = ''
    try:
        sekarang = timezone.now()
        complaint = Complaint.objects.get(slug=slug)
        complaint.deleted_at = sekarang
        complaint.save()
        message = 'success'
    except Complaint.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, slug):
    message = ''
    try:
        complaint = Complaint.objects.get(slug=slug)
        complaint.delete()
        message = 'success'
    except Complaint.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, slug):
    message = ''
    try:
        complaint = Complaint.objects.get(slug=slug)
        complaint.deleted_at = None
        complaint.save()
        message = 'success'
    except Complaint.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)