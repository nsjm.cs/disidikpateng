from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import Document
from django.utils import timezone
import os
from django.conf import settings
from ..decorators import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

@require_http_methods(["GET"])
def index(request):

    runlink = RunningLink.objects.filter(deleted_at=None).order_by('id')
    documents = Document.objects.filter(deleted_at=None).order_by('-created_at')
    paginator = Paginator(documents, 10) ### === JUMLAH DATA YG DITAMPILKAN
    page = request.GET.get('page', 1)
    tgl_dt = []

    try:
        documents = paginator.page(page)
    except PageNotAnInteger:
        documents = paginator.page(1)
    except EmptyPage:
        documents = paginator.page(paginator.num_pages)

    for x in documents:
        ext = os.path.splitext(settings.MEDIA_ROOT+str(x.dokumen))[-1].lower()

        if ext == '.pdf':
            x.filex = 'pdf.png'
            x.jnsfl = 'pdf'
        elif ext in ['.jpg','.jpeg','.png']:
            x.filex = 'img.png'
            x.jnsfl = 'img'
        else:
            x.filex = 'doc.png'
            x.jnsfl = 'doc'

        x.fil_date = x.created_at.strftime("%d%m%Y")
        arr_dt = {'value':x.created_at.strftime("%d%m%Y"),'nama':x.created_at}
        tgl_dt.append(arr_dt)

    for x in runlink:
        x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.logo))

    array_temp = []
    array_fix = []

    for xx in tgl_dt:
        if xx['value'] not in array_temp:
            array_temp.append(xx['value'])
            array_fix.append(xx)

    context = {
        'title' : 'Dokumen Publikasi',
        'documents' : documents,
        'runlink' : runlink,
        'jml_dt' : len(documents),
        'tgl_dt' : array_fix,
    }
    return render(request, 'profile/document/index.html', context)

@require_http_methods(["GET"])
def detail(request, slug):
    template = 'profile/document/detail.html'
    try:
        doc = Document.objects.get(slug=slug)
    except Document.DoesNotExist:
        doc = None
    context = {
        'title' : doc.nama if doc != None else 'TIDAK DITEMUKAN',
        'document' : doc,
    }
    return render(request, template, context)

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    documents = Document.objects.all().order_by('-created_at')
    archives = Document.objects.filter(deleted_at__isnull=False).order_by('-created_at')
    context = {
        'title' : 'Dokumen - Admin',
        'documents' : documents,
        'archives' : archives,
    }

    return render(request, 'profile/admin/document/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = DocumentForm()
        context = {
            'title' : 'ADD DOCUMENT',
            'form' : form,
        }

        template = 'profile/admin/document/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        form = DocumentForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            new_doc = form.save(commit=False)
            new_doc.created_by_id = request.user.id
            new_doc.save()
            doc = Document.objects.get(id = new_doc.id)
            messages.success(request, 'Dokumen berhasil disimpan.')
            return redirect('profile:admin_document')

        messages.error(request, 'Documnet gagal disimpan.')
        return render(request, 'profile/admin/document/create.html', {'form': form})

@login_required
@is_verified()
def edit(request, slug):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        try:
            doc = Document.objects.get(slug=slug)
            form = DocumentForm(instance=doc)
            slug = doc.slug
        except:
            doc = None
            form = DocumentForm()
            slug = None
            
        context = {
            'title' : 'EDIT DOCUMENT',
            'form' : form,
            'edit' : 'true',
            'slug' : slug,
            'doc' : doc,
        }
        template = 'profile/admin/document/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        doc = Document.objects.get(slug=slug)
        path_file_lama = f"{settings.MEDIA_ROOT}/{doc.dokumen}"
        if bool(request.FILES):
            form = DocumentForm(data=request.POST, files=request.FILES, instance=doc)
        else:
            form = DocumentFormTanpaFile(data=request.POST, instance=doc)

        if form.is_valid():
            if not request.FILES.get('dokumen'):
                doc.nama = form.cleaned_data.get('nama')
                doc.keterangan = form.cleaned_data.get('keterangan')
            else:
                os.remove(path_file_lama)
                doc.dokumen = form.cleaned_data.get('dokumen')
                doc.nama = form.cleaned_data.get('nama')
                doc.keterangan = form.cleaned_data.get('keterangan')
            doc.last_updated_by = request.user.id
            doc.save()
            messages.success(request, 'Dokumen berhasil disimpan.')
            return redirect('profile:admin_document')

        messages.error(request, 'Dokumen gagal disimpan.')
        return render(request, 'profile/admin/document/create.html', {'form': form})

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_detail(request, slug):
    template = 'profile/admin/document/detail.html'
    try:
        doc = Document.objects.get(slug=slug)
    except Document.DoesNotExist:
        doc = None
    context = {
        'title' : doc.nama if doc != None else 'TIDAK DITEMUKAN',
        'document' : doc,
    }
    return render(request, template, context)

@login_required
@is_verified()
def softDelete(request, slug):
    message = ''
    try:
        sekarang = timezone.now()
        doc = Document.objects.get(slug=slug)
        doc.deleted_at = sekarang
        doc.save()
        message = 'success'
    except Document.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, slug):
    message = ''
    try:
        doc = Document.objects.get(slug=slug)
        try:
            doc.dokumen.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except Document.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, slug):
    message = ''
    try:
        doc = Document.objects.get(slug=slug)
        doc.deleted_at = None
        doc.save()
        message = 'success'
    except Document.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)