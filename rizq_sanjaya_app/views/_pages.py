from django.shortcuts import render, HttpResponse, HttpResponseRedirect, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News, Category
from datetime import datetime
import os
from django.conf import settings
from ..decorators import *


@require_http_methods(["GET"])
def index(request, slug):
	kategori = Category.objects.all().order_by('nama')
	favorits = News.objects.filter(deleted_at=None).order_by('-seen')[:5]

	runlink = RunningLink.objects.filter(deleted_at=None).order_by('id')
	for x in runlink:
		x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.logo))

	context = {
		'title' : slug.replace('-', ' ').title(),
		'cats' : kategori,
		'favs' : favorits,
		'runlink' : runlink,
	}
	return render(request, 'profile/pages/index.html', context)