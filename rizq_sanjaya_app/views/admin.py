from django.shortcuts import render, HttpResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods

from rizq_sanjaya_app.decorators import is_verified
from ..forms import *
from ..models import *
from django.db import connections
from ..helpers import dictfetchall
from django.db.models import Count, DateTimeField
from django.db.models.functions import ExtractYear, ExtractMonth, TruncMonth
from django.http import JsonResponse
from datetime import datetime
from calendar import month_name

def get_available_years_and_months():
    dates = PelayananPublik.objects.annotate(
        year=ExtractYear('created_at'),
        month=ExtractMonth('created_at')
    ).values('year', 'month').distinct().order_by('year', 'month')
    
    return dates

def get_indonesian_month_name(month_number):
    bulan_names_id = [
        "Januari", "Februari", "Maret", "April",
        "Mei", "Juni", "Juli", "Agustus",
        "September", "Oktober", "November", "Desember"
    ]
    return bulan_names_id[month_number - 1] if 1 <= month_number <= 12 else ""

@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    Popular = Carousel.objects.filter(kondisi = "Popular").count() 
    terlaris = Carousel.objects.filter(kondisi = "Terlaris").count()
    gallery = Gallery.objects.all().count()
    all = Carousel.objects.all().count()
    produk_total = Product.objects.filter().count()
    produk_kalung = Product.objects.filter(category__nama='Kalung').count()
    produk_gelang = Product.objects.filter(category__nama='Gelang').count()
    produk_aksesoris = Product.objects.filter(category__nama='Aksesoris').count()
    produk_best = Product.objects.filter(best_seller=True).count()

    dates = get_available_years_and_months()
    
    years = sorted(set(date['year'] for date in dates))
    months = sorted(set(date['month'] for date in dates))

    months_label = [get_indonesian_month_name(month) for month in months]

    months_zip = zip(months, months_label)
    
    selected_year = int(request.GET.get('year', datetime.now().year))
    selected_month = int(request.GET.get('month', datetime.now().month))

    filtered_data = PelayananPublik.objects.filter(
        created_at__year=selected_year,
        created_at__month=selected_month
    ).annotate(month=TruncMonth('created_at')).values('month').annotate(count=Count('id')).order_by('month')
    year = request.GET.get('year', datetime.now().year)
    month = request.GET.get('month', datetime.now().month)


    kategori_counts = PelayananPublik.objects.filter(created_at__year=year, created_at__month=month) \
        .values('kategori__nama_kategori') \
        .annotate(count=Count('id')) \
        .order_by('kategori__nama_kategori')


    kategori_count_bar = PelayananPublik.objects.filter(created_at__year=selected_year) \
            .annotate(month=TruncMonth('created_at')) \
            .values('month', 'kategori__nama_kategori') \
            .annotate(count=Count('id')) \
            .order_by('month', 'kategori__nama_kategori')

    

    kategori_labels = [data['kategori__nama_kategori'] for data in kategori_counts]
    kategori_count = [data['count'] for data in kategori_counts]


    months_kategori = sorted(set(data['month'] for data in kategori_count_bar))
    label_perkategori = sorted(set(data['kategori__nama_kategori'] for data in kategori_count_bar))


    kategori_count_bar = PelayananPublik.objects.filter(created_at__year=selected_year) \
        .annotate(month=TruncMonth('created_at')) \
        .values('month', 'kategori__nama_kategori') \
        .annotate(count=Count('id')) \
        .order_by('month', 'kategori__nama_kategori')

    data_dict = {}
    category_set = set()

    for entry in kategori_count_bar:
        month = entry['month'].strftime('%B')  # Nama bulan dalam format teks
        kategori = entry['kategori__nama_kategori']
        count = entry['count']
        
        if month not in data_dict:
            data_dict[month] = {}
        
        data_dict[month][kategori] = count
        category_set.add(kategori)

    # Mengubah set ke dalam list untuk mendapatkan urutan kategori yang konsisten
    categories = sorted(list(category_set))

    # Persiapkan data untuk Chart.js
    chart_labels = list(data_dict.keys())  # Label untuk sumbu X (bulan)
    chart_datasets = []

    for category in categories:
        dataset = {
            'label': category,
            'data': [data_dict[month].get(category, 0) for month in chart_labels],  # Jumlah untuk setiap bulan
        }
        chart_datasets.append(dataset)

    print(chart_datasets)


    pending_count = PelayananPublik.objects.filter(status_pengaduan=None).count()
    selesai_count = PelayananPublik.objects.filter(status_pengaduan=1).count()
    belum_selesai_count = PelayananPublik.objects.filter(status_pengaduan=0).count()


    with connections['default'].cursor() as cursor:
        cursor.execute("""
            SELECT 
                DISTINCT a.browser_type as browser_type,
                (SELECT count(b.browser_type) FROM public.rizq_sanjaya_app_logvisitor b 
                WHERE b.browser_type=a.browser_type and b.device_type<>'-') as total_count
            FROM public.rizq_sanjaya_app_logvisitor a WHERE a.device_type<>'-';""")
        browsers = dictfetchall(cursor)
    browser_labels = []
    browser_count = []
    for browser in browsers:
        browser_labels.append(browser['browser_type'])
        browser_count.append(browser['total_count'])
    
    with connections['default'].cursor() as cursor:
        cursor.execute("""
            SELECT 
                DISTINCT a.device_type as device_type,
                (SELECT count(b.device_type) FROM public.rizq_sanjaya_app_logvisitor b 
                WHERE b.device_type=a.device_type and b.device_type<>'-') as total_count
            FROM public.rizq_sanjaya_app_logvisitor a WHERE a.device_type<>'-';""")
        devices = dictfetchall(cursor)
    device_labels = []
    device_count = []
    for device in devices:
        device_labels.append(device['device_type'])
        device_count.append(device['total_count'])
    
    with connections['default'].cursor() as cursor:
        cursor.execute("""
            SELECT d::date as tanggal,
                (SELECT count(id) FROM public.rizq_sanjaya_app_logvisitor b 
                WHERE d::date=date(waktu) and b.device_type<>'-') as total_count
            FROM generate_series(DATE(NOW()) - INTERVAL '14 DAYS', DATE(NOW()), '1 day'::interval) d""")
        dates = dictfetchall(cursor)
    date_labels = []
    date_count = []
    for date in dates:
        date_labels.append(date['tanggal'].strftime("%d/%m/%Y"))
        date_count.append(date['total_count'])

    context = {
        'gallery' : gallery,
        'popular' : Popular,
        'all' : all,
        'terlaris' : terlaris,
        'title':'Dashboard',
        'produk_kalung' : produk_kalung,
        'produk_gelang' : produk_gelang,
        'produk_aksesoris' : produk_aksesoris,
        'produk_best' : produk_best,
        'produk_total' : produk_total,
        # 'news_published': news_published,
        # 'news_total': news_total,
        # 'document_published': document_published,
        # 'document_total': document_total,
        # 'gallery_published': gallery_published,
        # 'gallery_total': gallery_total,
        # 'announcement_published': announcement_published,
        # 'announcement_total': announcement_total,
        # 'newss': news,
        # 'guestbooks': guestbooks,
        # 'complaints': complaints,
        'device_labels': device_labels,
        'device_count': device_count,
        'date_labels': date_labels,
        'date_count': date_count,
        'browser_labels': browser_labels,
        'browser_count': browser_count,
        # 'agendas': agendas,
        'years': years,
        'months': months,
        'months_zip': months_zip,
        'selected_year': selected_year,
        'selected_month': selected_month,
        'kategori_labels': kategori_labels,
        'kategori_count': kategori_count,
        'chart_labels': chart_labels,
        'chart_datasets': chart_datasets,
        'pending_count': pending_count,
        'selesai_count': selesai_count,
        'belum_selesai_count': belum_selesai_count,
        

    }
    return render(request, 'profile/admin/index.html', context)


@login_required
@require_http_methods(["GET"])
def Datapengaduan(request):
    selected_year = int(request.GET.get('year'))
    selected_month = int(request.GET.get('month'))

    kategori_counts = PelayananPublik.objects.filter(created_at__year=selected_year, created_at__month=selected_month) \
        .values('kategori__nama_kategori') \
        .annotate(count=Count('id')) \
        .order_by('kategori__nama_kategori')

    kategori_labels = [data['kategori__nama_kategori'] for data in kategori_counts]
    kategori_count = [data['count'] for data in kategori_counts]

    response_data = {
        'kategori_labels': kategori_labels,
        'kategori_count': kategori_count,
    }

    return JsonResponse(response_data)