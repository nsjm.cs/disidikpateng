from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import Aksesbilitas
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    aksesbilitas = Aksesbilitas.objects.filter(status='Publish').order_by('title')
    archives = Aksesbilitas.objects.filter(status='Draft').order_by('title')
    paginator = Paginator(aksesbilitas, 15)
    try:
        aksesbilitas = paginator.page(page)
    except PageNotAnInteger:
        aksesbilitas = paginator.page(1)
    except EmptyPage:
        aksesbilitas = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Aksesbilitas Link - Admin',
        'aksesbilitas' : aksesbilitas,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/aksesbilitas/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = Aksesbilitas.objects.all()
        context = {
            'title' : 'ADD AKSESBILITAS LINK',
            'form' : form,
        }

        template = 'profile/admin/aksesbilitas/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        image = request.FILES['image']
        title = request.POST.get('title')
        link = request.POST.get('link')
        if title is not None:
            aksesbilitas = Aksesbilitas()
            aksesbilitas.image = image
            aksesbilitas.title = title
            aksesbilitas.link = link
            aksesbilitas.save()
            
            messages.success(request, 'Aksesbilitas berhasil disimpan.')
            return redirect('profile:admin_aksesbilitas')

        messages.error(request, 'Aksesbilitas gagal disimpan.')
        return render(request, 'profile/admin/aksesbilitas/create.html', {'form': form,})
    
@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        news = Aksesbilitas.objects.get(id = id)
        form = AksesbilitasLink()

        context = {
            'title' : 'EDIT AKSESBILITAS LINK',
            'form' : form,
            'edit' : 'true',
            'aksesbilitas' : news,
        }
        template = 'profile/admin/aksesbilitas/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        news = Aksesbilitas.objects.get(id = id)
        path_file_lama = f"{settings.MEDIA_ROOT}/{news.image}"
        foto_old = bool(news.image)
        title = request.POST.get('title')
        link = request.POST.get('link')

        if news is not None:
            news.title = title
            news.link = link
            news.save()
    
            if 'image' in request.FILES:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                foto = request.FILES['image']
                news.image = foto
                news.save()

            messages.success(request, 'Kata Mereka berhasil disimpan')
            return redirect('profile:admin_aksesbilitas')

        messages.error(request, 'Kata Mereka gagal disimpan.')
        return render(request, 'profile/admin/aksesbilitas/create.html', {'form': form,})

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_detail(request, id):
    template = 'profile/admin/kata/detail.html'
    try:
        news = Kata.objects.get(id=id)
    except Kata.DoesNotExist:
        news = None
    context = {
        'title' : news.judul if news != None else 'TIDAK DITEMUKAN',
        'news' : news,
    }
    return render(request, template, context)

@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        status = 'Draft'
        doc = Aksesbilitas.objects.get(id=id)
        doc.status = status
        doc.save()
        message = 'success'
    except Aksesbilitas.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, id):
    message = ''
    try:
        doc = Kata.objects.get(id=id)
        try:
            doc.image.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except Kata.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        doc = Aksesbilitas.objects.get(id=id)
        doc.status = 'Publish'
        doc.save()
        message = 'success'
    except Aksesbilitas.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)