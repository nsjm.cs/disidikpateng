from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseBadRequest
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth import login, authenticate, logout
from django.conf import settings
from ..models import Account as m_user, ROLE_CHOICES
import shutil,os,requests
from django.db import transaction
from django.db.models.functions import TruncMonth
from django.db.models import Sum, Count
from support.support_function import check_is_email
from support import support_function as sup
from support.support_function import admin_only
from django.db.models import Q, F


def LoginViews(request):
    if request.method == 'GET':
        data = {
            'page_title': 'Dashboard',
        }
        return render(request, 'registration/login.html', data)

    if request.method == 'POST':
        turnstile_response = request.POST.get("cf-turnstile-response")
        
        data = {
            "secret": settings.CLOUDFLARE_TURNSTILE_SECRET_KEY,
            "response": turnstile_response,
            "remoteip": request.META.get("REMOTE_ADDR"),
        }
        verify_response = requests.post(
            "https://challenges.cloudflare.com/turnstile/v0/siteverify", data=data
        )
        
        result = verify_response.json()

        if result.get("success"):
            if not request.user.is_authenticated:
                email = request.POST.get('frm_email')
                pwd = request.POST.get('frm_pwd')

                is_email = check_is_email(email)
                
                if is_email:
                    user = authenticate(request, email=email, password=pwd)
                else:
                    try:
                        user = m_user.objects.get(username = email, is_active = True)
                        if not user.check_password(pwd):
                            user = None
                    except Exception as e:
                        user = None
                
                if user is not None:
                    login(request, user)
                    messages.success(request, f"Selamat datang {user.username} ({user.get_role_display().upper()})")
                    print('masuk')
                    if request.GET.get('next') is not None:
                        return redirect(request.GET.get('next'))
                    else:
                        messages.success(request, f"Selamat datang {user.username} ({user.get_role_display().upper()})")
                        return redirect('profile:admin_home')
                else:
                    messages.error(request, "Login gagal, silahkan masukkan data dengan benar")
                    return redirect('profile:index_login')
            else:
                return redirect('profile:admin_home')

        else:
            messages.error(request, "Login gagal, silahkan masukkan data dengan benar")
            return redirect('profile:index_login')
        


@login_required
def LogoutViews(request):
    if request.method == 'GET':
        logout_message = request.GET.get('logout_message', None)
        if logout_message is not None:
            messages.info(request, logout_message)
        
        logout(request)
        return redirect(request.META['HTTP_REFERER'])
        
        