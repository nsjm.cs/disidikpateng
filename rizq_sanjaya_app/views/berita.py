from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News
from django.utils import timezone
from django.conf import settings
from datetime import datetime
from ..decorators import *
from django.db import connections
from django.db.models import Q, Count
from ..helpers import dictfetchall, get_list_berita, get_category, get_list_galeri
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
import os
import pprint
from django.template.loader import render_to_string
import requests
from django.db.models import OuterRef, Subquery
from taggit.models import Tag, TaggedItem



@require_http_methods(["GET"])
def index(request):
    if 'category_id' in request.GET:
        category_id = request.GET.get('category_id')
        berita = News.objects.filter(category_id=category_id, deleted_at=None).order_by('-created_at')
    else:
        berita = News.objects.filter(deleted_at=None).order_by('-created_at')


        
    profilSKPD_home = ProfilSKPD.objects.exclude(typexs='Misi').values('typexs').annotate(count=Count('typexs')).order_by('typexs')
    jml_data = 6
    news = News.objects.filter(deleted_at=None).order_by('-created_at')[:12]
    nws_jml = int(len(news))
    if nws_jml >= jml_data:
        thumbnail = {}
        for x in news:
            x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))
            x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/')}
        news_list = news
    else:
        news_list = get_list_berita(jml_data)
    subquery = News.objects.filter(
        category=OuterRef('pk'), deleted_at__isnull=True
    ).values('category').annotate(news_count=Count('id')).values('news_count')
    tot_category = Category.objects.filter(
        typexs="news"
    ).annotate(news_count=Subquery(subquery)).order_by('id')

    tag = Tag.objects.all()

    paginator = Paginator(berita, 8)  # Menampilkan 10 berita per halaman
    page = request.GET.get('page')

    try:
        daftar_berita = paginator.page(page)
    except PageNotAnInteger:
        # Jika 'page' bukan integer, tampilkan halaman pertama.
        daftar_berita = paginator.page(1)
    except EmptyPage:
        # Jika 'page' lebih besar dari jumlah halaman yang ada, tampilkan halaman terakhir.
        daftar_berita = paginator.page(paginator.num_pages)
    
    #tot_category = Category.objects.filter(typexs="news", news__deleted_at__isnull=True).exclude(news__isnull=True).distinct('nama')
    # dt_kategori_mobil = kat_car.objects.filter(deleted_at=None).order_by('-id')
    context = {
        'daftar_berita': daftar_berita,
        'news_side' : news_list,
        'cat' : tot_category, 
        'home_profilSKPD':profilSKPD_home,
        'tag':tag,
         
       
    }
    return render(request, 'profile/frontend/berita/index.html', context)


def detail_berita(request, slug):
    berita_all = News.objects.filter(deleted_at=None).order_by('-created_at')[:6]
    berita = get_object_or_404(News, slug=slug, deleted_at=None)   
    profilSKPD_home = ProfilSKPD.objects.exclude(typexs='Misi').values('typexs').annotate(count=Count('typexs')).order_by('typexs')
    jml_data = 6
    news = News.objects.filter(deleted_at=None).order_by('-created_at')
    nws_jml = int(len(news))
    if nws_jml >= jml_data:
        thumbnail = {}
        for x in news:
            x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))
            x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/')}
        news_list = news
    else:
        news_list = get_list_berita(jml_data) 
    subquery = News.objects.filter(
        category=OuterRef('pk'), deleted_at__isnull=True
    ).values('category').annotate(news_count=Count('id')).values('news_count')
    tot_category = Category.objects.filter(
        typexs="news"
    ).annotate(news_count=Subquery(subquery)).order_by('id')

    news_tags = TaggedItem.objects.filter(object_id=berita.id, content_type__model='news')
    tag = Tag.objects.all()

    print(berita, news_tags)

    context = {
        'berita': berita,
        'news_side' : news_list,
        'cat' : tot_category, 
        'home_profilSKPD':profilSKPD_home,
        'news_tags':news_tags,
        'tag':tag,
        'berita_all':berita_all,
       
    }
    return render(request, 'profile/frontend/berita/detail_berita.html', context)




def cat_berita(request, slug):
    berita = News.objects.filter(category__slug=slug, deleted_at=None).order_by('-created_at')
    profilSKPD_home = ProfilSKPD.objects.exclude(typexs='Misi').values('typexs').annotate(count=Count('typexs')).order_by('typexs')
    jml_data = 6
    news = News.objects.filter(deleted_at=None).order_by('-created_at')
    nws_jml = int(len(news))
    if nws_jml >= jml_data:
        thumbnail = {}
        for x in news:
            x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))
            x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/')}
        news_list = news
    else:
        news_list = get_list_berita(jml_data)  
    subquery = News.objects.filter(
        category=OuterRef('pk'), deleted_at__isnull=True
    ).values('category').annotate(news_count=Count('id')).values('news_count')
    tot_category = Category.objects.filter(
        typexs="news"
    ).annotate(news_count=Subquery(subquery)).order_by('id')

    tag = Tag.objects.all()

    paginator = Paginator(berita, 6)  # Menampilkan 10 berita per halaman
    page = request.GET.get('page')

    try:
        daftar_berita = paginator.page(page)
    except PageNotAnInteger:
        # Jika 'page' bukan integer, tampilkan halaman pertama.
        daftar_berita = paginator.page(1)
    except EmptyPage:
        # Jika 'page' lebih besar dari jumlah halaman yang ada, tampilkan halaman terakhir.
        daftar_berita = paginator.page(paginator.num_pages)
    
    #tot_category = Category.objects.filter(typexs="news", news__deleted_at__isnull=True).exclude(news__isnull=True).distinct('nama')
    # dt_kategori_mobil = kat_car.objects.filter(deleted_at=None).order_by('-id')
    context = {
        'daftar_berita': daftar_berita,
        'news_side' : news_list,
        'cat' : tot_category, 
        'home_profilSKPD':profilSKPD_home,
        'tag':tag,
         
       
    }
    return render(request, 'profile/frontend/berita/cat_berita.html', context)

def tag_berita(request, slug):
    berita = News.objects.filter(tags__slug=slug, deleted_at=None).order_by('-created_at')
    profilSKPD_home = ProfilSKPD.objects.exclude(typexs='Misi').values('typexs').annotate(count=Count('typexs')).order_by('typexs')
    latestnewsinfo = News.objects.filter(deleted_at__isnull=True).order_by('-created_at')[:6] 
    subquery = News.objects.filter(
        category=OuterRef('pk'), deleted_at__isnull=True
    ).values('category').annotate(news_count=Count('id')).values('news_count')
    tot_category = Category.objects.filter(
        typexs="news"
    ).annotate(news_count=Subquery(subquery)).order_by('id')

    tag = Tag.objects.all()

    paginator = Paginator(berita, 6)  # Menampilkan 10 berita per halaman
    page = request.GET.get('page')

    try:
        daftar_berita = paginator.page(page)
    except PageNotAnInteger:
        # Jika 'page' bukan integer, tampilkan halaman pertama.
        daftar_berita = paginator.page(1)
    except EmptyPage:
        # Jika 'page' lebih besar dari jumlah halaman yang ada, tampilkan halaman terakhir.
        daftar_berita = paginator.page(paginator.num_pages)
    
    #tot_category = Category.objects.filter(typexs="news", news__deleted_at__isnull=True).exclude(news__isnull=True).distinct('nama')
    # dt_kategori_mobil = kat_car.objects.filter(deleted_at=None).order_by('-id')
    context = {
        'daftar_berita': daftar_berita,
        'latestnews' : latestnewsinfo,
        'cat' : tot_category, 
        'home_profilSKPD':profilSKPD_home,
        'tag':tag,
         
       
    }
    return render(request, 'profile/frontend/berita/cat_berita.html', context)