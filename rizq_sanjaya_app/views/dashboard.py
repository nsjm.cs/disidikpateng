from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News
from django.utils import timezone
from django.conf import settings
from datetime import datetime
from ..decorators import *
from django.db import connections
from django.db.models import Q, Count
from ..helpers import dictfetchall, get_list_berita, get_category, get_list_galeri
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
import os
import pprint
from django.template.loader import render_to_string
import requests



@require_http_methods(["GET"])
def index(request):
    profilSKPD_home = ProfilSKPD.objects.exclude(typexs='Misi').values('typexs').annotate(count=Count('typexs')).order_by('typexs')
    layanan_skpd = LayananDinas.objects.exclude(status='Draft').annotate(count=Count('typexs'))
    news =  News.objects.filter(deleted_at = None).order_by('-id')
    news_side_home =  News.objects.filter(deleted_at = None).order_by('-id')[:5]
    akses_link_home = Aksesbilitas.objects.exclude(status ='Draft')
    home_agenda = InfoTerpadu.objects.filter(typexs='agenda',status='Publish')

    try: 
        is_caro = AppSetting.objects.get(nama='is_carousel').keterangan
    except: 
        is_caro = 'False'

    if is_caro != 'False':
        kondisi = 'slide'
        caroline = Carousel.objects.filter(deleted_at = None, kondisi=kondisi).order_by('id')
        img_caroline = settings.STATIC_URL+'frontend/img/slider/noimage-jangandihapus.png'
        if caroline:
            for z in caroline:
                is_foto = os.path.isfile(settings.MEDIA_ROOT+str(z.foto))
                if is_foto == False:
                    z.foto = {'url':img_caroline}
            dt_caroline = caroline
        else:
            dt_caroline = [{'judul':'Selamat Datang', 'subjudul':'Diwebsite Dinas Pendidikan Provinsi Papua Tengah','foto':{'url':img_caroline}}]

    else:
        kondisi = 'video'
        try:
            caroline = Carousel.objects.get(deleted_at = None, kondisi=kondisi)
        except:
            caroline = ''

        dt_caroline = caroline

    context = {
         
        'home_news':news,
        'home_news_side':news_side_home,
        'home_starndar_pelayanan' :layanan_skpd,
        'home_link_external':akses_link_home,
        'home_agenda': home_agenda,
        'caroline' : dt_caroline,
       
    }
    return render(request, 'profile/layouts/frontend/base.html', context)


    
