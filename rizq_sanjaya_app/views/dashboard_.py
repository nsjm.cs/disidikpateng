from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News
from django.utils import timezone
from django.conf import settings
from datetime import datetime
from ..decorators import *
from django.db import connections
from django.db.models import Q, Count
from ..helpers import dictfetchall, get_list_berita, get_category, get_list_galeri
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
import os
import pprint
from django.template.loader import render_to_string
import requests



@require_http_methods(["GET"])
def index(request):
    carousel = Carousel.objects.filter(kondisi = "Popular", deleted_at__isnull=True).order_by('-id')
    terlaris = Carousel.objects.filter(kondisi = "Terlaris", deleted_at__isnull=True).order_by('-id')
    paket = Carousel.objects.filter(deleted_at = None)[:6]
    kata = Kata.objects.filter(deleted_at=None).order_by('-id')
    news =  News.objects.all()
    try: 
        foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
    except: 
        foto_diskon = ''

    try: 
        diskon = AppSetting.objects.get(nama = 'diskon')
    except: 
        diskon = ''

    try: 
        tanggal = AppSetting.objects.get(nama = 'tanggal')
    except: 
        tanggal = ''

    try: 
        email = AppSetting.objects.get(nama = 'email')
    except: 
        email = ''

    try: 
        rumah = AppSetting.objects.get(nama = 'telepon_text')
    except: 
        rumah = ''

    try: 
        wa = AppSetting.objects.get(nama = 'whatsapp_text')
    except: 
        wa = ''

    try: 
        text = AppSetting.objects.get(nama ='telepon')
    except: 
        text = ''

    try: 
        alamat = AppSetting.objects.get(nama ='alamat')
    except: 
        alamat = ''

    try: 
        alamat_link = AppSetting.objects.get(nama ='alamat_link')
    except: 
        alamat_link = ''

    try: 
        link_facebook = AppSetting.objects.get(nama ='link_facebook')
    except: 
        link_facebook = ''

    try: 
        link_twitter = AppSetting.objects.get(nama ='link_twitter')
    except: 
        link_twitter = ''

    try: 
        link_tiktok = AppSetting.objects.get(nama ='link_tiktok')
    except: 
        link_tiktok = ''

    try: 
        link_instagram = AppSetting.objects.get(nama ='link_instagram')
    except: 
        link_instagram = ''

    owl = RunningLink.objects.filter(deleted_at__isnull = True).order_by('-id')

    try: 
        meta_property_title = AppSetting.objects.get(nama ='meta_property_title')
    except: 
        meta_property_title = ''

    try: 
        meta_author = AppSetting.objects.get(nama ='meta_author')
    except: 
        meta_author = ''

    try: 
        meta_keywords = AppSetting.objects.get(nama ='meta_keywords')
    except: 
        meta_keywords = ''

    try: 
        meta_property_description = AppSetting.objects.get(nama ='meta_property_description')
    except: 
        meta_property_description = ''

    context = {
       'dt_carousel' : carousel,
       'dt_paket' : paket,
       'dt_kata' : kata,
       'foto' : foto_diskon,
       'diskon' : diskon,
       'tanggal' : tanggal,
       'email' : email,
       'rumah' : rumah,
       'wa' : wa,
       'text' : text,
       'alamat' : alamat,
       'terlaris' : terlaris,
       'news' : news,
       'alamat_link' : alamat_link,
        'link_facebook' : link_facebook,
        'link_twitter' : link_twitter,
        'link_instagram' : link_instagram,
        'link_tiktok' : link_tiktok,
        'owl' : owl,
        'title' : meta_property_title,
        'author' : meta_author,
        'keywords' : meta_keywords,
        'description' : meta_property_description,
    }
    return render(request, 'profile/layouts/frontend/base.html', context)

def profil(request):
    head = AboutMe.objects.get(jenis='header')
    child = AboutMe.objects.filter(jenis='child').order_by('-id')
    try: 
        foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
    except: 
        foto_diskon = ''

    try: 
        diskon = AppSetting.objects.get(nama = 'diskon')
    except: 
        diskon = ''

    try: 
        tanggal = AppSetting.objects.get(nama = 'tanggal')
    except: 
        tanggal = ''

    try: 
        email = AppSetting.objects.get(nama = 'email')
    except: 
        email = ''

    try: 
        rumah = AppSetting.objects.get(nama = 'telepon_text')
    except: 
        rumah = ''

    try: 
        wa = AppSetting.objects.get(nama = 'whatsapp_text')
    except: 
        wa = ''

    try: 
        text = AppSetting.objects.get(nama ='telepon')
    except: 
        text = ''

    try: 
        alamat = AppSetting.objects.get(nama ='alamat')
    except: 
        alamat = ''

    try: 
        alamat_link = AppSetting.objects.get(nama ='alamat_link')
    except: 
        alamat_link = ''

    try: 
        link_facebook = AppSetting.objects.get(nama ='link_facebook')
    except: 
        link_facebook = ''

    try: 
        link_twitter = AppSetting.objects.get(nama ='link_twitter')
    except: 
        link_twitter = ''

    try: 
        link_instagram = AppSetting.objects.get(nama ='link_instagram')
    except: 
        link_instagram = ''

    paket = Carousel.objects.filter(deleted_at = None)[:6]

    carousel = Carousel.objects.filter(kondisi = "Popular", deleted_at__isnull=True).order_by('-id')[:2]
    terlaris = Carousel.objects.filter(kondisi="Terlaris", diskon__isnull=False, deleted_at__isnull=True).order_by('-id').first()
    print(terlaris)
    owl = RunningLink.objects.filter(deleted_at__isnull = True).order_by('-id')

    try: 
        meta_property_title = AppSetting.objects.get(nama ='meta_property_title')
    except: 
        meta_property_title = ''

    try: 
        meta_author = AppSetting.objects.get(nama ='meta_author')
    except: 
        meta_author = ''

    try: 
        meta_keywords = AppSetting.objects.get(nama ='meta_keywords')
    except: 
        meta_keywords = ''

    try: 
        meta_property_description = AppSetting.objects.get(nama ='meta_property_description')
    except: 
        meta_property_description = ''

    x = {
        'head' : head,
        'child' : child,
        'foto' : foto_diskon,
        'diskon' : diskon,
        'tanggal' : tanggal,
        'email' : email,
        'rumah' : rumah,
        'wa' : wa,
        'text' : text,
        'alamat' : alamat,
        'dt_paket' : paket,
        'popular' : carousel,
        'terlaris' : terlaris,
        'alamat_link' : alamat_link,
        'link_facebook' : link_facebook,
        'link_twitter' : link_twitter,
        'link_instagram' : link_instagram,
        'owl' : owl,
        'title' : meta_property_title,
        'author' : meta_author,
        'keywords' : meta_keywords,
        'description' : meta_property_description,
    }
    return render(request, 'profile/home/profil.html', x)

def paket(request):
    try: 
        foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
    except: 
        foto_diskon = ''

    try: 
        diskon = AppSetting.objects.get(nama = 'diskon')
    except: 
        diskon = ''

    try: 
        tanggal = AppSetting.objects.get(nama = 'tanggal')
    except: 
        tanggal = ''

    try: 
        email = AppSetting.objects.get(nama = 'email')
    except: 
        email = ''

    try: 
        rumah = AppSetting.objects.get(nama = 'telepon_text')
    except: 
        rumah = ''

    try: 
        wa = AppSetting.objects.get(nama = 'whatsapp_text')
    except: 
        wa = ''

    try: 
        text = AppSetting.objects.get(nama ='telepon')
    except: 
        text = ''

    try: 
        alamat = AppSetting.objects.get(nama ='alamat')
    except: 
        alamat = ''

    try: 
        alamat_link = AppSetting.objects.get(nama ='alamat_link')
    except: 
        alamat_link = ''

    try: 
        link_facebook = AppSetting.objects.get(nama ='link_facebook')
    except: 
        link_facebook = ''

    try: 
        link_twitter = AppSetting.objects.get(nama ='link_twitter')
    except: 
        link_twitter = ''

    try: 
        link_instagram = AppSetting.objects.get(nama ='link_instagram')
    except: 
        link_instagram = ''

    paket = Carousel.objects.filter(deleted_at = None)[:6]
    owl = RunningLink.objects.filter(deleted_at__isnull = True).order_by('-id')

    try: 
        meta_property_title = AppSetting.objects.get(nama ='meta_property_title')
    except: 
        meta_property_title = ''

    try: 
        meta_author = AppSetting.objects.get(nama ='meta_author')
    except: 
        meta_author = ''

    try: 
        meta_keywords = AppSetting.objects.get(nama ='meta_keywords')
    except: 
        meta_keywords = ''

    try: 
        meta_property_description = AppSetting.objects.get(nama ='meta_property_description')
    except: 
        meta_property_description = ''

    x = {
        'foto' : foto_diskon,
        'diskon' : diskon,
        'tanggal' : tanggal,
        'email' : email,
        'rumah' : rumah,
        'wa' : wa,
        'text' : text,
        'alamat' : alamat,
        'dt_paket' : paket,
        'alamat_link' : alamat_link,
        'link_facebook' : link_facebook,
        'link_twitter' : link_twitter,
        'link_instagram' : link_instagram,
        'owl' : owl,
        'title' : meta_property_title,
        'author' : meta_author,
        'keywords' : meta_keywords,
        'description' : meta_property_description,
    }
    return render(request, 'profile/home/paket.html', x)


def detail_paket(request, id):
    detail_paket = Carousel.objects.get(id=id)
    
    destinasi = Destinasi.objects.filter(paket = detail_paket.id, deleted_at__isnull=True)
    foto = DestinasiImage.objects.filter(destinasi__in=destinasi)

    nama_destinasi = Destinasi.objects.filter(paket=detail_paket.id, deleted_at__isnull=True).values_list('nama', flat=True)
    final_destinasi = ', '.join(nama_destinasi)

    paket_biasa = Carousel.objects.filter(deleted_at__isnull=True).exclude(kondisi__in=["Terlaris", "Popular"]).order_by('-id')[:2]
    paket_populer = Carousel.objects.filter(kondisi = "Popular", deleted_at__isnull=True).order_by('-id')[:2]
    paket_terlaris = Carousel.objects.filter(kondisi = "Terlaris", diskon__isnull = False, deleted_at__isnull=True).order_by('-id').first()

    try: 
        foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
    except: 
        foto_diskon = ''

    try: 
        diskon = AppSetting.objects.get(nama = 'diskon')
    except: 
        diskon = ''

    try: 
        tanggal = AppSetting.objects.get(nama = 'tanggal')
    except: 
        tanggal = ''

    try: 
        email = AppSetting.objects.get(nama = 'email')
    except: 
        email = ''

    try: 
        rumah = AppSetting.objects.get(nama = 'telepon_text')
    except: 
        rumah = ''

    try: 
        wa = AppSetting.objects.get(nama = 'whatsapp_text')
    except: 
        wa = ''

    try: 
        text = AppSetting.objects.get(nama ='telepon')
    except: 
        text = ''

    try: 
        alamat = AppSetting.objects.get(nama ='alamat')
    except: 
        alamat = ''

    try: 
        alamat_link = AppSetting.objects.get(nama ='alamat_link')
    except: 
        alamat_link = ''

    try: 
        link_facebook = AppSetting.objects.get(nama ='link_facebook')
    except: 
        link_facebook = ''

    try: 
        link_twitter = AppSetting.objects.get(nama ='link_twitter')
    except: 
        link_twitter = ''

    try: 
        link_instagram = AppSetting.objects.get(nama ='link_instagram')
    except: 
        link_instagram = ''

    paket = Carousel.objects.filter(deleted_at = None)[:6]
    owl = RunningLink.objects.filter(deleted_at__isnull = True).order_by('-id')

    try: 
        meta_property_title = AppSetting.objects.get(nama ='meta_property_title')
    except: 
        meta_property_title = ''

    try: 
        meta_author = AppSetting.objects.get(nama ='meta_author')
    except: 
        meta_author = ''

    try: 
        meta_keywords = AppSetting.objects.get(nama ='meta_keywords')
    except: 
        meta_keywords = ''

    try: 
        meta_property_description = AppSetting.objects.get(nama ='meta_property_description')
    except: 
        meta_property_description = ''

    x = {
        'foto' : foto_diskon,
        'diskon' : diskon,
        'tanggal' : tanggal,
        'email' : email,
        'rumah' : rumah,
        'wa' : wa,
        'text' : text,
        'alamat' : alamat,
        'dt_paket' : paket,
        'detpak' : detail_paket,
        'detfot' : foto,
        'destinasi' : destinasi,
        'namdes' : final_destinasi,
        'biasa' : paket_biasa,
        'populer' : paket_populer,
        'terlaris' : paket_terlaris,
        'alamat_link' : alamat_link,
        'link_facebook' : link_facebook,
        'link_twitter' : link_twitter,
        'link_instagram' : link_instagram,
        'owl' : owl,
        'title' : meta_property_title,
        'author' : meta_author,
        'keywords' : meta_keywords,
        'description' : meta_property_description,
    }
    return render(request, 'profile/home/detail_paket.html', x)

def cara_bayar(request):
    try: 
        image_cara_bayar = AppSetting.objects.get(nama ='image_cara_bayar')
    except: 
        image_cara_bayar = ''

    try: 
        highlight = AppSetting.objects.get(nama ='highlight')
    except: 
        highlight = ''

    try: 
        deskripsi = AppSetting.objects.get(nama ='deskripsi')
    except: 
        deskripsi = ''

    try: 
        embed_maps_bayar = AppSetting.objects.get(nama ='embed_maps_bayar')
    except: 
        embed_maps_bayar = ''

    try: 
        bank = AppSetting.objects.get(nama ='bank')
    except: 
        bank = ''

    try: 
        norek = AppSetting.objects.get(nama ='norek')
    except: 
        norek = ''

    try: 
        an = AppSetting.objects.get(nama ='an')
    except: 
        an = ''

    faq = FAQ.objects.all()

    popular = Carousel.objects.filter(kondisi = "Popular", deleted_at__isnull=True).order_by('-id')[:2]
    terlaris = Carousel.objects.filter(kondisi = "Terlaris", diskon__isnull = False, deleted_at__isnull=True).order_by('-id').first()

    try: 
        foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
    except: 
        foto_diskon = ''

    try: 
        diskon = AppSetting.objects.get(nama = 'diskon')
    except: 
        diskon = ''

    try: 
        tanggal = AppSetting.objects.get(nama = 'tanggal')
    except: 
        tanggal = ''

    try: 
        email = AppSetting.objects.get(nama = 'email')
    except: 
        email = ''

    try: 
        rumah = AppSetting.objects.get(nama = 'telepon_text')
    except: 
        rumah = ''

    try: 
        wa = AppSetting.objects.get(nama = 'whatsapp_text')
    except: 
        wa = ''

    try: 
        text = AppSetting.objects.get(nama ='telepon')
    except: 
        text = ''

    try: 
        alamat = AppSetting.objects.get(nama ='alamat')
    except: 
        alamat = ''

    try: 
        alamat_link = AppSetting.objects.get(nama ='alamat_link')
    except: 
        alamat_link = ''

    try: 
        link_facebook = AppSetting.objects.get(nama ='link_facebook')
    except: 
        link_facebook = ''

    try: 
        link_twitter = AppSetting.objects.get(nama ='link_twitter')
    except: 
        link_twitter = ''

    try: 
        link_instagram = AppSetting.objects.get(nama ='link_instagram')
    except: 
        link_instagram = ''

    paket = Carousel.objects.filter(deleted_at = None)[:6]
    owl = RunningLink.objects.filter(deleted_at__isnull = True).order_by('-id')

    try: 
        meta_property_title = AppSetting.objects.get(nama ='meta_property_title')
    except: 
        meta_property_title = ''

    try: 
        meta_author = AppSetting.objects.get(nama ='meta_author')
    except: 
        meta_author = ''

    try: 
        meta_keywords = AppSetting.objects.get(nama ='meta_keywords')
    except: 
        meta_keywords = ''

    try: 
        meta_property_description = AppSetting.objects.get(nama ='meta_property_description')
    except: 
        meta_property_description = ''

    x = {
        'foto' : foto_diskon,
        'diskon' : diskon,
        'tanggal' : tanggal,
        'email' : email,
        'rumah' : rumah,
        'wa' : wa,
        'text' : text,
        'alamat' : alamat,
        'dt_paket' : paket,
        'image' : image_cara_bayar,
        'highlight' : highlight,
        'deskripsi' : deskripsi,
        'embed' : embed_maps_bayar,
        'bank' : bank,
        'norek' : norek,
        'an' : an,
        'faq' : faq,
        'popular' : popular,
        'terlaris' : terlaris,
        'alamat_link' : alamat_link,
        'link_facebook' : link_facebook,
        'link_twitter' : link_twitter,
        'link_instagram' : link_instagram,
        'owl' : owl,
        'title' : meta_property_title,
        'author' : meta_author,
        'keywords' : meta_keywords,
        'description' : meta_property_description,
    }
    return render(request, 'profile/home/carabayar.html', x)

def galeri(request):
    galeri = Gallery.objects.filter(deleted_at = None).order_by('id')

    try: 
        foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
    except: 
        foto_diskon = ''

    try: 
        diskon = AppSetting.objects.get(nama = 'diskon')
    except: 
        diskon = ''

    try: 
        tanggal = AppSetting.objects.get(nama = 'tanggal')
    except: 
        tanggal = ''

    try: 
        email = AppSetting.objects.get(nama = 'email')
    except: 
        email = ''

    try: 
        rumah = AppSetting.objects.get(nama = 'telepon_text')
    except: 
        rumah = ''

    try: 
        wa = AppSetting.objects.get(nama = 'whatsapp_text')
    except: 
        wa = ''

    try: 
        text = AppSetting.objects.get(nama ='telepon')
    except: 
        text = ''

    try: 
        alamat = AppSetting.objects.get(nama ='alamat')
    except: 
        alamat = ''

    try: 
        alamat_link = AppSetting.objects.get(nama ='alamat_link')
    except: 
        alamat_link = ''

    try: 
        link_facebook = AppSetting.objects.get(nama ='link_facebook')
    except: 
        link_facebook = ''

    try: 
        link_twitter = AppSetting.objects.get(nama ='link_twitter')
    except: 
        link_twitter = ''

    try: 
        link_instagram = AppSetting.objects.get(nama ='link_instagram')
    except: 
        link_instagram = ''


    paket = Carousel.objects.filter(deleted_at__isnull=True).order_by('-id')[:6]
    owl = RunningLink.objects.filter(deleted_at__isnull = True).order_by('-id')
    try: 
        meta_property_title = AppSetting.objects.get(nama ='meta_property_title')
    except: 
        meta_property_title = ''

    try: 
        meta_author = AppSetting.objects.get(nama ='meta_author')
    except: 
        meta_author = ''

    try: 
        meta_keywords = AppSetting.objects.get(nama ='meta_keywords')
    except: 
        meta_keywords = ''

    try: 
        meta_property_description = AppSetting.objects.get(nama ='meta_property_description')
    except: 
        meta_property_description = ''

    x = {
        'foto' : foto_diskon,
        'diskon' : diskon,
        'tanggal' : tanggal,
        'email' : email,
        'rumah' : rumah,
        'wa' : wa,
        'text' : text,
        'alamat' : alamat,
        'dt_paket' : paket,
        'dt_galeri' : galeri,
        'alamat_link' : alamat_link,
        'link_facebook' : link_facebook,
        'link_twitter' : link_twitter,
        'link_instagram' : link_instagram,
        'owl' : owl,
        'title' : meta_property_title,
        'author' : meta_author,
        'keywords' : meta_keywords,
        'description' : meta_property_description,
    }
    return render(request, 'profile/home/galeri.html', x)

def berita(request):
    try: 
        foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
    except: 
        foto_diskon = ''

    try: 
        diskon = AppSetting.objects.get(nama = 'diskon')
    except: 
        diskon = ''

    try: 
        tanggal = AppSetting.objects.get(nama = 'tanggal')
    except: 
        tanggal = ''

    try: 
        email = AppSetting.objects.get(nama = 'email')
    except: 
        email = ''

    try: 
        rumah = AppSetting.objects.get(nama = 'telepon_text')
    except: 
        rumah = ''

    try: 
        wa = AppSetting.objects.get(nama = 'whatsapp_text')
    except: 
        wa = ''

    try: 
        text = AppSetting.objects.get(nama ='telepon')
    except: 
        text = ''

    try: 
        alamat = AppSetting.objects.get(nama ='alamat')
    except: 
        alamat = ''

    try: 
        alamat_link = AppSetting.objects.get(nama ='alamat_link')
    except: 
        alamat_link = ''

    try: 
        link_facebook = AppSetting.objects.get(nama ='link_facebook')
    except: 
        link_facebook = ''

    try: 
        link_twitter = AppSetting.objects.get(nama ='link_twitter')
    except: 
        link_twitter = ''

    try: 
        link_instagram = AppSetting.objects.get(nama ='link_instagram')
    except: 
        link_instagram = ''

    paket = Carousel.objects.filter(deleted_at__isnull=True).order_by('-id')[:6]

    berita = News.objects.filter(deleted_at=None).order_by('-id')

    paginator = Paginator(berita, 4)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    cat = Category.objects.filter(typexs="news", news__deleted_at__isnull=True).exclude(news__isnull=True).order_by('id').annotate(news_count=Count('news'))
    gallery = Gallery.objects.filter(deleted_at=None).order_by('id')[:9]
    
    owl = RunningLink.objects.filter(deleted_at__isnull = True).order_by('-id')
    try: 
        meta_property_title = AppSetting.objects.get(nama ='meta_property_title')
    except: 
        meta_property_title = ''

    try: 
        meta_author = AppSetting.objects.get(nama ='meta_author')
    except: 
        meta_author = ''

    try: 
        meta_keywords = AppSetting.objects.get(nama ='meta_keywords')
    except: 
        meta_keywords = ''

    try: 
        meta_property_description = AppSetting.objects.get(nama ='meta_property_description')
    except: 
        meta_property_description = ''

    x = {
        'foto' : foto_diskon,
        'diskon' : diskon,
        'tanggal' : tanggal,
        'email' : email,
        'rumah' : rumah,
        'wa' : wa,
        'text' : text,
        'alamat' : alamat,
        'dt_paket' : paket,
        'page_obj' : page_obj,
        'cat' : cat,
        'gal' : gallery,
        'alamat_link' : alamat_link,
        'link_facebook' : link_facebook,
        'link_twitter' : link_twitter,
        'link_instagram' : link_instagram,
        'owl' : owl,
        'title' : meta_property_title,
        'author' : meta_author,
        'keywords' : meta_keywords,
        'description' : meta_property_description,
    }
    return render(request, 'profile/home/berita.html', x)

def kategori(request, id):
    try: 
        foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
    except: 
        foto_diskon = ''

    try: 
        diskon = AppSetting.objects.get(nama = 'diskon')
    except: 
        diskon = ''

    try: 
        tanggal = AppSetting.objects.get(nama = 'tanggal')
    except: 
        tanggal = ''

    try: 
        email = AppSetting.objects.get(nama = 'email')
    except: 
        email = ''

    try: 
        rumah = AppSetting.objects.get(nama = 'telepon_text')
    except: 
        rumah = ''

    try: 
        wa = AppSetting.objects.get(nama = 'whatsapp_text')
    except: 
        wa = ''

    try: 
        text = AppSetting.objects.get(nama ='telepon')
    except: 
        text = ''

    try: 
        alamat = AppSetting.objects.get(nama ='alamat')
    except: 
        alamat = ''

    paket = Carousel.objects.filter(deleted_at__isnull=True).order_by('-id')[:6]
    try: 
        alamat_link = AppSetting.objects.get(nama ='alamat_link')
    except: 
        alamat_link = ''

    try: 
        link_facebook = AppSetting.objects.get(nama ='link_facebook')
    except: 
        link_facebook = ''

    try: 
        link_twitter = AppSetting.objects.get(nama ='link_twitter')
    except: 
        link_twitter = ''

    try: 
        link_instagram = AppSetting.objects.get(nama ='link_instagram')
    except: 
        link_instagram = ''


    berita = News.objects.filter(deleted_at=None, category = id).order_by('-id')

    paginator = Paginator(berita, 4)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    cat = Category.objects.filter(typexs="news").order_by('id').annotate(news_count=Count('news'))
    gallery = Gallery.objects.filter(deleted_at=None).order_by('id')[:9]
    owl = RunningLink.objects.filter(deleted_at__isnull = True).order_by('-id')
    try: 
        meta_property_title = AppSetting.objects.get(nama ='meta_property_title')
    except: 
        meta_property_title = ''

    try: 
        meta_author = AppSetting.objects.get(nama ='meta_author')
    except: 
        meta_author = ''

    try: 
        meta_keywords = AppSetting.objects.get(nama ='meta_keywords')
    except: 
        meta_keywords = ''

    try: 
        meta_property_description = AppSetting.objects.get(nama ='meta_property_description')
    except: 
        meta_property_description = ''

    x = {
        'foto' : foto_diskon,
        'diskon' : diskon,
        'tanggal' : tanggal,
        'email' : email,
        'rumah' : rumah,
        'wa' : wa,
        'text' : text,
        'alamat' : alamat,
        'dt_paket' : paket,
        'page_obj' : page_obj,
        'cat' : cat,
        'gal' : gallery,
        'alamat_link' : alamat_link,
        'link_facebook' : link_facebook,
        'link_twitter' : link_twitter,
        'link_instagram' : link_instagram,
        'owl' : owl,
        'title' : meta_property_title,
        'author' : meta_author,
        'keywords' : meta_keywords,
        'description' : meta_property_description,
    }
    return render(request, 'profile/home/berita.html', x)

def detail_berita(request, slug):
    try: 
        foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
    except: 
        foto_diskon = ''

    try: 
        diskon = AppSetting.objects.get(nama = 'diskon')
    except: 
        diskon = ''

    try: 
        tanggal = AppSetting.objects.get(nama = 'tanggal')
    except: 
        tanggal = ''

    try: 
        email = AppSetting.objects.get(nama = 'email')
    except: 
        email = ''

    try: 
        rumah = AppSetting.objects.get(nama = 'telepon_text')
    except: 
        rumah = ''

    try: 
        wa = AppSetting.objects.get(nama = 'whatsapp_text')
    except: 
        wa = ''

    try: 
        text = AppSetting.objects.get(nama ='telepon')
    except: 
        text = ''

    try: 
        alamat = AppSetting.objects.get(nama ='alamat')
    except: 
        alamat = ''

    try: 
        alamat_link = AppSetting.objects.get(nama ='alamat_link')
    except: 
        alamat_link = ''

    try: 
        link_facebook = AppSetting.objects.get(nama ='link_facebook')
    except: 
        link_facebook = ''

    try: 
        link_twitter = AppSetting.objects.get(nama ='link_twitter')
    except: 
        link_twitter = ''

    try: 
        link_instagram = AppSetting.objects.get(nama ='link_instagram')
    except: 
        link_instagram = ''


    paket = Carousel.objects.all()

    berita = News.objects.get(slug=slug)

    news = News.objects.filter(deleted_at = None).order_by('-id')[:2]
    obj = News.objects.filter(deleted_at = None).order_by('-id')
    paginator = Paginator(obj, 3)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    pak = Carousel.objects.filter(deleted_at__isnull=True).order_by('-id')[:6]

    # nama_destinasi = News.objects.get(slug=slug).values_list('tags', flat=True)
    # final_destinasi = ', '.join(nama_destinasi)

    # cat = Category.objects.filter(typexs="news").order_by('id').annotate(news_count=Count('news'))
    # gallery = Gallery.objects.filter(deleted_at=None).order_by('id')[:9]
    owl = RunningLink.objects.filter(deleted_at__isnull = True).order_by('-id')
    try: 
        meta_property_title = AppSetting.objects.get(nama ='meta_property_title')
    except: 
        meta_property_title = ''

    try: 
        meta_author = AppSetting.objects.get(nama ='meta_author')
    except: 
        meta_author = ''

    try: 
        meta_keywords = AppSetting.objects.get(nama ='meta_keywords')
    except: 
        meta_keywords = ''

    try: 
        meta_property_description = AppSetting.objects.get(nama ='meta_property_description')
    except: 
        meta_property_description = ''

    x = {
        'foto' : foto_diskon,
        'diskon' : diskon,
        'tanggal' : tanggal,
        'email' : email,
        'rumah' : rumah,
        'wa' : wa,
        'text' : text,
        'alamat' : alamat,
        'dt_paket' : paket,
        'page_obj' : berita,
        'news' : news,
        'obj' : page_obj,
        'pak' : pak,
        'alamat_link' : alamat_link,
        'link_facebook' : link_facebook,
        'link_twitter' : link_twitter,
        'link_instagram' : link_instagram,
        'owl' : owl,
        'title' : meta_property_title,
        'author' : meta_author,
        'keywords' : meta_keywords,
        'description' : meta_property_description,
    }
    return render(request, 'profile/home/detail_berita.html', x)

def kontak(request):
    try: 
        foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
    except: 
        foto_diskon = ''

    try: 
        diskon = AppSetting.objects.get(nama = 'diskon')
    except: 
        diskon = ''

    try: 
        tanggal = AppSetting.objects.get(nama = 'tanggal')
    except: 
        tanggal = ''

    try: 
        email = AppSetting.objects.get(nama = 'email')
    except: 
        email = ''

    try: 
        rumah = AppSetting.objects.get(nama = 'telepon_text')
    except: 
        rumah = ''

    try: 
        wa = AppSetting.objects.get(nama = 'whatsapp_text')
    except: 
        wa = ''

    try: 
        text = AppSetting.objects.get(nama ='telepon')
    except: 
        text = ''

    try: 
        alamat = AppSetting.objects.get(nama ='alamat')
    except: 
        alamat = ''

    try: 
        alamat_link = AppSetting.objects.get(nama ='alamat_link')
    except: 
        alamat_link = ''

    try: 
        link_facebook = AppSetting.objects.get(nama ='link_facebook')
    except: 
        link_facebook = ''

    try: 
        link_twitter = AppSetting.objects.get(nama ='link_twitter')
    except: 
        link_twitter = ''

    try: 
        link_instagram = AppSetting.objects.get(nama ='link_instagram')
    except: 
        link_instagram = ''

    paket = Carousel.objects.filter(deleted_at__isnull=True).order_by('-id')
    owl = RunningLink.objects.filter(deleted_at__isnull = True).order_by('-id')

    try: 
        meta_property_title = AppSetting.objects.get(nama ='meta_property_title')
    except: 
        meta_property_title = ''

    try: 
        meta_author = AppSetting.objects.get(nama ='meta_author')
    except: 
        meta_author = ''

    try: 
        meta_keywords = AppSetting.objects.get(nama ='meta_keywords')
    except: 
        meta_keywords = ''

    try: 
        meta_property_description = AppSetting.objects.get(nama ='meta_property_description')
    except: 
        meta_property_description = ''
    x = {
        'foto' : foto_diskon,
        'diskon' : diskon,
        'tanggal' : tanggal,
        'email' : email,
        'rumah' : rumah,
        'wa' : wa,
        'text' : text,
        'alamat' : alamat,
        'dt_paket' : paket,
        'alamat_link' : alamat_link,
        'link_facebook' : link_facebook,
        'link_twitter' : link_twitter,
        'link_instagram' : link_instagram,
        'owl' : owl,
        'title' : meta_property_title,
        'author' : meta_author,
        'keywords' : meta_keywords,
        'description' : meta_property_description,
    }
    return render(request, 'profile/home/kontak.html', x)

def render_laporan(request):
    template_html = 'profile/home/laporanpenjualan.html'
    data = {
		'app_url': 'http://localhost:8000',
		# 'dt_penjualan':dari model / database,
		# selanjutnya optional,
	}
    
    html = render_to_string(template_html, data)
    
    payload = {
		'template_src':html,
		'type_document': 'pdf'
	}
    render_template = requests.post('https://reportingservice.sanjayateknologi.com', data = payload)
    
    if payload['type_document'] == 'html':
        return HttpResponse(render_template.content)
    else:
        return HttpResponse(render_template.content, content_type='application/pdf')

def prev(request, id):
    # mengambil 8 data produk dengan id kurang dari 1000
    post = Product.objects.filter(id__lt=id).order_by('-id').first()
    if not post:
        post = Product.objects.last()
    return redirect('profile:home')

def next(request, id):
    post = Product.objects.filter(id__gt=id).order_by('id').first()
    if not post:
        post = Product.objects.first()
    return redirect('profile:home')

@require_http_methods(["GET"])
def ajax(request):
    about = AboutMe.objects.get(jenis='header')
    typexs = 'product'
    categories = Category.objects.filter(typexs=typexs).exclude(nama='promo').order_by('-id')
    context = {
        'title' : 'Category',
        'title2' :  'Product',
        'about' : about,
        'data' : categories,
    }
    return render(request, 'profile/ajax/detail_bestsel.html', context)

@require_http_methods(["GET"])
def produk(request, id):
    produk = Category.objects.get(id=id)
    about = AboutMe.objects.get(jenis='header')
    kedua = produk.nama
    data = Product.objects.filter(category = id).order_by('-id')
    print(data)
    try: 
        whatsapp = AppSetting.objects.get(nama = 'whatsapp_text')
        wa = whatsapp.keterangan
    except: 
        whatsapp = ''
    context = {
        'title' : 'View More',
        'title2' :  kedua,
        'about' : about,
        'produk' : produk,
        'haha' : data,
        'wa' : wa,
    }
    return render(request, 'profile/ajax/produk.html', context)


def contact(request):
    if request.method == 'GET':
        try: 
            meta_property_title = AppSetting.objects.get(nama = 'meta_property_title')
        except: 
            meta_property_title = ''
        test = meta_property_title.keterangan

        try: 
            telepon = AppSetting.objects.get(nama = 'telepon')
            no = telepon.keterangan
        except: 
            telepon = ''

        try: 
            whatsapp = AppSetting.objects.get(nama = 'whatsapp')
            wa = whatsapp.keterangan
        except: 
            whatsapp = ''

        try: 
            alamat = AppSetting.objects.get(nama = 'alamat')
            addres = alamat.keterangan
        except: 
            alamat = ''

        try: 
            surel = AppSetting.objects.get(nama = 'email')
            email = surel.keterangan
        except: 
            surel = ''
        print(test)
        context = {
            'title' : 'Contact',
            'meta_property_title' : test,
            'nomor' : no,
            'wa' : wa,
            'addres' : addres,
            'email' : email,
        }
        return render(request, 'profile/contact/contact.html', context)
    
@require_http_methods(["GET"])
def supplier(request):
    try: 
        whatsapp = AppSetting.objects.get(nama = 'whatsapp')
        wa = whatsapp.keterangan
    except: 
        whatsapp = ''

    try: 
        telepon = AppSetting.objects.get(nama = 'telepon')
        no = telepon.keterangan
    except: 
        telepon = ''
        
    about = AboutMe.objects.get(jenis='header')
    context = {
        'title' : 'Reseller and Wholesale',
        'wa' : wa,
        'nomor' : no,
        'about' : about,
    }
    return render(request, 'profile/contact/suplier.html', context)

