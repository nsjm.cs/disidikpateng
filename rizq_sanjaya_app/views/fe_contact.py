from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News
from django.utils import timezone
from django.conf import settings
from datetime import datetime
from ..decorators import *
from django.db import connections
from django.db.models import Q, Count
from ..helpers import dictfetchall, get_list_berita, get_category, get_list_galeri
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
import os
import pprint
from django.template.loader import render_to_string
import requests
from taggit.models import Tag

@require_http_methods(["GET"])
def index(request):
    titla_head   = 'HELPDESK PENGADUAN'
    subtitle     = 'Dinas Pendidikan & Kebudayaan Provinsi Papua Tengah'
    
    context = {
         'title_head':titla_head,
         'subtitle':subtitle,
    }
    return render(request, 'profile/layouts/frontend/kontak/index.html', context)


