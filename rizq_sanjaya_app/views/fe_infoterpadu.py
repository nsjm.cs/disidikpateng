from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News
from django.utils import timezone
from django.conf import settings
from datetime import datetime
from ..decorators import *
from django.db import connections
from django.db.models import Q, Count
from ..helpers import dictfetchall, get_list_berita, get_category, get_list_galeri
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
import os
import pprint
from django.template.loader import render_to_string
import requests
from taggit.models import Tag

@require_http_methods(["GET"])
def index_infografis(request):
   titla_head   = 'INFOGRAFIS'
   subtitle     = 'Dinas Pendidikan & Kebudayaan Provinsi Papua Tengah'
   list_infografis = InfoTerpadu.objects.filter(status='Publish',typexs='infografis').order_by('-id')[:9]
    
   news_side =  News.objects.filter(deleted_at = None).order_by('-id')[:5]
   category_list =  Category.objects.order_by('-id')
   tags = Tag.objects.all()
    
   paginator = Paginator(list_infografis, 8)
   page = request.GET.get('page', 1)

   try:
    list_infografis = paginator.page(page)
   except PageNotAnInteger:
     list_infografis = paginator.page(1)
   except EmptyPage:
     list_infografis = paginator.page(paginator.num_pages)
   context = {
     
         'title_head':titla_head,
         'subtitle':subtitle,
         'news_side_list':news_side,
         'list_infografis':list_infografis,
         'category_list':category_list,
         'tag_list': tags,
    }
   return render(request, 'profile/layouts/frontend/infoterpadu/infografis.html', context)

@require_http_methods(["GET"])
def index_infopublik(request):
   titla_head   = 'INFO PUBLIK'
   subtitle     = 'Dinas Pendidikan & Kebudayaan Provinsi Papua Tengah'
   list_infopublik = InfoTerpadu.objects.filter(status='Publish',typexs='infopublik').order_by('-id')[:6]
    
   news_side =  News.objects.filter(deleted_at = None).order_by('-id')[:5]
   category_list =  Category.objects.order_by('-id')
   tags = Tag.objects.all()
    
   paginator = Paginator(list_infopublik, 8)
   page = request.GET.get('page', 1)

   try:
    list_infopublik = paginator.page(page)
   except PageNotAnInteger:
     list_infopublik = paginator.page(1)
   except EmptyPage:
     list_infopublik = paginator.page(paginator.num_pages)
   context = {
     
         'title_head':titla_head,
         'subtitle':subtitle,
         'news_side_list':news_side,
         'list_infopublik':list_infopublik,
         'category_list':category_list,
         'tag_list': tags,
    }
   return render(request, 'profile/layouts/frontend/infoterpadu/infopublik.html', context)

@require_http_methods(["GET"])
def index_infoagenda(request):
   titla_head   = 'Agenda Kegiatan'
   subtitle     = 'Dinas Pendidikan & Kebudayaan Provinsi Papua Tengah'
   list_infoagenda_top = InfoTerpadu.objects.filter(status='Publish',typexs='agenda').order_by('-id')[:3]
   list_infoagenda = InfoTerpadu.objects.filter(status='Publish',typexs='agenda').order_by('-id')[:6]
    
   jml_data = 3
   news = News.objects.filter(deleted_at=None).order_by('-created_at')
   nws_jml = int(len(news))
   if nws_jml >= jml_data:
    thumbnail = {}
    for x in news:
     x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))
     x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/')}
    news_list = news

   else:
    news_list = get_list_berita(jml_data)
   category_list =  Category.objects.order_by('-id')
   tags = Tag.objects.all()
    
   paginator = Paginator(list_infoagenda, 8)
   page = request.GET.get('page', 1)

   try:
    list_infoagenda = paginator.page(page)
   except PageNotAnInteger:
     list_infoagenda = paginator.page(1)
   except EmptyPage:
     list_infoagenda = paginator.page(paginator.num_pages)
   context = {
     
         'title_head':titla_head,
         'subtitle':subtitle,
         'news_side':news_list,
         'top_agenda':list_infoagenda_top,
         'list_agenda':list_infoagenda,
         'category_list':category_list,
         'tag_list': tags,
    }
   return render(request, 'profile/layouts/frontend/infoterpadu/agenda.html', context)

@require_http_methods(["GET"])
def admin_detail_agenda(request, slug):
     jml_data = 6
     news = News.objects.filter(deleted_at=None).order_by('-created_at')
     nws_jml = int(len(news))
     if nws_jml >= jml_data:
      thumbnail = {}
      for x in news:
        x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))
        x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/')}
      news_list = news

     else:
      news_list = get_list_berita(jml_data)

     detail_agenda = InfoTerpadu.objects.filter(status ='Publish', slug=slug)
     list_infoagenda = InfoTerpadu.objects.filter(status='Publish',typexs='agenda').order_by('-id')[:6]
     category_list =  Category.objects.order_by('-id')
     tags = Tag.objects.all()
     template = 'profile/layouts/frontend/infoterpadu/detail_agenda.html'  
     context = {
          'title' : 'Detail Agenda Kegiatan',
          'news_side' : news_list,
          'category_list':category_list,
          'detail_agenda':detail_agenda,
          'list_infoagenda':list_infoagenda,
          'tag_list': tags,

     }
     return render(request, template, context)

@require_http_methods(["GET"])
def index_infokalender(request):
   titla_head   = 'Kalender Pendidikan'
   subtitle     = 'Dinas Pendidikan & Kebudayaan Provinsi Papua Tengah'
   jml_data = 4
   news = News.objects.filter(deleted_at=None).order_by('-created_at')
   nws_jml = int(len(news))
   if nws_jml >= jml_data:
    thumbnail = {}
    for x in news:
      x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))
      x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/')}
    news_list = news
   else:
     news_list = get_list_berita(jml_data)
   list_kalender = InfoTerpadu.objects.filter(status='Publish',typexs='kalender pendidikan').order_by('-id')[:6]
    
   list_infoagenda_top = InfoTerpadu.objects.filter(status='Publish',typexs='agenda').order_by('-id')[:3]
   category_list =  Category.objects.order_by('-id')

    
   paginator = Paginator(list_kalender, 8)
   page = request.GET.get('page', 1)

   try:
    list_kalender = paginator.page(page)
   except PageNotAnInteger:
     list_kalender = paginator.page(1)
   except EmptyPage:
     list_kalender = paginator.page(paginator.num_pages)
   context = {
     
         'title_head':titla_head,
         'subtitle':subtitle,
         'news_side':news_list,
         'list_kalender':list_kalender,
         'category_list':category_list,
         'list_infoagenda_top':list_infoagenda_top,

    }
   return render(request, 'profile/layouts/frontend/infoterpadu/kalender_pendidikan.html', context)

@require_http_methods(["GET"])
def admin_detail_kalender(request, slug):
     jml_data = 6
     news = News.objects.filter(deleted_at=None).order_by('-created_at')
     nws_jml = int(len(news))
     if nws_jml >= jml_data:
      thumbnail = {}
      for x in news:
        x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))
        x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/')}
      news_list = news

     else:
      news_list = get_list_berita(jml_data)

     detail_kalender_pendidikan = InfoTerpadu.objects.filter(status ='Publish', slug=slug)
     category_list =  Category.objects.order_by('-id')
     tags = Tag.objects.all()
     template = 'profile/layouts/frontend/infoterpadu/detail_kalender.html'  
     context = {
          'title' : 'Detail Agenda Kegiatan',
          'news_side' : news_list,
          'category_list':category_list,
          'detail_kalender_pendidikan':detail_kalender_pendidikan,
          'tag_list': tags,

     }
     return render(request, template, context)

@require_http_methods(["GET"])
def index_inforegulasi(request):
   titla_head   = 'Regulasi & Aturan'
   subtitle     = 'Dinas Pendidikan & Kebudayaan Provinsi Papua Tengah'
   jml_data = 6
   news = News.objects.filter(deleted_at=None).order_by('-created_at')
   nws_jml = int(len(news))
   if nws_jml >= jml_data:
    thumbnail = {}
    for x in news:
      x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))
      x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/')}
    news_list = news
   else:
     news_list = get_list_berita(jml_data)
   list_regulasi = InfoTerpadu.objects.filter(status='Publish',typexs='regulasi').order_by('-id')[:6]
   category_list =  Category.objects.order_by('-id')

    
   paginator = Paginator(list_regulasi, 8)
   page = request.GET.get('page', 1)

   try:
    list_regulasi = paginator.page(page)
   except PageNotAnInteger:
     list_regulasi = paginator.page(1)
   except EmptyPage:
     list_regulasi = paginator.page(paginator.num_pages)
   context = {
     
         'title':titla_head,
         'subtitle':subtitle,
         'news_side':news_list,
         'list_regulasi':list_regulasi,
         'category_list':category_list,

    }
   return render(request, 'profile/layouts/frontend/infoterpadu/regulasi.html', context)