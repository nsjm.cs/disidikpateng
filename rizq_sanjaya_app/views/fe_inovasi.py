from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News
from django.utils import timezone
from django.conf import settings
from datetime import datetime
from ..decorators import *
from django.db import connections
from django.db.models import Q, Count
from ..helpers import dictfetchall, querytags, get_list_berita, get_category, get_list_galeri
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
import os
import pprint
from django.template.loader import render_to_string
import requests
from taggit.models import Tag

@require_http_methods(["GET"])
def index(request):
    titla_head   = 'INOVASI'
    subtitle     = 'Dinas Pendidikan & Kebudayaan Provinsi Papua Tengah'
    list_inovasi = News.objects.filter(deleted_at = None, category__typexs='inovasi').order_by('-id')[:8]
    
    jml_data = 6
    news = News.objects.filter(deleted_at=None).order_by('-created_at')
    nws_jml = int(len(news))
    if nws_jml >= jml_data:
        thumbnail = {}
        for x in news:
            x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))
            x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/')}
        news_list = news
        category_list =  Category.objects.order_by('-id')
    else:
        news_list = get_list_berita(jml_data)
        category_list =  Category.objects.order_by('-id')
        
    
    paginator = Paginator(list_inovasi, 8)
    page = request.GET.get('page', 1)

    try:
     list_inovasi = paginator.page(page)
    except PageNotAnInteger:
     list_inovasi = paginator.page(1)
    except EmptyPage:
     list_inovasi = paginator.page(paginator.num_pages)
    context = {
         'title_head':titla_head,
         'subtitle':subtitle,
         'news_side':news_list,
         'list_inovasi':list_inovasi,
         'category_list':category_list,
         'dt_tags': querytags('news'),
    }
    return render(request, 'profile/layouts/frontend/inovasi/inovasi.html', context)

@require_http_methods(["GET"])
def admin_detail(request, slug):
     
     list_inovasi = News.objects.filter(deleted_at = None).order_by('-id')[:8]
     category_list =  Category.objects.order_by('-id')
     jml_data = 6
     news = News.objects.filter(deleted_at=None,category__typexs='inovasi').order_by('-created_at')
     nws_jml = int(len(news))
     if nws_jml >= jml_data:
         thumbnail = {}
         for x in news:
             x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))
             x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/')}
         news_list = news
     else:
         news_list = get_list_berita(jml_data)
         category_list =  Category.objects.order_by('-id')
         tags = Tag.objects.all()
     template = 'profile/layouts/frontend/inovasi/detail.html'  
     try:
         news = News.objects.get(slug=slug)
          
     except News.DoesNotExist:
         news = None
     context = {
          'title' : news.title if news != None else 'TIDAK DITEMUKAN',
          'news' : news,
          'news_side' : news_list,
          'category_list':category_list,
          'list_inovasi':list_inovasi,
          'tag_list': tags,

     }
     return render(request, template, context)

@require_http_methods(["GET"])
def admin_by_category(request, slug):
     template = 'profile/layouts/frontend/inovasi/bycategory.html'  
     slugs = slug
     
     list_news = News.objects.filter(deleted_at = None).order_by('-id')[:8]
     category_list =  Category.objects.order_by('-id')

     jml_data = 6
     news = News.objects.filter(deleted_at=None,category__typexs='inovasi').order_by('-created_at')
     nws_jml = int(len(news))
     if nws_jml >= jml_data:
         thumbnail = {}
         for x in news:
             x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))
             x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/')}
         news_list = news
     else:
         news_list = get_list_berita(jml_data)
         category_list =  Category.objects.order_by('-id')
         tags = Tag.objects.all()
     try:
         news = News.objects.filter(category__slug=slug)

     except News.DoesNotExist:
         news = None
     context = {
          'title_by':slugs,
          'news_bycategory' : news,
          'news_side' : news_list,
          'category_list':category_list,
          'list_news':list_news,
          'tag_list': tags,

     }
     return render(request, template, context)

