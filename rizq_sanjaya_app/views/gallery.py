import os

from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import Gallery
from django.utils import timezone
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db import connections
from ..helpers import dictfetchall, set_categori_galeri


@require_http_methods(["GET"])
def index(request):
    # gallery = Gallery.objects.filter(deleted_at=None).order_by('-created_at')
    # favorits = News.objects.filter(deleted_at=None).order_by('-seen')[:5]

    # runlink = RunningLink.objects.filter(deleted_at=None).order_by('id')
    # for x in runlink:
    #     x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.logo))

    # page = request.GET.get('page', 1)
    # paginator = Paginator(gallery, 15)

    # try:
    #     gallery = paginator.page(page)
    # except PageNotAnInteger:
    #     gallery = paginator.page(1)
    # except EmptyPage:
    #     gallery = paginator.page(paginator.num_pages)

    # for x in gallery:
    #     if (x.foto):
    #         x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))

    # with connections['default'].cursor() as cursor:
    #     cursor.execute("""
    #         SELECT COUNT(a.tag_id) as jml, c.name, c.slug, b.model FROM taggit_taggeditem a
    #             LEFT JOIN django_content_type b ON a.content_type_id = b.id
    #             LEFT JOIN taggit_tag c ON a.tag_id = c.id
    #         WHERE b.model = 'gallery'
    #         GROUP BY a.tag_id, c.name, c.slug, b.model
    #         ORDER BY c.name;""")
        # dt_tags = dictfetchall(cursor)

    context = {
        'title' : 'Galeri',
        # 'galleries' : gallery,
        # 'favs' : favorits,
        # 'runlink' : runlink,
        # 'dt_tags': dt_tags,
    }
    return render(request, 'profile/gallery/index.html', context)

@require_http_methods(["GET"])
def detail(request, slug):
    template = 'profile/gallery/detail.html'
    try:
        gallery = Gallery.objects.get(slug=slug)
    except Gallery.DoesNotExist:
        gallery = None
    context = {
        'title' : gallery.title if gallery != None else 'TIDAK DITEMUKAN',
        'gallery' : gallery,
    }
    return render(request, template, context)

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    gallery = Gallery.objects.filter(deleted_at__isnull=True).order_by('-created_at', )
    paginator = Paginator(gallery, 20)
    try:
        gallery = paginator.page(page)
    except PageNotAnInteger:
        gallery = paginator.page(1)
    except EmptyPage:
        gallery = paginator.page(paginator.num_pages)

    # for x in gallery:
    #     if (x.foto):
    #         x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))
        
    archives = Gallery.objects.filter(deleted_at__isnull=False).order_by('-created_at')
    context = {
        'title' : 'Galeri - Admin',
        'galleries' : gallery,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/gallery/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        context = {
            'title' : 'ADD GALLERY',
        }

        template = 'profile/admin/gallery/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        foto = request.FILES.get('foto')
        # judul = request.POST.get('judul')
        # keterangan = request.POST.get('keterangan')
        is_valid = True
        if is_valid:
            gallery = Gallery()
            user = Account.objects.get(id= request.user.id)
            gallery.created_by = user
            gallery.last_updated_by = request.user.id
            gallery.foto = foto
            # gallery.title = judul
            # gallery.keterangan = keterangan
            gallery.save()

            ### INSERT KATEGORI GALERI FROM HELPER ============================================
            # set_categori_galeri(request.POST.getlist('categori'), gallery.id, 'add')

            # if request.POST.get('jenis')  == 'foto':
            #     foto = Gallery.objects.get(id = gallery.id)
            #     form_gallery = GalleryFotoForm(data=request.POST, files=request.FILES, instance=foto)
            #     form_gallery.save()

            # if request.POST.get('jenis') == 'video':
            #     link = request.POST.get('video')
            #     if len(link.split('watch?v=')) == 2:
            #         gallery.video = f"https://www.youtube.com/embed/{link.split('watch?v=')[1]}"
            #         gallery.save()
            #     if len(link.split('https://youtu.be/')) == 2:
            #         gallery.video = f"https://www.youtube.com/embed/{link.split('https://youtu.be/')[1]}"
            #         gallery.save()

            messages.success(request, 'Galeri berhasil disimpan.')
            return redirect('profile:admin_gallery')

        messages.error(request, 'Galeri gagal disimpan.')
        return render(request, 'profile/admin/gallery/create.html', {'form': form,})

@login_required
@is_verified()
def edit(request, slug):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        gallery = Gallery.objects.get(slug=slug)
        slug = gallery.slug


        context = {
            'title' : 'EDIT GALERI',
            'edit' : 'true',
            'slug' : slug,
            'gallery': gallery,
        }
        template = 'profile/admin/gallery/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        gallery = Gallery.objects.get(slug=slug)
        foto_old = bool(gallery.foto)

        if foto_old : 
            path_file_lama = f"{settings.MEDIA_ROOT}/{gallery.foto}"

        is_valid = True

        if is_valid:
            foto = request.FILES.get('foto')
            # judul = request.POST.get('judul')
            # keterangan = request.POST.get('keterangan')

            if len(request.FILES) > 0:
                if foto_old : 
                    if len(request.FILES) > 0:
                        try:
                            os.remove(path_file_lama)
                        except:
                            pass
            # gallery.title = judul
            # gallery.keterangan = keterangan
            gallery.last_updated_by = request.user.id
            if len(request.FILES) > 0:
                gallery.foto = foto
            gallery.save()

            messages.success(request, 'Galeri berhasil disimpan.')
            return redirect('profile:admin_gallery')

        messages.error(request, 'Galeri gagal disimpan.')
        return render(request, 'profile/admin/gallery/create.html', {'form': form})

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_detail(request, slug):
    template = 'profile/admin/gallery/detail.html'
    try:
        gallery = Gallery.objects.get(slug=slug)
    except Gallery.DoesNotExist:
        gallery = None
    context = {
        'title' : gallery.title if gallery != None else 'TIDAK DITEMUKAN',
        'gallery' : gallery,
    }
    return render(request, template, context)

@login_required
@is_verified()
def softDelete(request, slug):
    message = ''
    try:
        sekarang = timezone.now()
        doc = Gallery.objects.get(slug=slug)
        doc.deleted_at = sekarang
        doc.save()
        message = 'success'
    except Gallery.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, slug):
    message = ''
    try:
        doc = Gallery.objects.get(slug=slug)
        try:
            if doc.foto != None: doc.foto.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except Gallery.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, slug):
    message = ''
    try:
        doc = Gallery.objects.get(slug=slug)
        doc.deleted_at = None
        doc.save()
        message = 'success'
    except Gallery.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)