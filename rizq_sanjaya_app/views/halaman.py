import os

from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import Gallery
from django.utils import timezone
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db import connections
from ..helpers import dictfetchall, set_categori_galeri

@require_http_methods(["GET"])
def index_about(request):
	context = {
	'title' : 'Profile',
	}
	return render(request, 'profile/halaman/idx_about.html', context)

@require_http_methods(["GET"])
def index_contact(request):
	context = {
	'title' : 'Contact',
	}
	return render(request, 'profile/halaman/idx_contact.html', context)