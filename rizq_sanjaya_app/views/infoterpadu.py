from django.shortcuts import render, HttpResponse, redirect
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import InfoTerpadu
from django.utils import timezone
import os
from ..decorators import *
from datetime import datetime
from django.conf import settings


####################################START MANAGEMEN VIEW INFOGRAFIS####################################
@require_http_methods(["GET"])
def admin_index_infografis(request):
    infoterpadu = InfoTerpadu.objects.filter(typexs='infografis',status='Publish').order_by('-id')
    arsip = InfoTerpadu.objects.filter(typexs='infografis',status='Draft').order_by('-id')

    for x in infoterpadu:
        ext = os.path.splitext(settings.MEDIA_ROOT+str(x.dokumen))[-1].lower()

        if ext == '.pdf':
            x.filex = 'file-pdf'
        elif ext in ['.jpg','.jpeg','.png']:
            x.filex = 'file-image'
        else:
            x.filex = 'dash'

        if (x.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))  
            if(is_foto == False):
                x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'}

    for x in arsip:
        if (x.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))  
            if(is_foto == False):
                x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 

    context = {
        'title'            : 'Infografis',
        'data_infoterpadu' : infoterpadu,
        'arsip'            : arsip,
    }
    return render(request, 'profile/admin/infoterpadu/infografis/index.html', context)

####################################CREATE INFOGRAFIS#################################################
@login_required
@is_verified()
def admin_index_infografis_create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = InfoTerpaduForm()
        form_foto = InfoTerpaduImg()
        foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'}

        context = {
            'title' : 'ADD INFOGRAFIS',
            'form' : form,
            'form_foto' : form_foto,
            'infografis' : {'foto':foto},
        }

        template = 'profile/admin/infoterpadu/infografis/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        form = InfoTerpaduForm(data=request.POST)
        form_foto = InfoTerpaduImg(data=request.POST, files=request.FILES)
        if form.is_valid():
            new_dt = form.save(commit=False)
            new_dt.save()
            form.save_m2m()
            
            if form_foto.is_valid():
                get_dt = InfoTerpadu.objects.get(id = new_dt.id)
                dt_frm_img = InfoTerpaduImg(data=request.POST, files=request.FILES, instance=get_dt)
                dt_frm_img.save()
            
            messages.success(request, 'Infografis berhasil disimpan.')
            return redirect('profile:admin_infoterpadu_infografis')

        messages.error(request, 'Infografis gagal disimpan.')
        return render(request, 'profile/admin/infoterpadu/infografis/create.html', {'form': form,})

        

@login_required
@is_verified()
def admin_index_infografis_edit(request, slug):
    template = ''
    context = {} 
    form = InfoTerpaduForm()
    
    if request.method == 'GET':
        infografis = InfoTerpadu.objects.get(slug = slug, typexs='infografis')
        form = InfoTerpaduForm(instance=infografis)
        form_foto = InfoTerpaduImg()

        if (infografis.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(infografis.foto))  
            if(is_foto == False):
                infografis.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            infografis.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 

        context = {
            'title' : 'EDIT INFOGRAFIS',
            'form' : form,
            'form_foto' : form_foto,
            'edit' : 'true',
            'slug' : slug,
            'infografis' : infografis,
        }
        template = 'profile/admin/infoterpadu/infografis/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        dt_get = InfoTerpadu.objects.get(slug = slug)
        path_file_lama = f"{settings.MEDIA_ROOT}/{dt_get.foto}"
        foto_old = bool(dt_get.foto)
        if bool(request.FILES):
            form = InfoTerpaduForm(data=request.POST, files=request.FILES, instance=dt_get)
            form_foto = InfoTerpaduImg(data=request.POST, files=request.FILES, instance=dt_get)
            is_valid = form.is_valid() and form_foto.is_valid()
        else:
            form = InfoTerpaduForm(data=request.POST, instance=dt_get)
            is_valid = form.is_valid()

        if is_valid:
            new_dt = form.save(commit=False)
            new_dt.title = form.cleaned_data.get('title')
            new_dt.typexs = form.cleaned_data.get('typexs')
            new_dt.description = form.cleaned_data.get('description')
            new_dt.save()
            form.save_m2m()
            if len(request.FILES) > 0:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                form_foto.save()

            messages.success(request, 'Infografis berhasil disimpan')
            return redirect('profile:admin_infoterpadu_infografis')

        messages.error(request, 'Infografis gagal disimpan.')
        return redirect('profile:admin_infoterpadu_infografis_edit', slug=slug)
    

####################################START MANAGEMEN VIEW INFORMASI PUBLIK####################################
@require_http_methods(["GET"])
def admin_index_infopublik(request):
    infoterpadu = InfoTerpadu.objects.filter(typexs='infopublik',status='Publish').order_by('-id')
    arsip = InfoTerpadu.objects.filter(typexs='infopublik',status='Draft').order_by('-id')
    for x in infoterpadu:
        ext = os.path.splitext(settings.MEDIA_ROOT+str(x.dokumen))[-1].lower()

        if ext == '.pdf':
            x.filex = 'file-pdf'
        elif ext in ['.jpg','.jpeg','.png']:
            x.filex = 'file-image'
        else:
            x.filex = 'dash'

        if (x.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))  
            if(is_foto == False):
                x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'}

    for x in arsip:
        if (x.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))  
            if(is_foto == False):
                x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 

    context = {
        'title'            : 'Informasi Terpadu',
        'data_infoterpadu' : infoterpadu,
        'arsip'            : arsip,
    }
    return render(request, 'profile/admin/infoterpadu/infopublik/index.html', context)

####################################CREATE INFOPUBLIK#################################################
@login_required
@is_verified()
def admin_index_infopublik_create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = InfoTerpaduForm()
        form_foto = InfoTerpaduImg()
        foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'}

        context = {
            'title' : 'ADD INFO PUBLIK',
            'form' : form,
            'form_foto' : form_foto,
            'infografis' : {'foto':foto},
        }

        template = 'profile/admin/infoterpadu/infopublik/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        form = InfoTerpaduForm(data=request.POST)
        form_foto = InfoTerpaduImg(data=request.POST, files=request.FILES)
        if_katalog = request.POST.get('if_katalog')

        if if_katalog == 'True':
            dokumen = request.FILES['dokumen']
        else:
            dokumen = ''

        if form.is_valid():
            new_dt = form.save(commit=False)
            new_dt.dokumen = dokumen
            new_dt.save()
            form.save_m2m()
            
            if form_foto.is_valid():
                get_dt = InfoTerpadu.objects.get(id = new_dt.id)
                dt_frm_img = InfoTerpaduImg(data=request.POST, files=request.FILES, instance=get_dt)
                dt_frm_img.save()
            
            messages.success(request, 'infopublik berhasil disimpan.')
            return redirect('profile:admin_infoterpadu_infopublik')

        messages.error(request, 'infopublik gagal disimpan.')
        return render(request, 'profile/admin/infoterpadu/infopublik/create.html', {'form': form,})

@login_required
@is_verified()
def admin_index_infopublik_edit(request, slug):
    template = ''
    context = {} 
    form = InfoTerpaduForm()
    
    if request.method == 'GET':
        infografis = InfoTerpadu.objects.get(slug = slug, typexs='infopublik')
        form = InfoTerpaduForm(instance=infografis)
        form_foto = InfoTerpaduImg()

        if (infografis.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(infografis.foto))  
            if(is_foto == False):
                infografis.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            infografis.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 

        if bool(infografis.dokumen) == False:
            infografis.dokumen = {'url':''}

        context = {
            'title' : 'EDIT Informasi Publik',
            'form' : form,
            'form_foto' : form_foto,
            'edit' : 'true',
            'slug' : slug,
            'infografis' : infografis,
        }
        template = 'profile/admin/infoterpadu/infopublik/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        dokumen = ''
        foto_old = False
        file_old = False
        dt_get = InfoTerpadu.objects.get(slug = slug)
        if_katalog = request.POST.get('if_katalog')
        if_foto = request.POST.get('if_foto')

        if bool(request.FILES):
            form = InfoTerpaduForm(data=request.POST, files=request.FILES, instance=dt_get)
            if if_foto == 'True':
                form_foto = InfoTerpaduImg(data=request.POST, files=request.FILES, instance=dt_get)
                path_file_lama = f"{settings.MEDIA_ROOT}/{dt_get.foto}"
                foto_old = bool(dt_get.foto)
                is_valid = form.is_valid() and form_foto.is_valid()

            if if_katalog == 'True':
                path_file_lama = f"{settings.MEDIA_ROOT}/{dt_get.dokumen}"
                file_old = bool(dt_get.dokumen)
                dokumen = request.FILES['dokumen']
                is_valid = form.is_valid()

        else:
            form = InfoTerpaduForm(data=request.POST, instance=dt_get)
            is_valid = form.is_valid()

        if is_valid:
            new_dt = form.save(commit=False)
            new_dt.title = form.cleaned_data.get('title')
            new_dt.description = form.cleaned_data.get('description')
            if if_katalog == 'True':
                new_dt.dokumen = dokumen
            new_dt.save()
            form.save_m2m()

            if len(request.FILES) > 0:
                if foto_old and if_foto == 'True': 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                    form_foto.save()

                if file_old and if_katalog == 'True':
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass

            messages.success(request, 'Informasi Publik berhasil disimpan')
            return redirect('profile:admin_infoterpadu_infopublik')

        messages.error(request, 'Informasi Publik gagal disimpan.')
        return redirect('profile:admin_infoterpadu_infopublik_edit', slug=slug)

####################################START MANAGEMEN VIEW REGULASI####################################
@require_http_methods(["GET"])
def admin_index_regulasi(request):
    infoterpadu = InfoTerpadu.objects.filter(typexs='regulasi',status='Publish').order_by('-id')
    arsip = InfoTerpadu.objects.filter(typexs='regulasi',status='Draft').order_by('-id')
    for x in infoterpadu:
        ext = os.path.splitext(settings.MEDIA_ROOT+str(x.dokumen))[-1].lower()

        if ext == '.pdf':
            x.filex = 'file-pdf'
        elif ext in ['.jpg','.jpeg','.png']:
            x.filex = 'file-image'
        else:
            x.filex = 'dash'

        if (x.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))  
            if(is_foto == False):
                x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'}

    for x in arsip:
        if (x.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))  
            if(is_foto == False):
                x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 

    context = {
        'title'            : 'Data Regulasi',
        'data_infoterpadu' : infoterpadu,
        'arsip'            : arsip,
    }
    return render(request, 'profile/admin/infoterpadu/regulasi/index.html', context)
    
####################################CREATE REGULASI#################################################
@login_required
@is_verified()
def admin_index_regulasi_create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = InfoTerpaduForm()
        form_foto = InfoTerpaduImg()
        foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'}

        context = {
            'title' : 'ADD REGULASI',
            'form' : form,
            'form_foto' : form_foto,
            'regulasi' : {'foto':foto},
        }

        template = 'profile/admin/infoterpadu/regulasi/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        form = InfoTerpaduForm(data=request.POST)
        form_foto = InfoTerpaduImg(data=request.POST, files=request.FILES)
        if_katalog = request.POST.get('if_katalog')
        if_foto = request.POST.get('if_foto')

        if if_katalog == 'True':
            dokumen = request.FILES['dokumen']
        else:
            dokumen = ''

        if form.is_valid():
            new_dt = form.save(commit=False)
            new_dt.dokumen = dokumen
            new_dt.save()
            form.save_m2m()
            
            if form_foto.is_valid():
                get_dt = InfoTerpadu.objects.get(id = new_dt.id)
                dt_frm_img = InfoTerpaduImg(data=request.POST, files=request.FILES, instance=get_dt)
                dt_frm_img.save()
            
            messages.success(request, 'Regulasi berhasil disimpan.')
            return redirect('profile:admin_infoterpadu_regulasi')

        messages.error(request, 'Regulasi gagal disimpan.')
        return redirect('profile:admin_infoterpadu_regulasi_cretae')

@login_required
@is_verified()
def admin_index_regulasi_edit(request, slug):
    template = ''
    context = {} 
    form = InfoTerpaduForm()
    
    if request.method == 'GET':
        regulasi = InfoTerpadu.objects.get(slug = slug, typexs='regulasi')
        form = InfoTerpaduForm(instance=regulasi)
        form_foto = InfoTerpaduImg()

        if (regulasi.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(regulasi.foto))  
            if(is_foto == False):
                regulasi.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            regulasi.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 

        if bool(regulasi.dokumen) == False:
            regulasi.dokumen = {'url':''}

        context = {
            'title' : 'EDIT Regulasi',
            'form' : form,
            'form_foto' : form_foto,
            'edit' : 'true',
            'slug' : slug,
            'regulasi' : regulasi,
        }
        template = 'profile/admin/infoterpadu/regulasi/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        dokumen = ''
        foto_old = False
        file_old = False
        dt_get = InfoTerpadu.objects.get(slug = slug)
        if_katalog = request.POST.get('if_katalog')
        if_foto = request.POST.get('if_foto')

        if bool(request.FILES):
            form = InfoTerpaduForm(data=request.POST, files=request.FILES, instance=dt_get)
            if if_foto == 'True':
                form_foto = InfoTerpaduImg(data=request.POST, files=request.FILES, instance=dt_get)
                path_file_lama = f"{settings.MEDIA_ROOT}/{dt_get.foto}"
                foto_old = bool(dt_get.foto)
                is_valid = form.is_valid() and form_foto.is_valid()

            if if_katalog == 'True':
                path_file_lama = f"{settings.MEDIA_ROOT}/{dt_get.dokumen}"
                file_old = bool(dt_get.dokumen)
                dokumen = request.FILES['dokumen']
                is_valid = form.is_valid()

        else:
            form = InfoTerpaduForm(data=request.POST, instance=dt_get)
            is_valid = form.is_valid()

        if is_valid:
            new_dt = form.save(commit=False)
            new_dt.title = form.cleaned_data.get('title')
            new_dt.description = form.cleaned_data.get('description')
            if if_katalog == 'True':
                new_dt.dokumen = dokumen
            new_dt.save()
            form.save_m2m()

            if len(request.FILES) > 0:
                if foto_old and if_foto == 'True': 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                    form_foto.save()

                if file_old and if_katalog == 'True':
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass

            messages.success(request, 'Informasi Publik berhasil disimpan')
            return redirect('profile:admin_infoterpadu_regulasi')

        messages.error(request, 'Informasi Publik gagal disimpan.')
        return redirect('profile:admin_infoterpadu_regulasi_edit', slug=slug)

####################################START MANAGEMEN VIEW KALENDER PENDIDIKAN####################################
@require_http_methods(["GET"])
def admin_index_kalender_pendidikan(request):
    infoterpadu = InfoTerpadu.objects.filter(typexs='kalender_pendidikan',status='Publish').order_by('-id')
    arsip = InfoTerpadu.objects.filter(typexs='kalender_pendidikan',status='Draft').order_by('-id')
    for x in infoterpadu:
        ext = os.path.splitext(settings.MEDIA_ROOT+str(x.dokumen))[-1].lower()

        if ext == '.pdf':
            x.filex = 'file-pdf'
        elif ext in ['.jpg','.jpeg','.png']:
            x.filex = 'file-image'
        else:
            x.filex = 'dash'

        if (x.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))  
            if(is_foto == False):
                x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'}

    for x in arsip:
        if (x.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))  
            if(is_foto == False):
                x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 

    context = {
        'title'            : 'Data Kalender Pendidikan',
        'data_infoterpadu' : infoterpadu,
        'arsip'            : arsip,
    }
    return render(request, 'profile/admin/infoterpadu/kalender_pendidikan/index.html', context)
    
####################################CREATE KALENDER PENDIDIKAN#################################################
@login_required
@is_verified()
def admin_index_kalender_pendidikan_create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = InfoTerpaduForm()
        form_foto = InfoTerpaduImg()
        foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'}
        context = {
            'title' : 'ADD KALENDER PENDIDIKAN',
            'form' : form,
            'form_foto' : form_foto,
            'kalender' : {'foto':foto},
        }

        template = 'profile/admin/infoterpadu/kalender_pendidikan/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        form = InfoTerpaduForm(data=request.POST)
        form_foto = InfoTerpaduImg(data=request.POST, files=request.FILES)
        if_katalog = request.POST.get('if_katalog')
        if_foto = request.POST.get('if_foto')

        if if_katalog == 'True':
            dokumen = request.FILES['dokumen']
        else:
            dokumen = ''

        if form.is_valid():
            new_dt = form.save(commit=False)
            new_dt.dokumen = dokumen
            new_dt.save()
            form.save_m2m()
            
            if form_foto.is_valid():
                get_dt = InfoTerpadu.objects.get(id = new_dt.id)
                dt_frm_img = InfoTerpaduImg(data=request.POST, files=request.FILES, instance=get_dt)
                dt_frm_img.save()
            
            messages.success(request, 'Kalender Pendidikan berhasil disimpan.')
            return redirect('profile:admin_infoterpadu_kalender_pendidikan')

        messages.error(request, 'Kalender Pendidikan gagal disimpan.')
        return redirect('profile:admin_kalender_pendidikan_create')

@login_required
@is_verified()
def admin_index_kalender_pendidikan_edit(request, slug):
    template = ''
    context = {} 
    form = InfoTerpaduForm()
    
    if request.method == 'GET':
        kalender = InfoTerpadu.objects.get(slug = slug, typexs='kalender_pendidikan')
        form = InfoTerpaduForm(instance=kalender)
        form_foto = InfoTerpaduImg()

        if (kalender.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(kalender.foto))  
            if(is_foto == False):
                kalender.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            kalender.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 

        if bool(kalender.dokumen) == False:
            kalender.dokumen = {'url':''}

        context = {
            'title' : 'EDIT Kalender Pendidikan',
            'form' : form,
            'form_foto' : form_foto,
            'edit' : 'true',
            'slug' : slug,
            'kalender' : kalender,
        }
        template = 'profile/admin/infoterpadu/kalender_pendidikan/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        dokumen = ''
        foto_old = False
        file_old = False
        dt_get = InfoTerpadu.objects.get(slug = slug)
        if_katalog = request.POST.get('if_katalog')
        if_foto = request.POST.get('if_foto')

        if bool(request.FILES):
            form = InfoTerpaduForm(data=request.POST, files=request.FILES, instance=dt_get)
            if if_foto == 'True':
                form_foto = InfoTerpaduImg(data=request.POST, files=request.FILES, instance=dt_get)
                path_file_lama = f"{settings.MEDIA_ROOT}/{dt_get.foto}"
                foto_old = bool(dt_get.foto)
                is_valid = form.is_valid() and form_foto.is_valid()

            if if_katalog == 'True':
                path_file_lama = f"{settings.MEDIA_ROOT}/{dt_get.dokumen}"
                file_old = bool(dt_get.dokumen)
                dokumen = request.FILES['dokumen']
                is_valid = form.is_valid()

        else:
            form = InfoTerpaduForm(data=request.POST, instance=dt_get)
            is_valid = form.is_valid()

        if is_valid:
            new_dt = form.save(commit=False)
            new_dt.title = form.cleaned_data.get('title')
            new_dt.description = form.cleaned_data.get('description')
            if if_katalog == 'True':
                new_dt.dokumen = dokumen
            new_dt.save()
            form.save_m2m()

            if len(request.FILES) > 0:
                if foto_old and if_foto == 'True': 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                    form_foto.save()

                if file_old and if_katalog == 'True':
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass

            messages.success(request, 'Informasi Publik berhasil disimpan')
            return redirect('profile:admin_infoterpadu_kalender_pendidikan')

        messages.error(request, 'Informasi Publik gagal disimpan.')
        return redirect('profile:admin_infoterpadu_kalender_pendidikan_edit', slug=slug)
    
####################################START MANAGEMEN VIEW AGENDA####################################
@require_http_methods(["GET"])
def admin_index_agenda(request):
    infoterpadu = InfoTerpadu.objects.filter(typexs='agenda',status='Publish').order_by('-id')
    for x in infoterpadu:
        ext = os.path.splitext(settings.MEDIA_ROOT+str(x.dokumen))[-1].lower()

        if ext == '.pdf':
            x.filex = 'file-pdf'
        elif ext in ['.jpg','.jpeg','.png']:
            x.filex = 'file-image'
        else:
            x.filex = 'dash'

        if (x.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))  
            if(is_foto == False):
                x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 

    arsip = InfoTerpadu.objects.filter(typexs='agenda',status='Draft').order_by('-id')
    for x in arsip:
        if (x.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))  
            if(is_foto == False):
                x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 

    context = {
        'title'            : 'Data Agenda',
        'data_infoterpadu' : infoterpadu,
        'arsip'            : arsip,
    }
    return render(request, 'profile/admin/infoterpadu/agenda/index.html', context)
    
####################################CREATE AGENDA#################################################
@login_required
@is_verified()
def admin_index_agenda_create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = InfoTerpaduForm()
        form_foto = InfoTerpaduImg()
        foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'}

        context = {
            'title' : 'ADD AGENDA',
            'form' : form,
            'form_foto' : form_foto,
            'agenda' : {'foto':foto},
        }

        template = 'profile/admin/infoterpadu/agenda/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        form = InfoTerpaduForm(data=request.POST)
        form_foto = InfoTerpaduImg(data=request.POST, files=request.FILES)
        if_katalog = request.POST.get('if_katalog')
        if_foto = request.POST.get('if_foto')

        if if_katalog == 'True':
            dokumen = request.FILES['dokumen']
        else:
            dokumen = ''

        if form.is_valid():
            new_dt = form.save(commit=False)
            new_dt.dokumen = dokumen
            new_dt.save()
            form.save_m2m()
            
            if form_foto.is_valid():
                get_dt = InfoTerpadu.objects.get(id = new_dt.id)
                dt_frm_img = InfoTerpaduImg(data=request.POST, files=request.FILES, instance=get_dt)
                dt_frm_img.save()
            
            messages.success(request, 'Regulasi berhasil disimpan.')
            return redirect('profile:admin_infoterpadu_agenda')

        messages.error(request, 'Regulasi gagal disimpan.')
        return redirect('profile:admin_infoterpadu_agenda_cretae')

@login_required
@is_verified()
def admin_index_agenda_edit(request, slug):
    template = ''
    context = {} 
    form = InfoTerpaduForm()
    
    if request.method == 'GET':
        agenda = InfoTerpadu.objects.get(slug = slug, typexs='agenda')
        form = InfoTerpaduForm(instance=agenda)
        form_foto = InfoTerpaduImg()

        if (agenda.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(agenda.foto))  
            if(is_foto == False):
                agenda.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            agenda.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 

        if bool(agenda.dokumen) == False:
            agenda.dokumen = {'url':''}

        context = {
            'title' : 'EDIT AGENDA',
            'form' : form,
            'form_foto' : form_foto,
            'edit' : 'true',
            'slug' : slug,
            'agenda' : agenda,
        }
        template = 'profile/admin/infoterpadu/agenda/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        dokumen = ''
        foto_old = False
        file_old = False
        dt_get = InfoTerpadu.objects.get(slug = slug)
        if_katalog = request.POST.get('if_katalog')
        if_foto = request.POST.get('if_foto')

        if bool(request.FILES):
            form = InfoTerpaduForm(data=request.POST, files=request.FILES, instance=dt_get)
            if if_foto == 'True':
                form_foto = InfoTerpaduImg(data=request.POST, files=request.FILES, instance=dt_get)
                path_file_lama = f"{settings.MEDIA_ROOT}/{dt_get.foto}"
                foto_old = bool(dt_get.foto)
                is_valid = form.is_valid() and form_foto.is_valid()

            if if_katalog == 'True':
                path_file_lama = f"{settings.MEDIA_ROOT}/{dt_get.dokumen}"
                file_old = bool(dt_get.dokumen)
                dokumen = request.FILES['dokumen']
                is_valid = form.is_valid()

        else:
            form = InfoTerpaduForm(data=request.POST, instance=dt_get)
            is_valid = form.is_valid()

        if is_valid:
            new_dt = form.save(commit=False)
            new_dt.title = form.cleaned_data.get('title')
            new_dt.description = form.cleaned_data.get('description')
            if if_katalog == 'True':
                new_dt.dokumen = dokumen
            new_dt.save()
            form.save_m2m()

            if len(request.FILES) > 0:
                if foto_old and if_foto == 'True': 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                    form_foto.save()

                if file_old and if_katalog == 'True':
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass

            messages.success(request, 'Informasi Publik berhasil disimpan')
            return redirect('profile:admin_infoterpadu_agenda')

        messages.error(request, 'Informasi Publik gagal disimpan.')
        return redirect('profile:admin_infoterpadu_agenda_edit', slug=slug)

####################################END MANAGEMEN VIEW INFOGRAFIS###########################################################
#SOFT DELETE & RESTORE INI DIGUNAKAN UNTUK SOFTDELETE PADA MENU INFOGRAFIS, INFOPUBLIK, AGENDA, REGULASI & KALENDER AKADEMIK
######SOFTDELETE DATA INFOTERPADU###########################################################################################
@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        status = 'Draft'
        doc = InfoTerpadu.objects.get(id=id)
        doc.status = status
        doc.save()
        message = 'success'
    except InfoTerpadu.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

######RESTORE DATA INFOTERPADU###########################################################################################
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        status = 'Publish'
        doc = InfoTerpadu.objects.get(id=id)
        doc.status = status
        doc.save()
        message = 'success'
    except InfoTerpadu.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)    
######END RESTORE DATA INFOTERPADU#######################################################################################