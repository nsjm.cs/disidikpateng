from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category,LayananDinas, katPengaduan
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category, jenis_layanan

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    kategori = katPengaduan.objects.filter(status='Publish').order_by('id')
    archives = katPengaduan.objects.filter(status='Draft').order_by('id')
    paginator = Paginator(kategori, 15)
    try:
        kategori = paginator.page(page)
    except PageNotAnInteger:
        kategori = paginator.page(1)
    except EmptyPage:
        kategori = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Kategori pengaduan - Admin',
        'data_kategori' : kategori,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/kategori_pengaduan/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''
    jns_layanan = jenis_layanan()

    if request.method == 'GET':
        form = katPengaduanForm()

        context = {
            'title' : 'ADD Katogori Pengaduan',
            'form' : form,
            'jns_layanan' : jns_layanan,
        }

        template = 'profile/admin/kategori_pengaduan/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        form = katPengaduanForm(data=request.POST)
        if form.is_valid():
            new_dt = form.save(commit=False)
            new_dt.save()
            form.save_m2m()
            
            messages.success(request, 'Kategori pengaduan berhasil disimpan.')
            return redirect('profile:admin_kat_pengaduan')

        messages.error(request, 'Kategori pengaduan gagal disimpan.')
        return render(request, 'profile/admin/kategori_pengaduan/create.html', {'form': form,})
    
@login_required
@is_verified()
def edit(request, slug):
    template = ''
    context = {} 
    form = katPengaduanForm()
    jns_layanan = jenis_layanan()
    
    if request.method == 'GET':
        kat_pengaduan = katPengaduan.objects.get(slug = slug)
        form = katPengaduanForm(instance=kat_pengaduan)


        context = {
            'title' : 'EDIT Kategori pengaduan',
            'form' : form,
            'edit' : 'true',
            'slug' : slug,
            'kat_pengaduan' : kat_pengaduan,
            'jns_layanan' : jns_layanan,
        }
        template = 'profile/admin/kategori_pengaduan/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        dt_get = katPengaduan.objects.get(slug = slug)
        form = katPengaduanForm(data=request.POST, instance=dt_get)
        is_valid = form.is_valid()
        if is_valid:
            new_dt = form.save(commit=False)
            new_dt.nama_kategori = form.cleaned_data.get('nama_kategori')
            new_dt.jenis_kategori = form.cleaned_data.get('jenis_kategori')
            new_dt.save()
            form.save_m2m()

            messages.success(request, 'Kategori pengaduan berhasil disimpan')
            return redirect('profile:admin_kat_pengaduan')

        messages.error(request, 'Kategori pengaduan gagal disimpan.')
        return render(request, 'profile/admin/kategori_pengaduan/create.html', {'form': form,})


@login_required
@is_verified()
def softDelete(request, slug):
    message = ''
    try:
        status = 'Draft'
        doc = katPengaduan.objects.get(slug=slug)
        doc.status = status
        doc.save()
        message = 'success'
    except katPengaduan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, slug):
    message = ''
    try:
        doc = katPengaduan.objects.get(slug=slug)
        doc.delete()
        message = 'success'
    except katPengaduan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, slug):
    message = ''
    try:
        status = 'Publish'
        doc = katPengaduan.objects.get(slug=slug)
        doc.status = status
        doc.save()
        message = 'success'
    except katPengaduan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)