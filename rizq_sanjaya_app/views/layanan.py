from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import LayananDinas, katPengaduan
from django.utils import timezone
from django.conf import settings
from datetime import datetime
from ..decorators import *
from django.db import connections
from django.db.models import Q, Count
from ..helpers import dictfetchall, get_list_berita, get_category, get_list_galeri
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
import os
import pprint
from django.template.loader import render_to_string
import requests
from django.db.models import OuterRef, Subquery
from taggit.models import Tag, TaggedItem



@require_http_methods(["GET"])
def index(request):
	dt_standar = LayananDinas.objects.filter(status = 'Publish', typexs='Standar Layanan').order_by('-id')
	latestnewsinfo = News.objects.filter(deleted_at__isnull=True).order_by('-created_at')[:6] 

	context = {
		'dt_standar' : dt_standar,
		'news_side' : latestnewsinfo,

	}
	return render(request, 'profile/frontend/layanan/standar_pel.html', context)
    
def info_publik(request):
	dt_standar = LayananDinas.objects.filter(status = 'Publish', typexs='Layanan Akses Informasi Publik').order_by('-id')
	latestnewsinfo = News.objects.filter(deleted_at__isnull=True).order_by('-created_at')[:6] 

	context = {
		'dt_standar' : dt_standar,
		'news_side' : latestnewsinfo,

	}
	return render(request, 'profile/frontend/layanan/info_publik.html', context)

def pengaduan(request):
	dt_standar = LayananDinas.objects.filter(status = 'Publish', typexs='Pengaduan Publik').order_by('-id')
	latestnewsinfo = News.objects.filter(deleted_at__isnull=True).order_by('-created_at')[:6] 
	kategori = katPengaduan.objects.filter(deleted_at__isnull=True).order_by('-id')

	context = {
		'dt_standar' : dt_standar,
		'news_side' : latestnewsinfo,
		'kategori' : kategori,

	}
	return render(request, 'profile/frontend/layanan/pengaduan.html', context)

def permohonan(request):
	dt_standar = LayananDinas.objects.filter(status = 'Publish', typexs='Layanan Permohonan Informasi').order_by('-id')
	latestnewsinfo = News.objects.filter(deleted_at__isnull=True).order_by('-created_at')[:6] 

	context = {
		'dt_standar' : dt_standar,
		'news_side' : latestnewsinfo,

	}
	return render(request, 'profile/frontend/layanan/permohonan.html', context)
    