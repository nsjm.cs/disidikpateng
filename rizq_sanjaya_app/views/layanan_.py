from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category,LayananDinas
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category, jenis_layanan

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    layanan = LayananDinas.objects.filter(status='Publish').order_by('typexs','title')
    archives = LayananDinas.objects.filter(status='Draft').order_by('typexs','title')
    paginator = Paginator(layanan, 15)
    try:
        layanan = paginator.page(page)
    except PageNotAnInteger:
        layanan = paginator.page(1)
    except EmptyPage:
        layanan = paginator.page(paginator.num_pages)

    for x in layanan:
        if (x.foto):
            x.is_foto = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))  
            if(x.is_foto == False):
                x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 

    for x in archives:
        if (x.foto):
            x.is_foto = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))  
            if(x.is_foto == False):
                x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'}  

    context = {
        'title' : 'Layanan Dinas - Admin',
        'data_layanan' : layanan,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/layanan_/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''
    jns_layanan = jenis_layanan()

    if request.method == 'GET':
        form = LayananDinasForm()
        form_foto = LayananDinasImg()
        foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'}

        context = {
            'title' : 'ADD Layanan Dinas',
            'form' : form,
            'form_foto' : form_foto,
            'jns_layanan' : jns_layanan,
            'layanandinas' : {'foto':foto},
        }

        template = 'profile/admin/layanan_/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        form = LayananDinasForm(data=request.POST)
        form_foto = LayananDinasImg(data=request.POST, files=request.FILES)
        if form.is_valid():
            new_dt = form.save(commit=False)
            new_dt.save()
            form.save_m2m()
            
            if form_foto.is_valid():
                get_dt = LayananDinas.objects.get(id = new_dt.id)
                dt_frm_img = LayananDinasImg(data=request.POST, files=request.FILES, instance=get_dt)
                dt_frm_img.save()
            
            messages.success(request, 'Layanan dinas berhasil disimpan.')
            return redirect('profile:admin_layanan')

        messages.error(request, 'Layanan dinas gagal disimpan.')
        return render(request, 'profile/admin/layanan_/create.html', {'form': form,})
    
@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = LayananDinasForm()
    jns_layanan = jenis_layanan()
    
    if request.method == 'GET':
        layanandinas = LayananDinas.objects.get(id = id)
        form = LayananDinasForm(instance=layanandinas)
        form_foto = LayananDinasImg()

        if (layanandinas.foto):
            is_foto = os.path.isfile(settings.MEDIA_ROOT+str(layanandinas.foto))  
            if(is_foto == False):
                layanandinas.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
        else:
            layanandinas.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 

        context = {
            'title' : 'EDIT Layanan Dinas',
            'form' : form,
            'form_foto' : form_foto,
            'edit' : 'true',
            'id' : id,
            'layanandinas' : layanandinas,
            'jns_layanan' : jns_layanan,
        }
        template = 'profile/admin/layanan_/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        dt_get = LayananDinas.objects.get(id = id)
        path_file_lama = f"{settings.MEDIA_ROOT}/{dt_get.foto}"
        foto_old = bool(dt_get.foto)
        if bool(request.FILES):
            form = LayananDinasForm(data=request.POST, files=request.FILES, instance=dt_get)
            form_foto = LayananDinasImg(data=request.POST, files=request.FILES, instance=dt_get)
            is_valid = form.is_valid() and form_foto.is_valid()
        else:
            form = LayananDinasForm(data=request.POST, instance=dt_get)
            is_valid = form.is_valid()

        if is_valid:
            new_dt = form.save(commit=False)
            new_dt.title = form.cleaned_data.get('title')
            new_dt.typexs = form.cleaned_data.get('typexs')
            new_dt.description = form.cleaned_data.get('description')
            new_dt.save()
            form.save_m2m()
            if len(request.FILES) > 0:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                form_foto.save()

            messages.success(request, 'Layanan Dinas berhasil disimpan')
            return redirect('profile:admin_layanan')

        messages.error(request, 'Layanan Dinas gagal disimpan.')
        return render(request, 'profile/admin/layanan_/create.html', {'form': form,})


@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        status = 'Draft'
        doc = LayananDinas.objects.get(id=id)
        doc.status = status
        doc.save()
        message = 'success'
    except LayananDinas.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, id):
    message = ''
    try:
        doc = LayananDinas.objects.get(id=id)
        try:
            doc.image.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except LayananDinas.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        status = 'Publish'
        doc = LayananDinas.objects.get(id=id)
        doc.status = status
        doc.save()
        message = 'success'
    except LayananDinas.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)