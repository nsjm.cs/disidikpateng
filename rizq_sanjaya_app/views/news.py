from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category
from django.template.loader import render_to_string

# JUMLAH DATA PER PAGE ===> PAGINATIONS
jml_data = 3

@require_http_methods(["GET"])
def index(request):
	# page = request.GET.get('page', 1)
	news = News.objects.filter(deleted_at=None).order_by('-created_at')
	nws_jml = int(len(news))
	if nws_jml >= 3:
		thumbnail = {}
		for x in news:
			x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))
			x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/thumb/')}
		news_list = news


	else:
		news_list = get_list_berita(3)

	context = {
		'title' : 'Berita',
		'news' : news_list,
		'dt_tags': querytags('news'),
	}
	return render(request, 'profile/news/index.html', context)

@require_http_methods(["GET"])
def category(request, jenis, slug):
	news_count = News.objects.filter(deleted_at=None, category__slug = slug).count()
	news = get_list_berita(news_count)
	kategori = Category.objects.filter(typexs='news').order_by('nama')
	favorits = News.objects.filter(deleted_at=None).order_by('-seen')[:5]

	announcement = Announcement.objects.filter(deleted_at=None).order_by('-created_at')[:5]
	agenda = Agenda.objects.filter(deleted_at=None).order_by('-created_at')[:5]
	date_format = '%Y-%m-%d'
	about = AboutMe.objects.get(jenis='header')
	recent = News.objects.all().order_by('-created_at')[:3]

	for x in agenda:
		a = datetime.strptime(str(x.start_date), date_format)
		b = datetime.strptime(str(x.finish_date), date_format)
		delta = b - a
		x.days = delta.days + 1
	try:
		dtx = News.objects.get(slug=slug)
		dtx.images = os.path.isfile(settings.MEDIA_ROOT+str(dtx.thumbnail))
		dtx.seen += 1
		dtx.save()
	except News.DoesNotExist:
		dtx = None

	paginator = Paginator(news, 3)
	page_number = request.GET.get('page')
	page_obj = paginator.get_page(page_number) 
	
	context = {
		'title' : 'News',
		'title2' : 'Berita-Gorosario',
		'data' : news,
		'cats' : kategori,
		'favs' : favorits,
		'kategori' : slug.replace('-', ' ').title(),
		'pengumuman':announcement,
		'agendas' : agenda,
		'dt_tags': querytags('news'),
		'about' : about,
		'recent' : recent,
		'news' : dtx,
		'type' : 'category',
		'page_obj': page_obj,
	}
	return render(request, 'profile/news/category_blog.html', context)

@require_http_methods(["GET"])
def detail(request, slug):
	try:
		dtx = News.objects.get(slug=slug)
		dtx.images = os.path.isfile(settings.MEDIA_ROOT+str(dtx.thumbnail))
		dtx.seen += 1
		dtx.save()
	except News.DoesNotExist:
		dtx = None

	favorits = News.objects.filter(deleted_at=None).order_by('-seen')[:3]
	for x in favorits:
		x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/thumb/')}

	kategori = Category.objects.filter(typexs='news').order_by('nama')
	otherpage = News.objects.filter(deleted_at=None).order_by('seen')[:2]
	recent = News.objects.all().order_by('-created_at')[:3]
	about = AboutMe.objects.get(jenis='header')

	context = {
		'title' : dtx.title if dtx != None else 'TIDAK DITEMUKAN',
		'news' : dtx,
		'cats' : kategori,
		'favs' : favorits,
		'othr' : otherpage,
		'dt_tags': querytags('news'),
		'recent' : recent,
		'about' : about,
		'type'  : 'detail',
	}
	return render(request, 'profile/news/detail_blog.html', context)

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
	# search_query = request.GET.get('search')

	# news = News.objects.all().order_by('-created_at')
	# archives = News.objects.filter(deleted_at__isnull=False).order_by('-created_at')

	# if search_query:
	#     news = news.filter(title__icontains=search_query)

	# paginator = Paginator(news, 8)
	# page = request.GET.get('page', 1)

	# try:
	#     news = paginator.page(page)
	# except PageNotAnInteger:
	#     news = paginator.page(1)
	# except EmptyPage:
	#     news = paginator.page(paginator.num_pages)

	# context = {
	#     'title': 'Berita - Admin',
	#     'newss': news,
	#     'archives': archives,
	# }

	# return render(request, 'profile/admin/news/index.html', context)
	search_query = request.GET.get('search')
	news = News.objects.filter(deleted_at__isnull=True).order_by('-created_at')
	archives = News.objects.filter(deleted_at__isnull=False).order_by('-created_at')

	if search_query:
		news = news.filter(title__icontains=search_query)

	paginator = Paginator(news, 8)
	page = request.GET.get('page', 1)

	try:
		news = paginator.page(page)
	except PageNotAnInteger:
		news = paginator.page(1)
	except EmptyPage:
		news = paginator.page(paginator.num_pages)

	context = {
		'title': 'Berita - Admin',
		'newss': news,
		'archives': archives,
	}

	return render(request, 'profile/admin/news/index.html', context)

@login_required
@is_verified()
def create(request):
	template = ''
	context = {} 
	form = ''
	form_foto = ''
	category = get_category(['news', 'inovasi'])

	if request.method == 'GET':
		form = NewsForm()
		form_foto = NewsThumbnailForm()
		context = {
			'title' : 'ADD NEWS',
			'form' : form,
			'form_foto' : form_foto,
			'kategori' : category,
		}

		template = 'profile/admin/news/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		form = NewsForm(data=request.POST)
		form_foto = NewsThumbnailForm(data=request.POST, files=request.FILES)

		if form.is_valid() and form_foto.is_valid():
			new_news = form.save(commit=False)
			new_news.created_by_id = request.user.id
			new_news.last_updated_by = request.user.id
			new_news.save()
			form.save_m2m()
			
			news = News.objects.get(id = new_news.id)
			thumb_news = NewsThumbnailForm(data=request.POST, files=request.FILES, instance=news)
			thumb_news.save()
			
			messages.success(request, 'Berita berhasil disimpan.')
			return redirect('profile:admin_news')

		messages.error(request, 'Berita gagal disimpan.')
		return redirect('profile:admin_news_create')

@login_required
@is_verified()
def edit(request, slug):
	template = ''
	context = {} 
	form = ''
	category = get_category(['news', 'inovasi'])
	
	if request.method == 'GET':
		try:
			news = News.objects.get(slug=slug)
			form = NewsForm(instance=news)
			form_foto = NewsThumbnailForm()
			tags = ''
			slug = news.slug
			for tag in  news.tags.all():
				tags += f"{tag}, "
		except:
			print('Error get edit', e)
			news = None
			form = NewsForm()
			form_foto = NewsThumbnailForm()
			tags = ''
			slug = None
		
		context = {
			'title' : 'EDIT BERITA',
			'form' : form,
			'form_foto' : form_foto,
			'edit' : 'true',
			'slug' : slug,
			'news' : news,
			'tagsnya' : tags,
			'kategori' : category,
		}
		template = 'profile/admin/news/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		news = News.objects.get(slug=slug)
		path_file_lama = f"{settings.MEDIA_ROOT}/{news.thumbnail}"
		foto_old = bool(news.thumbnail)
		if bool(request.FILES):
			form = NewsForm(data=request.POST, files=request.FILES, instance=news)
			form_foto = NewsThumbnailForm(data=request.POST, files=request.FILES, instance=news)
			is_valid = form.is_valid() and form_foto.is_valid()
		else:
			form = NewsForm(data=request.POST, instance=news)
			is_valid = form.is_valid()

		if is_valid:
			new_news = form.save(commit=False)
			new_news.title = form.cleaned_data.get('title')
			new_news.content = form.cleaned_data.get('content')
			new_news.category = form.cleaned_data.get('category')
			new_news.last_updated_by = request.user.id
			new_news.save()
			form.save_m2m()

			if len(request.FILES) > 0:
				if foto_old : 
					try:
						os.remove(path_file_lama)
					except:
						pass
				form_foto.save()

			messages.success(request, 'Berita berhasil disimpan.')
			return redirect('profile:admin_news')

		messages.error(request, 'Berita gagal disimpan.')
		return render(request, 'profile/admin/news/create.html', {'form': form,'kategori' : category,})
	
@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_detail(request, slug):
	template = 'profile/admin/news/detail.html'
	try:
		news = News.objects.get(slug=slug)
	except News.DoesNotExist:
		news = None
	context = {
		'title' : news.title if news != None else 'TIDAK DITEMUKAN',
		'news' : news,
	}
	return render(request, template, context)

@login_required
@is_verified()
def softDelete(request, slug):
	message = ''
	try:
		sekarang = timezone.now()
		doc = News.objects.get(slug=slug)
		doc.deleted_at = sekarang
		doc.save()
		message = 'success'
	except News.DoesNotExist: 
		message = 'error'

	context = {
			'message' : message,
		}

	return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, slug):
	message = ''
	try:
		doc = News.objects.get(slug=slug)
		try:
			doc.thumbnail.delete()
		except:
			pass
		doc.delete()
		message = 'success'
	except News.DoesNotExist:
		message = 'error'

	context = {
			'message' : message,
		}

	return HttpResponse(context)
	
@login_required
@is_verified()
def restore(request, slug):
	message = ''
	try:
		doc = News.objects.get(slug=slug)
		doc.deleted_at = None
		doc.save()
		message = 'success'
	except News.DoesNotExist:
		message = 'error'

	context = {
			'message' : message,
		}

	return HttpResponse(context)