from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category,LayananDinas, katPengaduan, PelayananPublik
from django.utils import timezone
import os
from django.db.models import Q, F
from django.db import transaction
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category, jenis_layanan
from django.http import JsonResponse
from support import support_function as sup
import requests
from django.utils.html import strip_tags
from ..helpers import contains_html




@login_required
@is_verified()
@role_beasiswa()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    pengaduan = PelayananPublik.objects.all().order_by('id')
    kategori = katPengaduan.objects.all().order_by('-id')

    get_kategori = request.GET.get('kategori', '')
    get_search = request.GET.get('search', '')

    # Menggunakan Q untuk menangani filter yang kompleks
    query = Q(deleted_at=None)

    if get_kategori and get_search:
        query &= (Q(kategori__nama_kategori__icontains=get_kategori) & 
                (Q(nama__icontains=get_search) | Q(alamat__icontains=get_search)))
    elif get_kategori:
        query &= Q(kategori__nama_kategori__icontains=get_kategori)
    elif get_search:
        query &= (Q(nama__icontains=get_search) | Q(alamat__icontains=get_search))

    # Eksekusi query
    pengaduan = PelayananPublik.objects.filter(query).order_by('-id')



    paginator = Paginator(pengaduan, 15)
    try:
        pengaduan = paginator.page(page)
    except PageNotAnInteger:
        pengaduan = paginator.page(1)
    except EmptyPage:
        pengaduan = paginator.page(paginator.num_pages)

    offset = (pengaduan.number - 1) * paginator.per_page

    context = {
        'title' : 'Pelayanan pengaduan - Admin',
        'data_pengaduan' : pengaduan,
        'kategori' : kategori,
        'offset' : offset,
    }
    
    return render(request, 'profile/admin/pelayanan_pengaduan/index.html', context)

@login_required
@is_verified()
@role_beasiswa()
def inputCatatan(request, id):
    catatan = request.POST.get('catatan')
    dt_pengaduan = get_object_or_404(PelayananPublik, id=id)
    try:
        with transaction.atomic():
            dt_pengaduan.catatan = catatan
            dt_pengaduan.save()

        sup.send_email_pengaduan([dt_pengaduan.email], catatan)
        messages.success(request, 'Catatan pengaduan berhasil disimpan.')
        return redirect('profile:admin_pelayanan_pengaduan')
                                         
    except ValueError as e:
        messages.success(request, 'Catatan pengaduan berhasil disimpan.')
        return redirect('profile:admin_pelayanan_pengaduan')
        print(f'Error processing input data: {e}')

    return redirect('profile:admin_pelayanan_pengaduan')

    
def Inputpengaduan(request):
    if request.method == 'POST':
        turnstile_response = request.POST.get("cf-turnstile-response")
        
        data = {
            "secret": settings.CLOUDFLARE_TURNSTILE_SECRET_KEY,
            "response": turnstile_response,
            "remoteip": request.META.get("REMOTE_ADDR"),
        }
        verify_response = requests.post(
            "https://challenges.cloudflare.com/turnstile/v0/siteverify", data=data
        )
        
        result = verify_response.json()

        if result.get("success"):
            no_identitas = request.POST.get('no_identitas', '')
            nama = request.POST.get('nama', '')
            email = request.POST.get('email', '')
            no_hp = request.POST.get('no_hp', '')
            status = request.POST.get('status', '')
            nama_instansi = request.POST.get('nama_instansi', '')
            alamat = request.POST.get('alamat', '')
            kategori = request.POST.get('kategori', '')
            deskripsi = request.POST.get('deskripsi', '')


            for field_name, value in {
                'no_identitas': no_identitas,
                'nama': nama,
                'email': email,
                'no_hp': no_hp,
                'status': status,
                'nama_instansi': nama_instansi,
                'alamat': alamat,
                'kategori': kategori,
                'deskripsi': deskripsi,
            }.items():
                if contains_html(value):
                    return JsonResponse({'message': 'Mohon maaf! anda tidak bisa menambahkan script ke dalam inputkan'}, status=400)

            try:
                with transaction.atomic():
                    insert = PelayananPublik(
                        no_identitas=no_identitas,
                        nama=nama,
                        email=email,
                        no_hp=no_hp,
                        status=status,
                        nama_instansi=nama_instansi,
                        alamat=alamat,
                        kategori_id=kategori,
                        deskripsi=deskripsi,
                    )
                    insert.save()
                
                return JsonResponse({'message': 'Pengaduan berhasil disimpan.'})
                                                
            except ValueError as e:
                return JsonResponse({'message': 'silahkan isi terlebih dahulu reCAPTCHA.'})
                print(f'Error processing input data: {e}')
        else:
            return JsonResponse({'error': 'Invalid reCAPTCHA. Please try again.'}, status=400)




@login_required
@is_verified()
@role_beasiswa()
@require_http_methods(["GET"])
def Approve(request, id):
    message = ''
    try:
        doc = PelayananPublik.objects.get(id=id)
        doc.status_pengaduan = 1
        doc.save()
        message = 'success'
    except Kata.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }
    return HttpResponse(context)


@login_required
@is_verified()
@role_beasiswa()
@require_http_methods(["GET"])
def NotApprove(request, id):
    message = ''
    try:
        doc = PelayananPublik.objects.get(id=id)
        doc.status_pengaduan = 0
        doc.save()
        message = 'success'
    except Kata.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }
    return HttpResponse(context)