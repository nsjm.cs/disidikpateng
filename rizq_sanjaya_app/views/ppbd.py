from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags
from ..context_processors_ppdb import get_list_ppdb_berita
from django.template.loader import render_to_string

@require_http_methods(["GET"])
def index(request):
	dt_beranda = ppdb_konten.objects.get(typexs='beranda')
	dt_tahapan = ppdb_konten.objects.filter(typexs='tahapan')
	dt_faq = ppdb_konten.objects.filter(typexs='faq', deleted_at=None).order_by('-created_at')
	news_list = get_list_ppdb_berita(2)
	
	# # page = request.GET.get('page', 1)
	# news = News.objects.filter(deleted_at=None).order_by('-created_at')
	# nws_jml = int(len(news))
	# if nws_jml >= jml_data:
	# 	thumbnail = {}
	# 	for x in news:
	# 		x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))
	# 		x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/')}
	# 	news_list = news

	# else:
	# 	news_list = get_list_berita(jml_data)

	# dt_ppid = modul_PPID.objects.get(slug=slug)
	# ppid_lain = modul_PPID.objects.exclude(slug=slug)

	# for x in dt_beranda:

	if (dt_beranda.foto):
		is_foto = os.path.isfile(settings.MEDIA_ROOT+str(dt_beranda.foto))  
		if(is_foto == False):
			dt_beranda.foto = {'url':settings.STATIC_URL+'ppbd/img/logo/no-image-1.png'} 
	else:
		dt_beranda.foto = {'url':settings.STATIC_URL+'ppbd/img/logo/no-image-1.png'}

	dtx_thp = {}
	for x in dt_tahapan:
		dtx = {'title':x.title, 'description':x.description, 'if_link':x.if_link}
		dtx_thp[x.subtitle] = dtx

	context = {
		'title' : 'Beranda',
		'dt_beranda' : dt_beranda,
		'dtx_thp' : dtx_thp,
		'dt_faq' : dt_faq,
		'news_list' : news_list,
	}
	return render(request, 'profile/ppbd/layouts/index.html', context)


@require_http_methods(["GET"])
def index_berita(request):
	news = ppdb_News.objects.filter(deleted_at=None).order_by('-created_at')
	paginator = Paginator(news, 3)
	page = request.GET.get('page', 1)

	try:
		news = paginator.page(page)
	except PageNotAnInteger:
		news = paginator.page(1)
	except EmptyPage:
		news = paginator.page(paginator.num_pages)

	for x in news:
		x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))

	recentpost = ppdb_News.objects.filter(deleted_at=None).order_by('-created_at')[:5]
	for x in recentpost:
		x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))

	favorits = ppdb_News.objects.filter(deleted_at=None).order_by('-seen')[:5]
	for x in favorits:
		x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))

	context = {
		'title' : 'Berita',
		'dt_berita' : news,
		'favorits' : favorits,
		'recentpost' : recentpost,
		'dt_tags': querytags('ppdb_news'),
	}
	return render(request, 'profile/ppbd/layouts/frontend/berita/index.html', context)


@require_http_methods(["GET"])
def detail_berita(request, slug):
	try:
		dtx = ppdb_News.objects.get(slug=slug)
		dtx.images = os.path.isfile(settings.MEDIA_ROOT+str(dtx.thumbnail))
		dtx.seen += 1
		dtx.save()
	except:
		dtx = None

	recentpost = ppdb_News.objects.filter(deleted_at=None).order_by('-created_at')[:5]
	for x in recentpost:
		x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))

	favorits = ppdb_News.objects.filter(deleted_at=None).order_by('-seen')[:5]
	for x in favorits:
		x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))

	tags = ''
	for tag in dtx.tags.all():
		tags += f"{tag}, "

	context = {
		'title' : dtx.title if dtx != None else 'TIDAK DITEMUKAN',
		'news' : dtx,
		'favorits' : favorits,
		'recentpost' : recentpost,
		'tags' : tags,
		'dt_tags': querytags('ppdb_news'),
	}
	return render(request, 'profile/ppbd/layouts/frontend/berita/detail.html', context)


@require_http_methods(["GET"])
def tags_index(request, slug):
	news = ppdb_News.objects.filter(tags__slug__in=[slug], deleted_at=None).order_by('-created_at')
	paginator = Paginator(news, 3)
	page = request.GET.get('page', 1)

	try:
		news = paginator.page(page)
	except PageNotAnInteger:
		news = paginator.page(1)
	except EmptyPage:
		news = paginator.page(paginator.num_pages)

	for x in news:
		x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))


	recentpost = ppdb_News.objects.filter(deleted_at=None).order_by('-created_at')[:5]
	for x in recentpost:
		x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))

	favorits = ppdb_News.objects.filter(deleted_at=None).order_by('-seen')[:5]
	for x in favorits:
		x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))

	context = {
		'title' : f'Tags #{slug.title()}',
		'dt_berita' : news,
		'favorits' : favorits,
		'recentpost' : recentpost,
		'dt_tags': querytags('ppdb_news'),
	}
	return render(request, 'profile/ppbd/layouts/frontend/berita/tags.html', context)