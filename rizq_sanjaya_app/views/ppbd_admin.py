from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags
from django.template.loader import render_to_string

@login_required
@is_verified()
def adm_index(request):
	# # page = request.GET.get('page', 1)
	# news = News.objects.filter(deleted_at=None).order_by('-created_at')
	# nws_jml = int(len(news))
	# if nws_jml >= jml_data:
	# 	thumbnail = {}
	# 	for x in news:
	# 		x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))
	# 		x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/')}
	# 	news_list = news

	# else:
	# 	news_list = get_list_berita(jml_data)

	# dt_ppid = modul_PPID.objects.get(slug=slug)
	# ppid_lain = modul_PPID.objects.exclude(slug=slug)

	context = {
		'title' : 'Dashboard',
		# 'dt_ppid' : dt_ppid,
		# 'ppid_lain' : ppid_lain,
		# 'news_list' : news_list,
		# 'dt_tags': querytags('modul_ppid'),
	}
	return render(request, 'profile/ppbd/layouts/admin/index.html', context)

@login_required
@is_verified()
def adm_beranda(request):

	if request.method == 'GET':
		form = ppdb_BerandaForm()
		form_foto = ppdb_KontenImg()
		foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'}

		try:
			dt_beranda = ppdb_konten.objects.get(typexs='beranda')
			if (dt_beranda.foto):
				is_foto = os.path.isfile(settings.MEDIA_ROOT+str(dt_beranda.foto))  
				if(is_foto == False):
					dt_beranda.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
			else:
				dt_beranda.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'}

			form = ppdb_BerandaForm(instance=dt_beranda)
			is_edit = 'true'
		except:
			dt_beranda = {'foto':foto}
			is_edit = 'false'

		context = {
			'title' : 'Setting Beranda',
			'form' : form,
			'form_foto' : form_foto,
			'beranda' : dt_beranda,
			'is_edit' : is_edit,
		}

		return render(request, 'profile/ppbd/layouts/admin/beranda/index.html', context)


	if request.method == 'POST':
		dtx = request.POST

		if dtx.get('if_edit') == 'false':
			form = ppdb_BerandaForm(data=request.POST)
			form_foto = ppdb_KontenImg(data=request.POST, files=request.FILES)
			if_katalog = dtx.get('if_katalog')

			if if_katalog == 'True':
				dokumen = request.FILES['dokumen']
			else:
				dokumen = ''

			if form.is_valid():
				new_dt = form.save(commit=False)
				new_dt.dokumen = dokumen
				new_dt.save()
				form.save_m2m()
				
				if form_foto.is_valid():
					get_dt = ppdb_konten.objects.get(id = new_dt.id)
					dt_frm_img = ppdb_KontenImg(data=request.POST, files=request.FILES, instance=get_dt)
					dt_frm_img.save()
				
				messages.success(request, 'Setting Beranda berhasil disimpan.')
				return redirect('profile:adm_ppdb_beranda')

		# ### JIKA DATA SUDAH ISI ================================================================================================
		else:
			dokumen = ''
			foto_old = False
			file_old = False
			dt_get = ppdb_konten.objects.get(typexs='beranda')
			if_katalog = request.POST.get('if_katalog')
			if_foto = request.POST.get('if_foto')

			if bool(request.FILES):
				form = ppdb_BerandaForm(data=request.POST, files=request.FILES, instance=dt_get)
				if if_foto == 'True':
					form_foto = ppdb_KontenImg(data=request.POST, files=request.FILES, instance=dt_get)
					path_file_lama = f"{settings.MEDIA_ROOT}/{dt_get.foto}"
					foto_old = bool(dt_get.foto)
					is_valid = form.is_valid() and form_foto.is_valid()

				if if_katalog == 'True':
					path_file_lama = f"{settings.MEDIA_ROOT}/{dt_get.dokumen}"
					file_old = bool(dt_get.dokumen)
					dokumen = request.FILES['dokumen']
					is_valid = form.is_valid()

			else:
				form = ppdb_BerandaForm(data=request.POST, instance=dt_get)
				is_valid = form.is_valid()

			if is_valid:
				new_dt = form.save(commit=False)
				new_dt.title = form.cleaned_data.get('title')
				new_dt.subtitle = form.cleaned_data.get('subtitle')
				new_dt.description = form.cleaned_data.get('description')
				if if_katalog == 'True':
					new_dt.dokumen = dokumen
				new_dt.save()
				form.save_m2m()

				if len(request.FILES) > 0:
					if foto_old and if_foto == 'True': 
						try:
							os.remove(path_file_lama)
						except:
							pass
						form_foto.save()

					if file_old and if_katalog == 'True':
						try:
							os.remove(path_file_lama)
						except:
							pass

				messages.success(request, 'Setting Beranda berhasil disimpan')
				return redirect('profile:adm_ppdb_beranda')

		messages.error(request, 'Setting Beranda gagal disimpan.')
		return redirect('profile:adm_ppdb_beranda')


@login_required
@is_verified()
@require_http_methods(["GET"])
def adm_tahapan(request):
	tahapan = ppdb_konten.objects.filter(deleted_at__isnull=True, typexs='tahapan').order_by('subtitle')
	archives = ppdb_konten.objects.filter(deleted_at__isnull=False, typexs='tahapan').order_by('subtitle')
	context = {
		'title' : 'Setting Tahapan',
		'dt_tahapan': tahapan,
		'archives': archives,
	}
	return render(request, 'profile/ppbd/layouts/admin/tahapan/index.html', context) 

@login_required
@is_verified()
def adm_tahapan_edit(request, slug):
	dtxs = ppdb_konten.objects.get(slug=slug, typexs='tahapan')
	if request.method == 'GET':
		form = ppdb_TahapanForm(instance=dtxs)
		context = {
			'title' : 'EDIT Tahapan',
			'form' : form,
			'edit' : True,
			'slug' : slug,
		}
		return render(request, 'profile/ppbd/layouts/admin/tahapan/create.html', context)

	if request.method == 'POST':
		form = ppdb_TahapanForm(data=request.POST, instance=dtxs)
		if form.is_valid():
			new_dt = form.save(commit=False)
			new_dt.title = form.cleaned_data.get('title')
			new_dt.description = form.cleaned_data.get('description')
			new_dt.if_link = form.cleaned_data.get('if_link')
			new_dt.save()
			form.save_m2m()
			
			messages.success(request, 'Tahapan PPDB berhasil disimpan.')
			return redirect('profile:adm_ppdb_tahapan')

		messages.error(request, 'Tahapan PPDB gagal disimpan.')
		return redirect('profile:adm_ppdb_tahapan_create', slug=slug)


@login_required
@is_verified()
@require_http_methods(["GET"])
def adm_jalur(request):
	jalur = ppdb_konten.objects.filter(deleted_at__isnull=True, typexs='jalur').order_by('subtitle')
	archives = ppdb_konten.objects.filter(deleted_at__isnull=False, typexs='jalur').order_by('subtitle')
	context = {
		'title' : 'Setting Jalur',
		'dt_jalur': jalur,
		'archives': archives,
	}
	return render(request, 'profile/ppbd/layouts/admin/jalur/index.html', context) 


@login_required
@is_verified()
@require_http_methods(["GET"])
def adm_pengumuman(request):
	pengumuman = ppdb_konten.objects.filter(deleted_at__isnull=True, typexs='pengumuman').order_by('subtitle')
	archives = ppdb_konten.objects.filter(deleted_at__isnull=False, typexs='pengumuman').order_by('subtitle')
	context = {
		'title' : 'Setting Pengumuman',
		'dt_pengumuman': pengumuman,
		'archives': archives,
	}
	return render(request, 'profile/ppbd/layouts/admin/pengumuman/index.html', context) 


@login_required
@is_verified()
def adm_infografis(request):
	if request.method == 'GET':
		cinta = {}
		dtx = ppdb_AppSetting.objects.all()
		for x in dtx:
			cinta[x.nama] = x.keterangan

		context = {
			'title' : 'Setting Infografis',
			'form' : cinta,
		}
		return render(request, 'profile/ppbd/layouts/admin/infografis/index.html', context) 

	if request.method == 'POST':
		is_simpan = False
		dtx = request.POST
		for x in dtx:
			if x != 'csrfmiddlewaretoken':
				if dtx.get(x) != "":
					try:
						app_set = ppdb_AppSetting.objects.get(nama=x)
						app_set.keterangan = dtx.get(x)
						app_set.save()
					except:
						app_set = ppdb_AppSetting(nama=x, keterangan=dtx.get(x))
						app_set.save()

					is_simpan = True

		if is_simpan:
			messages.success(request, 'Setting Beranda berhasil disimpan')
			return redirect('profile:adm_ppdb_infografis')

		messages.error(request, 'Setting Beranda gagal disimpan.')
		return redirect('profile:adm_ppdb_infografis')


@login_required
@is_verified()
def adm_faq(request):
	if request.method == 'GET':
		cinta = {}
		dtx = ppdb_AppSetting.objects.all()
		for x in dtx:
			cinta[x.nama] = x.keterangan

		data_list = ppdb_konten.objects.filter(deleted_at__isnull=True, typexs='faq').order_by('-created_at')
		archives = ppdb_konten.objects.filter(deleted_at__isnull=False, typexs='faq').order_by('-created_at')

		context = {
			'title' : 'Setting FAQ',
			'form' : cinta,
			'data_list': data_list,
			'archives': archives,
		}
		return render(request, 'profile/ppbd/layouts/admin/faq/index.html', context) 

	if request.method == 'POST':
		is_simpan = False
		dtx = request.POST
		for x in dtx:
			if x != 'csrfmiddlewaretoken':
				if dtx.get(x) != "":
					try:
						app_set = ppdb_AppSetting.objects.get(nama=x)
						app_set.keterangan = dtx.get(x)
						app_set.save()
					except:
						app_set = ppdb_AppSetting(nama=x, keterangan=dtx.get(x))
						app_set.save()

					is_simpan = True

		if is_simpan:
			messages.success(request, 'Setting FAQ berhasil disimpan')
			return redirect('profile:adm_ppdb_faq')

		messages.error(request, 'Setting FAQ gagal disimpan.')
		return redirect('profile:adm_ppdb_faq') 


@login_required
@is_verified()
def adm_faq_create(request):
	if request.method == 'GET':
		form = ppdb_BerandaForm()
		context = {
			'title' : 'ADD FAQ',
			'form' : form,
			'edit' : False,
		}
		return render(request, 'profile/ppbd/layouts/admin/faq/create.html', context)

	if request.method == 'POST':
		form = ppdb_BerandaForm(data=request.POST)
		if form.is_valid():
			new_dt = form.save(commit=False)
			new_dt.save()
			form.save_m2m()
			
			messages.success(request, 'Data FAQ berhasil disimpan.')
			return redirect('profile:adm_ppdb_faq')

		messages.error(request, 'Data FAQ gagal disimpan.')
		return redirect('profile:adm_ppdb_faq_create')


@login_required
@is_verified()
def adm_faq_edit(request, slug):
	dtxs = ppdb_konten.objects.get(slug=slug, typexs='faq')
	if request.method == 'GET':
		form = ppdb_BerandaForm(instance=dtxs)
		context = {
			'title' : 'EDIT FAQ',
			'form' : form,
			'edit' : True,
			'slug' : slug,
		}
		return render(request, 'profile/ppbd/layouts/admin/faq/create.html', context)

	if request.method == 'POST':
		form = ppdb_BerandaForm(data=request.POST, instance=dtxs)
		if form.is_valid():
			new_dt = form.save(commit=False)
			new_dt.title = form.cleaned_data.get('title')
			new_dt.description = form.cleaned_data.get('description')
			new_dt.save()
			form.save_m2m()
			
			messages.success(request, 'Data FAQ berhasil disimpan.')
			return redirect('profile:adm_ppdb_faq')

		messages.error(request, 'Data FAQ gagal disimpan.')
		return redirect('profile:adm_ppdb_faq_edit', slug=slug)


@login_required
@is_verified()
def adm_setting(request):

	if request.method == 'GET':
		cinta = {}
		dtx = ppdb_AppSetting.objects.all()
		for x in dtx:
			cinta[x.nama] = x.keterangan

		context = {
			'title' : 'Setting Informasi',
			'form' : cinta,
		}

		return render(request, 'profile/ppbd/layouts/admin/setting/index.html', context)

	if request.method == 'POST':
		is_simpan = False
		dtx = request.POST
		for x in dtx:
			if x != 'csrfmiddlewaretoken':
				if dtx.get(x) != "":
					try:
						app_set = ppdb_AppSetting.objects.get(nama=x)
						app_set.keterangan = dtx.get(x)
						app_set.save()
					except:
						app_set = ppdb_AppSetting(nama=x, keterangan=dtx.get(x))
						app_set.save()

					is_simpan = True

		if is_simpan:
			messages.success(request, 'Setting Beranda berhasil disimpan')
			return redirect('profile:adm_ppdb_setting')

		messages.error(request, 'Setting Beranda gagal disimpan.')
		return redirect('profile:adm_ppdb_setting')



@login_required
@is_verified()
def softDelete(request, slug):
    message = ''
    try:
        sekarang = timezone.now()
        doc = ppdb_konten.objects.get(slug=slug)
        doc.deleted_at = sekarang
        doc.save()
        message = 'success'
    except ppdb_konten.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, slug):
    message = ''
    try:
        doc = ppdb_konten.objects.get(slug=slug)
        try:
            doc.foto.delete()
            doc.dokumen.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except ppdb_konten.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, slug):
    message = ''
    try:
        doc = ppdb_konten.objects.get(slug=slug)
        doc.deleted_at = None
        doc.save()
        message = 'success'
    except ppdb_konten.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)