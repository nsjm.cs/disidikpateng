from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags
from django.template.loader import render_to_string

@login_required
@is_verified()
@require_http_methods(["GET"])
def adm_berita(request):
	data_list = ppdb_News.objects.filter(deleted_at__isnull=True).order_by('-created_at')
	archives = ppdb_News.objects.filter(deleted_at__isnull=False).order_by('-created_at')

	context = {
		'title' : 'Setting Berita',
		'data_list': data_list,
		'archives': archives,
	}
	return render(request, 'profile/ppbd/layouts/admin/berita/index.html', context) 

@login_required
@is_verified()
def adm_berita_create(request):
	if request.method == 'GET':
		form = ppdb_NewsForm()
		form_foto = ppdb_NewsThumb()
		context = {
			'title' : 'ADD Berita',
			'form' : form,
			'form_foto' : form_foto,
		}

		template = 'profile/ppbd/layouts/admin/berita/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		form = ppdb_NewsForm(data=request.POST)
		form_foto = ppdb_NewsThumb(data=request.POST, files=request.FILES)

		if form.is_valid() and form_foto.is_valid():
			new_news = form.save(commit=False)
			new_news.created_by_id = request.user.id
			new_news.last_updated_by = request.user.id
			new_news.save()
			form.save_m2m()
			
			news = ppdb_News.objects.get(id = new_news.id)
			thumb_news = ppdb_NewsThumb(data=request.POST, files=request.FILES, instance=news)
			thumb_news.save()
			
			messages.success(request, 'Berita PPDB berhasil disimpan.')
			return redirect('profile:adm_ppdb_berita')

		messages.error(request, 'Berita PPDB gagal disimpan.')
		return redirect('profile:adm_ppdb_berita_create')


@login_required
@is_verified()
def adm_berita_edit(request, slug):
	tags = ''
	dtxs = ppdb_News.objects.get(slug=slug)

	if request.method == 'GET':
		form = ppdb_NewsForm(instance=dtxs)
		form_foto = ppdb_NewsThumb()
		for tag in  dtxs.tags.all():
			tags += f"{tag}, "

		context = {
			'title' : 'EDIT Berita',
			'form' : form,
			'form_foto' : form_foto,
			'edit' : True,
			'slug' : slug,
			'news' : dtxs,
			'tagsnya' : tags,
		}
		return render(request, 'profile/ppbd/layouts/admin/berita/create.html', context)

	if request.method == 'POST':
		path_file_lama = f"{settings.MEDIA_ROOT}/{dtxs.thumbnail}"
		foto_old = bool(dtxs.thumbnail)
		if bool(request.FILES):
			form = ppdb_NewsForm(data=request.POST, files=request.FILES, instance=dtxs)
			form_foto = ppdb_NewsThumb(data=request.POST, files=request.FILES, instance=dtxs)
			is_valid = form.is_valid() and form_foto.is_valid()
		else:
			form = ppdb_NewsForm(data=request.POST, instance=dtxs)
			is_valid = form.is_valid()

		if is_valid:
			new_news = form.save(commit=False)
			new_news.title = form.cleaned_data.get('title')
			new_news.content = form.cleaned_data.get('content')
			new_news.last_updated_by = request.user.id
			new_news.save()
			form.save_m2m()

			if len(request.FILES) > 0:
				if foto_old : 
					try:
						os.remove(path_file_lama)
					except:
						pass
				form_foto.save()
			
			messages.success(request, 'Data Berita PPDB berhasil disimpan.')
			return redirect('profile:adm_ppdb_berita')

		messages.error(request, 'Data Berita PPDB gagal disimpan.')
		return redirect('profile:adm_ppdb_berita_edit', slug=slug)

@login_required
@is_verified()
def softDelete(request, slug):
    message = ''
    try:
        sekarang = timezone.now()
        doc = ppdb_News.objects.get(slug=slug)
        doc.deleted_at = sekarang
        doc.save()
        message = 'success'
    except ppdb_News.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, slug):
    message = ''
    try:
        doc = ppdb_News.objects.get(slug=slug)
        try:
            doc.thumbnail.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except ppdb_News.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, slug):
    message = ''
    try:
        doc = ppdb_News.objects.get(slug=slug)
        doc.deleted_at = None
        doc.save()
        message = 'success'
    except ppdb_News.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)