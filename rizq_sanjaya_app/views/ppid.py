from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category
from django.template.loader import render_to_string

# JUMLAH DATA PER PAGE ===> PAGINATIONS
jml_data = 4

@require_http_methods(["GET"])
def index(request, slug):
	# page = request.GET.get('page', 1)
	news = News.objects.filter(deleted_at=None).order_by('-created_at')
	nws_jml = int(len(news))
	if nws_jml >= jml_data:
		thumbnail = {}
		for x in news:
			x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail))
			x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/')}
		news_list = news

	else:
		news_list = get_list_berita(jml_data)

	dt_ppid = modul_PPID.objects.get(slug=slug)
	ppid_lain = modul_PPID.objects.exclude(slug=slug)

	context = {
		'title' : 'PPID',
		'dt_ppid' : dt_ppid,
		'ppid_lain' : ppid_lain,
		'news_list' : news_list,
		'dt_tags': querytags('modul_ppid'),
	}
	return render(request, 'profile/ppid/index.html', context)

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
	ppid = modul_PPID.objects.filter(deleted_at__isnull=True).order_by('-created_at')
	archives = modul_PPID.objects.filter(deleted_at__isnull=False).order_by('-created_at')

	context = {
		'title': 'PPID - Admin',
		'dt_ppid': ppid,
		'archives': archives,
	}

	return render(request, 'profile/admin/ppid/index.html', context)

@login_required
@is_verified()
def create(request):
	template = ''
	context = {} 
	form = ''

	if request.method == 'GET':
		form = PPIDForm()
		context = {
			'title' : 'ADD PPID',
			'form' : form,
		}

		template = 'profile/admin/ppid/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		form = PPIDForm(data=request.POST)

		if form.is_valid():
			new_news = form.save(commit=False)
			new_news.created_by_id = request.user.id
			new_news.last_updated_by = request.user.id
			new_news.save()
			form.save_m2m()
			
			messages.success(request, 'PPID berhasil disimpan.')
			return redirect('profile:admin_ppid')

		messages.error(request, 'PPID gagal disimpan.')
		return redirect('profile:admin_ppid_create')

@login_required
@is_verified()
def edit(request, slug):
	template = ''
	context = {} 
	form = ''
	category = get_category('news')
	if request.method == 'GET':
		try:
			ppid = modul_PPID.objects.get(slug=slug)
			form = PPIDForm(instance=ppid)
			tags = ''
			slug = ppid.slug
			for tag in  ppid.tags.all():
				tags += f"{tag}, "
		except:
			ppid = None
			form = PPIDForm()
			tags = ''
			slug = None

		context = {
			'title' : 'EDIT PPID',
			'form' : form,
			'edit' : 'true',
			'slug' : slug,
			'ppid' : ppid,
			'tagsnya' : tags,
			'kategori' : category,
		}
		template = 'profile/admin/ppid/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		ppid = modul_PPID.objects.get(slug=slug)
		form = PPIDForm(data=request.POST, instance=ppid)
		is_valid = form.is_valid()

		if is_valid:
			new_ppid = form.save(commit=False)
			new_ppid.title = form.cleaned_data.get('title')
			new_ppid.description = form.cleaned_data.get('description')
			new_ppid.last_updated_by = request.user.id
			new_ppid.save()
			form.save_m2m()

			messages.success(request, 'PPID berhasil disimpan.')
			return redirect('profile:admin_ppid')

		messages.error(request, 'PPID gagal disimpan.')
		return redirect('profile:admin_ppid_create')

@login_required
@is_verified()
def softDelete(request, slug):
	message = ''
	try:
		sekarang = timezone.now()
		doc = modul_PPID.objects.get(slug=slug)
		doc.deleted_at = sekarang
		doc.save()
		message = 'success'
	except modul_PPID.DoesNotExist: 
		message = 'error'

	context = {
			'message' : message,
		}

	return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, slug):
	message = ''
	try:
		doc = modul_PPID.objects.get(slug=slug)
		try:
			doc.thumbnail.delete()
		except:
			pass
		doc.delete()
		message = 'success'
	except modul_PPID.DoesNotExist:
		message = 'error'

	context = {
			'message' : message,
		}

	return HttpResponse(context)
	
@login_required
@is_verified()
def restore(request, slug):
	message = ''
	try:
		doc = modul_PPID.objects.get(slug=slug)
		doc.deleted_at = None
		doc.save()
		message = 'success'
	except modul_PPID.DoesNotExist:
		message = 'error'

	context = {
			'message' : message,
		}

	return HttpResponse(context)