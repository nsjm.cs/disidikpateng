from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News
from django.utils import timezone
from django.conf import settings
from datetime import datetime
from ..decorators import *
from django.db import connections
from django.db.models import Q, Count
from ..helpers import dictfetchall, get_list_berita, get_category, get_list_galeri
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
import os
import pprint
from django.template.loader import render_to_string
import requests
from django.db.models import Q



@require_http_methods(["GET"])
def index(request):
    sejarah = ProfilSKPD.objects.filter(typexs = 'Sejarah', status='Publish')

    context = {
         
         'sejarah':sejarah,
 
    }
    return render(request, 'profile/frontend/profil/sejarah.html', context)

@require_http_methods(["GET"])
def VisiMisi(request):
     misi = ProfilSKPD.objects.filter(typexs = 'Misi', status='Publish')
     visi = ProfilSKPD.objects.filter(typexs = 'Visi', status='Publish')
     context = {
         
          'misi':misi,
          'visi':visi,
          
     }
     return render(request, 'profile/frontend/profil/visimisi.html', context)

@require_http_methods(["GET"])
def Tugas(request):
    tupoksi = ProfilSKPD.objects.filter(Q(typexs='Tupoksi') & Q(status='Publish'))
    context = {
     'tupoksi' : tupoksi,
    }
    return render(request, 'profile/frontend/profil/tugas.html', context)


@require_http_methods(["GET"])
def Struktur(request):
    struktur = ProfilSKPD.objects.filter(typexs = 'Struktur Organisasi', status='Publish')
    context = {
         
         'struktur':struktur,
       
    }
    return render(request, 'profile/frontend/profil/struktur.html', context)


@require_http_methods(["GET"])
def Pejabat(request):
    profil = ProfilSKPD.objects.filter(typexs='Profil Pejabat', status='Publish')
    context = {
     
     'profil':profil,
       
    }
    return render(request, 'profile/frontend/profil/pejabat.html', context)
