from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import ProfilSKPD
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category, type_posting

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
	page = request.GET.get('page', 1)
	ProfileSKPD = ProfilSKPD.objects.filter(status='Publish').order_by('typexs','title')
	archives = ProfilSKPD.objects.filter(status='Draft').order_by('typexs','title')
	paginator = Paginator(ProfileSKPD, 15)
	try:
		ProfileSKPD = paginator.page(page)
	except PageNotAnInteger:
		ProfileSKPD = paginator.page(1)
	except EmptyPage:
		ProfileSKPD = paginator.page(paginator.num_pages)

	for x in ProfileSKPD:
		if (x.foto):
			x.is_foto = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))  
			if(x.is_foto == False):
				x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
		else:
			x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 

	for x in archives:
		if (x.foto):
			x.is_foto = os.path.isfile(settings.MEDIA_ROOT+str(x.foto))  
			if(x.is_foto == False):
				x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
		else:
			x.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'}      

	context = {
		'title' : 'Profil SKPD - Admin',
		'data_ProfileSKPD' : ProfileSKPD,
		'archives' : archives,
	}
	
	return render(request, 'profile/admin/profilSKPD/index.html', context)

@login_required
@is_verified()
def create(request):
	template = ''
	context = {} 
	form = ''
	tp_posting = type_posting()

	if request.method == 'GET':
		form = Profilskpd()
		form_foto = ProfilskpdForm()
		foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'}

		context = {
			'title' : 'ADD Profil SKPD',
			'form' : form,
			'form_foto' : form_foto,
			'tp_posting' : tp_posting,
			'layanandinas' : {'foto':foto},
		}

		template = 'profile/admin/profilSKPD/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		form = Profilskpd(data=request.POST)
		form_foto = ProfilskpdForm(data=request.POST, files=request.FILES)
		if form.is_valid():
			new_dt = form.save(commit=False)
			new_dt.save()
			form.save_m2m()
			
			if form_foto.is_valid():
				get_dt = ProfilSKPD.objects.get(id = new_dt.id)
				form_foto_profil = ProfilskpdForm(data=request.POST, files=request.FILES, instance=get_dt)
				form_foto_profil.save()
			
			messages.success(request, 'Profil dinas berhasil disimpan.')
			return redirect('profile:admin_profilskpd')
		print(request.POST)
		messages.error(request, 'Profil dinas gagal disimpan.')
		return redirect('profile:admin_profilskpd_create')
	
@login_required
@is_verified()
def edit(request, slug):
	template = ''
	context = {} 
	form = Profilskpd()
	tp_posting = type_posting()
	
	if request.method == 'GET':
		layanandinas = ProfilSKPD.objects.get(slug = slug)
		form = Profilskpd(instance=layanandinas)
		form_foto = ProfilskpdForm()

		if (layanandinas.foto):
			is_foto = os.path.isfile(settings.MEDIA_ROOT+str(layanandinas.foto))  
			if(is_foto == False):
				layanandinas.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
		else:
			layanandinas.foto = {'url':settings.STATIC_URL+'frontend/img/logo/logo-animate.gif'} 
		
		context = {
			'title' : 'EDIT Profil SKPD',
			'form' : form,
			'form_foto' : form_foto,
			'edit' : 'true',
			'slug' : slug,
			'layanandinas' : layanandinas,
			'tp_posting' : tp_posting,
		}
		template = 'profile/admin/profilSKPD/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		dt_profil = ProfilSKPD.objects.get(slug=slug)
		path_file_lama = f"{settings.MEDIA_ROOT}/{dt_profil.foto}"
		foto_old = bool(dt_profil.foto)
		if bool(request.FILES):
			form = Profilskpd(data=request.POST, files=request.FILES, instance=dt_profil)
			form_foto = ProfilskpdForm(data=request.POST, files=request.FILES, instance=dt_profil)
			is_valid = form.is_valid() and form_foto.is_valid()
		else:
			form = Profilskpd(data=request.POST, instance=dt_profil)
			is_valid = form.is_valid()

		if is_valid:
			new_dt = form.save(commit=False)
			new_dt.title = form.cleaned_data.get('title')
			new_dt.typexs = form.cleaned_data.get('typexs')
			new_dt.description = form.cleaned_data.get('description')
			new_dt.save()
			form.save_m2m()
			if len(request.FILES) > 0:
				if foto_old : 
					try:
						os.remove(path_file_lama)
					except:
						pass
				form_foto.save()

			messages.success(request, 'Profil SKPD berhasil disimpan')
			return redirect('profile:admin_profilskpd')
		messages.error(request, 'Profil SKPD gagal disimpan.')
		return redirect('profile:admin_profilskpd_edit', slug=slug)


@login_required
@is_verified()
def softDelete(request, id):
	message = ''
	try:
		status = 'Draft'
		doc = ProfilSKPD.objects.get(id=id)
		doc.status = status
		doc.save()
		message = 'success'
	except ProfilSKPD.DoesNotExist:
		message = 'error'

	context = {
			'message' : message,
		}

	return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, id):
	message = ''
	try:
		doc = ProfilSKPD.objects.get(id=id)
		try:
			doc.image.delete()
		except:
			pass
		doc.delete()
		message = 'success'
	except ProfilSKPD.DoesNotExist:
		message = 'error'

	context = {
			'message' : message,
		}

	return HttpResponse(context)
	
@login_required
@is_verified()
def restore(request, id):
	message = ''
	try:
		status = 'Publish'
		doc = ProfilSKPD.objects.get(id=id)
		doc.status = status
		doc.save()
		message = 'success'
	except ProfilSKPD.DoesNotExist:
		message = 'error'

	context = {
			'message' : message,
		}

	return HttpResponse(context)