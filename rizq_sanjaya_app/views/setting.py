from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods

from rizq_sanjaya_app.views.news import category
from ..forms import *
from django.contrib import messages
from ..models import *
from django.utils import timezone
import os
import json
from ..decorators import *
from ..helpers import *
from datetime import datetime
from django.db import IntegrityError, connections

from pyffmpeg import FFmpeg


@require_http_methods(["GET"])
def index(request):
	agenda = Agenda.objects.filter(deleted_at=None).order_by('-created_at')
	context = {
		'title' : 'Agenda',
		'agendas' : agenda,
	}
	return render(request, 'profile/agenda/index.html', context)

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def admin_index_running_text(request):
	if request.method == 'GET':
		running_text = RunningText.objects.all().order_by('-created_at')
		archives = RunningText.objects.filter(deleted_at__isnull=False).order_by('-created_at')
		form = RunningTextForm()
		context = {
			'title' : 'Running Text - Admin',
			'running_texts' : running_text,
			'archives' : archives,
			'breadcrumb' : 'Running Text',
			'form': form,
		}
		
		return render(request, 'profile/admin/setting/index.html', context)

	if request.method == 'POST':
		form = RunningTextForm(data=request.POST)
		if form.is_valid():
			form.save()
			messages.success(request, 'Running Text berhasil disimpan.')
			return redirect('profile:admin_setting_running_text',)
		messages.error(request, 'Running Text gagal disimpan.')
		return redirect('profile:admin_setting_running_text',)

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def admin_index_category(request):
	if request.method == 'GET':
		category = Category.objects.all().order_by('typexs','nama')
		form = CategoryForm()
		context = {
			'title' : 'Kategori - Admin',
			'categories' : category,
			'breadcrumb' : 'Kategori',
			'form': form,
		}
		
		return render(request, 'profile/admin/setting/index.html', context)

	if request.method == 'POST':
		form = CategoryForm(data=request.POST)
		if form.is_valid():
			try:
				kat = form.save()
				x = Category.objects.get(id = kat.id)
				if 'foto' in request.FILES:
					x.foto = request.FILES['foto']
				x.save()
				messages.success(request, 'Kategori berhasil disimpan.')
				return redirect('profile:admin_setting_category',)
			except Exception as x:
				messages.error(request, f'Kategori gagal disimpan karena {str(x)}.')
				return redirect('profile:admin_setting_category',)
			
		return redirect('profile:admin_setting_category',)

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
@require_http_methods(["POST"])
def admin_index_running_text_edit(request):
	if request.method == 'POST':
		running_text = RunningText.objects.get(id = request.POST.get('id'))
		form = RunningTextForm(data=request.POST, instance=running_text)
		if form.is_valid():
			form.save()
			messages.success(request, 'Running Text berhasil disimpan.')
			return redirect('profile:admin_setting_running_text',)
		messages.error(request, 'Running Text gagal disimpan.')
		return redirect('profile:admin_setting_running_text',)

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
@require_http_methods(["POST"])
def admin_index_category_edit(request):
	if request.method == 'POST':
		category = Category.objects.get(id = request.POST.get('id'))
		form = CategoryForm(data=request.POST, instance=category)
		foto_old = bool(category.foto)

		if foto_old : 
			path_file_lama = f"{settings.MEDIA_ROOT}/{category.foto}"

		if form.is_valid():
			kat = form.save()
			x = Category.objects.get(id = kat.id)

			if 'foto' in request.FILES:
				if foto_old : 
					if len(request.FILES) > 0:
						try:
							os.remove(path_file_lama)
						except:
							pass
				x.foto = request.FILES['foto']
				x.save()
			messages.success(request, 'Kategori berhasil disimpan.')
			return redirect('profile:admin_setting_category',)
		messages.error(request, 'Kategori gagal disimpan.')
		return redirect('profile:admin_setting_category',)

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def admin_index_link(request):
	if request.method == 'GET':
		links = Link.objects.all().order_by('nama')
		archives = Link.objects.filter(deleted_at__isnull=False).order_by('nama')
		form = LinkForm()
		context = {
			'title' : 'Link - Admin',
			'links' : links,
			'archives' : archives,
			'breadcrumb' : 'Link',
			'form': form,
		}
		
		return render(request, 'profile/admin/setting/index.html', context)

	if request.method == 'POST':
		form =LinkForm(data=request.POST)
		if form.is_valid():
			form.save()
			messages.success(request, 'Link berhasil disimpan.')
			return redirect('profile:admin_setting_link',)
		messages.error(request, 'Link gagal disimpan.')
		return redirect('profile:admin_setting_link',)

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
@require_http_methods(["POST"])
def admin_index_link_edit(request):
	if request.method == 'POST':
		link = Link.objects.get(id = request.POST.get('id'))
		form = LinkForm(data=request.POST, instance=link)
		if form.is_valid():
			form.save()
			messages.success(request, 'Link berhasil disimpan.')
			return redirect('profile:admin_setting_link',)
		messages.error(request, 'Link gagal disimpan.')
		return redirect('profile:admin_setting_link',)
	
@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def admin_index_carabayar(request):
	form = ''

	if request.method == 'GET':
		try: 
			highlight = AppSetting.objects.get(nama = 'highlight')
		except: 
			highlight = ''

		try: 
			deskripsi = AppSetting.objects.get(nama = 'deskripsi')
			# tanggal_lahir = tanggal.tanggal.strftime('%Y-%m-%d')
		except: 
			deskripsi = ''

		try: 
			embed_maps_bayar = AppSetting.objects.get(nama = 'embed_maps_bayar')
		except: 
			embed_maps_bayar = ''

		try: 
			image_cara_bayar = AppSetting.objects.get(nama = 'image_cara_bayar')
		except: 
			image_cara_bayar = ''

		try: 
			bank = AppSetting.objects.get(nama = 'bank')
		except: 
			bank = ''

		try: 
			norek = AppSetting.objects.get(nama = 'norek')
		except: 
			norek = ''

		try: 
			an = AppSetting.objects.get(nama = 'an')
		except: 
			an = ''

		faq = FAQ.objects.all()

		# form = InfografisForm()
		context = {
			'title' : 'Cara Bayar - Admin',
			'breadcrumb' : 'Cara Bayar',
			'form' : form,
			'highlight' : highlight,
			'deskripsi' : deskripsi,
			'embed' : embed_maps_bayar,
			'image' : image_cara_bayar,
			'bank' : bank,
			'norek' : norek,
			'an' : an,
			'faq' : faq,
		}
		return render(request, 'profile/admin/setting/index.html', context)

	if request.method == 'POST':
		infografis = ''
		jenis = request.POST.get('jenis')
		form = ''
		if jenis == 'overview':
			try:
				infografis = AppSetting.objects.get(nama='highlight')
				infografis.keterangan = request.POST.get('highlight')
				infografis.save()
			except:
				infografis = AppSetting(nama='highlight', keterangan=request.POST.get('highlight'))
				infografis.save()

			try:
				infografis = AppSetting.objects.get(nama='deskripsi')
				infografis.keterangan = request.POST.get('deskripsi')
				infografis.save()
			except:
				infografis = AppSetting(nama='deskripsi', keterangan=request.POST.get('deskripsi'))
				infografis.save()

			try:
				infografis = AppSetting.objects.get(nama='embed_maps_bayar')
				maps = request.POST.get('embed_maps_bayar')
				print(maps)
				infografis.keterangan = maps
				infografis.save()
			except:
				infografis = AppSetting(nama='embed_maps_bayar', keterangan=request.POST.get('embed_maps_bayar'))
				infografis.save()

			try: 
				image = request.FILES.get('image')
				if 'image' in request.FILES:
					foto_diskon = AppSetting.objects.get(nama = 'image_cara_bayar')
					foto_diskon.image = image
					foto_diskon.save()
			except: 
				image = request.FILES.get('image')
				foto_diskon = AppSetting(nama='image_cara_bayar', image=request.FILES.get('image'))
				foto_diskon.save()

		if jenis == 'pembayaran':
			try:
				infografis = AppSetting.objects.get(nama='bank')
				infografis.keterangan = request.POST.get('bank')
				infografis.save()
			except:
				infografis = AppSetting(nama='bank', keterangan=request.POST.get('bank'))
				infografis.save()

			try:
				infografis = AppSetting.objects.get(nama='norek')
				infografis.keterangan = request.POST.get('norek')
				infografis.save()
			except:
				infografis = AppSetting(nama='norek', keterangan=request.POST.get('norek'))
				infografis.save()

			try:
				infografis = AppSetting.objects.get(nama='an')
				infografis.keterangan = request.POST.get('an')
				infografis.save()
			except:
				infografis = AppSetting(nama='an', keterangan=request.POST.get('an'))
				infografis.save()

		if jenis == 'faq':
			faqs = []
			for key in request.POST:
				if key.startswith('p') and key[1:].isdigit():
					index = int(key[1:])
					pertanyaan = request.POST[key]
					jawaban = request.POST.get(f'j{index}', '')
					highlight = request.POST.get(f'h{index}', '')
					existing_faq = FAQ.objects.filter(highlight = highlight).first()

					if existing_faq:
						# Jika ada, update nilai highlight
						existing_faq.pertanyaan = pertanyaan
						existing_faq.jawaban = jawaban
						existing_faq.save()
					else:
						# Jika tidak ada, buat objek FAQ baru
						faq = FAQ(pertanyaan=pertanyaan, jawaban=jawaban, highlight=highlight)
						faqs.append(faq)

			# Simpan semua FAQ baru ke database
			FAQ.objects.bulk_create(faqs)

		messages.success(request, 'Informasi berhasil disimpan.')
		return redirect('profile:admin_setting_carabayar')
	
@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def admin_index_infografis(request):
	form = ''

	if request.method == 'GET':
		try: 
			deskripsi = RnW.objects.get(jenis = 'deskripsi')
		except: 
			deskripsi = ''
		try: 
			pertanyaan_1 = RnW.objects.get(jenis = 'pertanyaan_1')
		except: 
			pertanyaan_1 = ''
		try: 
			jawaban_1 = RnW.objects.get(jenis = 'jawaban_1')
		except: 
			jawaban_1 = ''
		try: 
			pertanyaan_2 = RnW.objects.get(jenis = 'pertanyaan_2')
		except: 
			pertanyaan_2 = ''
		try: 
			jawaban_2 = RnW.objects.get(jenis = 'jawaban_2')
		except: 
			jawaban_2 = ''
		try: 
			pertanyaan_3 = RnW.objects.get(jenis = 'pertanyaan_3')
		except: 
			pertanyaan_3 = ''
		try: 
			jawaban_3 = RnW.objects.get(jenis = 'jawaban_3')
		except: 
			jawaban_3 = ''
		try: 
			pertanyaan_4 = RnW.objects.get(jenis = 'pertanyaan_4')
		except: 
			pertanyaan_4 = ''
		try: 
			jawaban_4 = RnW.objects.get(jenis = 'jawaban_4')
		except: 
			jawaban_4 = ''
		try: 
			pertanyaan_5 = RnW.objects.get(jenis = 'pertanyaan_5')
		except: 
			pertanyaan_5 = ''
		try: 
			jawaban_5 = RnW.objects.get(jenis = 'jawaban_5')
		except: 
			jawaban_5 = ''

		try: 
			alamat = AppSetting.objects.get(nama = 'alamat')
		except: 
			alamat = ''
			
		try: 
			alamat_link = AppSetting.objects.get(nama = 'alamat_link')
		except: 
			alamat_link = ''

		try: 
			embed_maps = AppSetting.objects.get(nama = 'embed_maps')
		except: 
			embed_maps = ''

		try: 
			telepon = AppSetting.objects.get(nama = 'telepon')
		except: 
			telepon = ''

		try: 
			telepon_text = AppSetting.objects.get(nama = 'telepon_text')
		except: 
			telepon_text = ''

		try: 
			whatsapp = AppSetting.objects.get(nama = 'whatsapp')
		except: 
			whatsapp = ''

		try: 
			whatsapp_text = AppSetting.objects.get(nama = 'whatsapp_text')
		except: 
			whatsapp_text = ''

		try: 
			email = AppSetting.objects.get(nama = 'email')
		except: 
			email = ''

		try: 
			meta_property_title = AppSetting.objects.get(nama = 'meta_property_title')
		except: 
			meta_property_title = ''

		try: 
			meta_author = AppSetting.objects.get(nama = 'meta_author')
		except: 
			meta_author = ''

		try: 
			meta_keywords = AppSetting.objects.get(nama = 'meta_keywords')
		except: 
			meta_keywords = ''

		try: 
			meta_property_description = AppSetting.objects.get(nama = 'meta_property_description')
		except: 
			meta_property_description = ''

		try: 
			facebook = AppSetting.objects.get(nama = 'facebook')
		except: 
			facebook = ''

		try: 
			link_facebook = AppSetting.objects.get(nama = 'link_facebook')
		except: 
			link_facebook = ''

		try: 
			link_twitter = AppSetting.objects.get(nama = 'link_twitter')
		except: 
			link_twitter = ''

		try: 
			link_instagram = AppSetting.objects.get(nama = 'link_instagram')
		except: 
			link_instagram = ''

		try: 
			link_youtube = AppSetting.objects.get(nama = 'link_youtube')
		except: 
			link_youtube = ''

		try: 
			lazada = AppSetting.objects.get(nama = 'lazada')
		except: 
			lazada = ''

		try: 
			shopee = AppSetting.objects.get(nama = 'shopee')
		except: 
			shopee = ''

		try: 
			tokped = AppSetting.objects.get(nama = 'tokped')
		except: 
			tokped = ''

		try: 
			link_tiktok = AppSetting.objects.get(nama = 'link_tiktok')
		except: 
			link_tiktok = ''

		try: 
			header_layanan = AppSetting.objects.get(nama = 'header_layanan')
		except: 
			header_layanan = ''

		try: 
			header_galeri = AppSetting.objects.get(nama = 'header_galeri')
		except: 
			header_galeri = ''

		try: 
			header_kontak = AppSetting.objects.get(nama = 'header_kontak')
		except: 
			header_kontak = ''

		try: 
			foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
		except: 
			foto_diskon = ''

		try: 
			diskon = AppSetting.objects.get(nama = 'diskon')
		except: 
			diskon = ''

		try: 
			tanggal = AppSetting.objects.get(nama = 'tanggal')
			tanggal_lahir = tanggal.tanggal.strftime('%Y-%m-%d')
		except: 
			tanggal_lahir = ''

		# form = InfografisForm()
		context = {
			'title' : 'Infografis - Admin',
			'alamat' : alamat,
			'alamat_link' : alamat_link,
			'embed_maps' : embed_maps,
			'telepon' : telepon,
			'telepon_text' : telepon_text,
			'whatsapp' : whatsapp,
			'whatsapp_text' : whatsapp_text,
			'email' : email,
			'meta_property_title' : meta_property_title,
			'meta_author' : meta_author,
			'meta_property_description' : meta_property_description,
			'meta_keywords' : meta_keywords,
			'facebook' : facebook,
			'link_facebook' : link_facebook,
			'link_youtube' : link_youtube,
			'link_twitter' : link_twitter,
			'link_instagram' : link_instagram,
			'link_tiktok' : link_tiktok,
			'breadcrumb' : 'Infografis',
			'header_layanan' : header_layanan,
			'header_galeri' : header_galeri,
			'header_kontak' : header_kontak,
			'form' : form,
			'p1' : pertanyaan_1,
			'p2' : pertanyaan_2,
			'p3' : pertanyaan_3,
			'p4' : pertanyaan_4,
			'p5' : pertanyaan_5,
			'j1' : jawaban_1,
			'j2' : jawaban_2,
			'j3' : jawaban_3,
			'j4' : jawaban_4,
			'j5' : jawaban_5,
			'deskripsi' : deskripsi,
			'lazada' : lazada,
			'shopee' : shopee,
			'tokped' : tokped,
			'foto_diskon' : foto_diskon,
			'diskon' : diskon,
			'tanggal' : tanggal_lahir,
		}
		return render(request, 'profile/admin/setting/index.html', context)

	if request.method == 'POST':
		infografis = ''
		jenis = request.POST.get('jenis')
		form = ''
		if jenis == 'overview':
			try:
				infografis = AppSetting.objects.get(nama='alamat')
				infografis.keterangan = request.POST.get('alamat')
				infografis.save()
			except:
				infografis = AppSetting(nama='alamat', keterangan=request.POST.get('alamat'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='alamat_link')
				infografis.keterangan = request.POST.get('alamat_link')
				infografis.save()
			except:
				infografis = AppSetting(nama='alamat_link', keterangan=request.POST.get('alamat_link'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='embed_maps')
				infografis.keterangan = request.POST.get('embed_maps')
				infografis.save()
			except:
				infografis = AppSetting(nama='embed_maps', keterangan=request.POST.get('embed_maps'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='telepon_text')
				infografis.keterangan = request.POST.get('telepon_text')
				infografis.save()
			except:
				infografis = AppSetting(nama='telepon_text', keterangan=request.POST.get('telepon_text'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='telepon')
				infografis.keterangan = request.POST.get('telepon')
				infografis.save()
			except:
				infografis = AppSetting(nama='telepon', keterangan=request.POST.get('telepon'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='whatsapp')
				infografis.keterangan = request.POST.get('whatsapp')
				infografis.save()
			except:
				infografis = AppSetting(nama='whatsapp', keterangan=request.POST.get('whatsapp'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='whatsapp_text')
				infografis.keterangan = request.POST.get('whatsapp_text')
				infografis.save()
			except:
				infografis = AppSetting(nama='whatsapp_text', keterangan=request.POST.get('whatsapp_text'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='email')
				infografis.keterangan = request.POST.get('email')
				infografis.save()
			except:
				infografis = AppSetting(nama='email', keterangan=request.POST.get('email'))
				infografis.save()

		if jenis == 'sosmed':
			try:
				infografis = AppSetting.objects.get(nama='facebook')
				infografis.keterangan = request.POST.get('facebook')
				infografis.save()
			except:
				infografis = AppSetting(nama='facebook', keterangan=request.POST.get('facebook'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='link_facebook')
				infografis.keterangan = request.POST.get('link_facebook')
				infografis.save()
			except:
				infografis = AppSetting(nama='link_facebook', keterangan=request.POST.get('link_facebook'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='link_twitter')
				infografis.keterangan = request.POST.get('link_twitter')
				infografis.save()
			except:
				infografis = AppSetting(nama='link_twitter', keterangan=request.POST.get('link_twitter'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='link_youtube')
				infografis.keterangan = request.POST.get('link_youtube')
				infografis.save()
			except:
				infografis = AppSetting(nama='link_youtube', keterangan=request.POST.get('link_youtube'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='link_instagram')
				infografis.keterangan = request.POST.get('link_instagram')
				infografis.save()
			except:
				infografis = AppSetting(nama='link_instagram', keterangan=request.POST.get('link_instagram'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='link_tiktok')
				infografis.keterangan = request.POST.get('link_tiktok')
				infografis.save()
			except:
				infografis = AppSetting(nama='link_tiktok', keterangan=request.POST.get('link_tiktok'))
				infografis.save()

		if jenis == 'ecomerce':
			try:
				infografis = AppSetting.objects.get(nama='lazada')
				infografis.keterangan = request.POST.get('lazada')
				infografis.save()
			except:
				infografis = AppSetting(nama='lazada', keterangan=request.POST.get('lazada'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='shopee')
				infografis.keterangan = request.POST.get('shopee')
				infografis.save()
			except:
				infografis = AppSetting(nama='shopee', keterangan=request.POST.get('shopee'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='tokped')
				infografis.keterangan = request.POST.get('tokped')
				infografis.save()
			except:
				infografis = AppSetting(nama='tokped', keterangan=request.POST.get('tokped'))
				infografis.save()

		if jenis == 'header':
			try:
				infografis = AppSetting.objects.get(nama='header_layanan')
				infografis.keterangan = request.POST.get('header_layanan')
				infografis.save()
			except:
				infografis = AppSetting(nama='header_layanan', keterangan=request.POST.get('header_layanan'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='header_galeri')
				infografis.keterangan = request.POST.get('header_galeri')
				infografis.save()
			except:
				infografis = AppSetting(nama='header_galeri', keterangan=request.POST.get('header_galeri'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='header_kontak')
				infografis.keterangan = request.POST.get('header_kontak')
				infografis.save()
			except:
				infografis = AppSetting(nama='header_kontak', keterangan=request.POST.get('header_kontak'))
				infografis.save()



		if jenis == 'meta':
			try:
				infografis = AppSetting.objects.get(nama='meta_property_title')
				infografis.keterangan = request.POST.get('meta_property_title')
				infografis.save()
			except:
				infografis = AppSetting(nama='meta_property_title', keterangan=request.POST.get('meta_property_title'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='meta_author')
				infografis.keterangan = request.POST.get('meta_author')
				infografis.save()
			except:
				infografis = AppSetting(nama='meta_author', keterangan=request.POST.get('meta_author'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='meta_keywords')
				infografis.keterangan = request.POST.get('meta_keywords')
				infografis.save()
			except:
				infografis = AppSetting(nama='meta_keywords', keterangan=request.POST.get('meta_keywords'))
				infografis.save()
			try:
				infografis = AppSetting.objects.get(nama='meta_property_description')
				infografis.keterangan = request.POST.get('meta_property_description')
				infografis.save()
			except:
				infografis = AppSetting(nama='meta_property_description', keterangan=request.POST.get('meta_property_description'))
				infografis.save()

		if jenis == 'reseller':
			try: 
				image = request.FILES.get('image')
				if 'image' in request.FILES:
					foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
				# print(image)
				# path_file_lama = f"{settings.MEDIA_ROOT}/{foto_diskon.image}"
				# os.remove(path_file_lama)
				# print(image)
					foto_diskon.image = image
					foto_diskon.save()
			except: 
				image = request.FILES.get('image')
				# print(image)
				foto_diskon = AppSetting(nama='foto_diskon', image=request.FILES.get('image'))
				foto_diskon.save()

			try:
				diskon = AppSetting.objects.get(nama='diskon')
				diskon.keterangan = request.POST.get('diskon')
				diskon.save()
			except:
				diskon = AppSetting(nama='diskon', keterangan=request.POST.get('diskon'))
				diskon.save()

			try:
				tanggal = AppSetting.objects.get(nama='tanggal')
				tanggal.tanggal = request.POST.get('tanggal')
				tanggal.save()
			except:
				tanggal = AppSetting(nama='tanggal', tanggal=request.POST.get('tanggal'))
				tanggal.save()

		messages.success(request, 'Informasi berhasil disimpan.')
		return redirect('profile:admin_setting_infografis')
		
@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def admin_index_carousel(request):
	if request.method == 'GET':
		carousel = Carousel.objects.filter(deleted_at__isnull=True).order_by('-created_at')
		archives = Carousel.objects.filter(deleted_at__isnull=False).order_by('-created_at')

		try: 
			is_carousel = AppSetting.objects.get(nama='is_carousel')
		except: 
			is_carousel = ''

		context = {
			'title' : 'Carousel - Admin',
			'carousels' : carousel,
			'archives' : archives,
			'breadcrumb' : 'Carousel',
			'is_carousel':is_carousel,
		}
		return render(request, 'profile/admin/setting/index.html', context)

	if request.method == 'POST':
		conf = ''
		jenis = request.POST.get('jenis')

		if jenis == 'content':
			try:
				conf = AppSetting.objects.get(nama='is_carousel')
				conf.keterangan = request.POST.get('is_carousel')
				conf.save()
			except:
				conf = AppSetting(nama='is_carousel', keterangan=request.POST.get('is_carousel'))
				conf.save()

		messages.success(request, 'Perubahan berhasil disimpan.')
		return redirect('profile:admin_setting_carousel')

@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def admin_index_destinasi(request):
	if request.method == 'GET':
		carousel = Destinasi.objects.filter(deleted_at = None).order_by('-created_at')
		archives = Destinasi.objects.filter(deleted_at__isnull=False).order_by('-created_at')
		foto = DestinasiImage.objects.all()
		first_photo = foto.first()
		print(first_photo)
		context = {
			'title' : 'Destinasi - Admin',
			'carousels' : carousel,
			'archives' : archives,
			'breadcrumb' : 'Destinasi',
			'foto' : foto,
			'first_photo_id': first_photo.id if first_photo else None,
		}
		
		return render(request, 'profile/admin/setting/index.html', context)

@require_http_methods(["GET"])
def detail_destinasi(request, id):
	template = 'profile/admin/setting/detail_destinasi.html'
	try:
		halaman = Destinasi.objects.get(id=id)
		foto = DestinasiImage.objects.filter(destinasi = id)
	except Destinasi.DoesNotExist:
		halaman = None
		foto = None
	context = {
		'title' : halaman.nama if halaman != None else 'TIDAK DITEMUKAN',
		'halaman' : halaman,
		'foto' : foto,
	}
	return render(request, template, context)

@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def edit_destinasi(request, id):
	template = ''
	context = {} 
	
	if request.method == 'GET':
		try:
			carousel = Destinasi.objects.get(id=id)
		except:
			carousel = None

		
		context = {
			'title' : 'EDIT DESTINASI WISATA',
			'edit' : 'true',
			'carousel': carousel,
			'breadcrumb': 'Destinasi',
		}
		template = 'profile/admin/setting/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		carousel = Destinasi.objects.get(id=id)
		
		foto = DestinasiImage.objects.filter(destinasi = id)
		foto_old = bool(foto)
		nama = request.POST.get('nama')
		paket = request.POST.get('popular')
		keterangan = request.POST.get('keterangan')
		fasilitas = request.POST.get('fasilitas')
		images_id = request.FILES.getlist('foto')

		# if foto_old : 
		#     path_file_lama = f"{settings.MEDIA_ROOT}/{foto[0].images}"

		is_valid = True
		if is_valid:
			
			if foto_old and images_id:
				for foto_obj in foto:
					path_file_lama = os.path.join(settings.MEDIA_ROOT, str(foto_obj.images.name))
					try:
						os.remove(path_file_lama)
					except OSError as e:
						# Handle error saat menghapus file lama
						# Misalnya, tampilkan pesan error atau lakukan tindakan lain
						pass 
				# if len(request.FILES) > 0:
				# try:
				#     os.remove(path_file_lama)
				# except:
				#     pass

			carousel.nama = nama
			paket_id = get_object_or_404(Carousel, id = paket)
			carousel.paket = paket_id
			carousel.deskripsi = keterangan
			carousel.fasilitas = fasilitas
			carousel.save()
			if len(request.FILES) > 0:
				for x, foto_obj in zip(images_id, foto):
					foto_obj.images = x
					foto_obj.save()
					# DestinasiImage.objects.filter(destinasi = id).update(images=x.name)

			messages.success(request, 'Destinasi Wisata berhasil disimpan.')
			return redirect('profile:admin_setting_destinasi')

		messages.error(request, 'Destinasi Wisata gagal disimpan.')
		return render(request, 'profile/admin/setting/create.html', {
			'title' : 'EDIT DESTINASI WISATA',
			'edit' : 'true',
			'carousel': carousel,
			'breadcrumb': 'Destinasi',})

@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def admin_index_running_link(request):
	if request.method == 'GET':
		running_link = RunningLink.objects.filter(deleted_at__isnull=True).order_by('-created_at')
		archives = RunningLink.objects.filter(deleted_at__isnull=False).order_by('-created_at')
		form = RunningLinkForm()
		form_foto = RunningLinkFotoForm()
		context = {
			'title' : 'Partner Kami - Admin',
			'running_links' : running_link,
			'archives' : archives,
			'breadcrumb' : 'Partner Kami',
			'form': form,
			'form_foto': form_foto,
		}
		
		return render(request, 'profile/admin/setting/index.html', context)

	if request.method == 'POST':
		form = CarouselForm(data=request.POST)
		form_foto = CarouselFotoForm()
		if form.is_valid():
			form.save()
			messages.success(request, 'Carousel berhasil disimpan.')
			return redirect('profile:admin_setting_carousel',)
		messages.error(request, 'Carousel gagal disimpan.')
		return redirect('profile:admin_setting_carousel',)

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def admin_index_pages(request):
	if request.method == 'GET':
		page = Halaman.objects.all().order_by('-created_at')
		archives = Halaman.objects.filter(deleted_at__isnull=False).order_by('-created_at')
		context = {
			'title' : 'Halaman - Admin',
			'halamans' : page,
			'archives' : archives,
			'breadcrumb' : 'Halaman',
		}
		
		return render(request, 'profile/admin/setting/index.html', context)

	if request.method == 'POST':
		form = HalamanForm(data=request.POST)
		if form.is_valid():
			form.save()
			messages.success(request, 'Running Text berhasil disimpan.')
			return redirect('profile:admin_setting_running_text',)
		messages.error(request, 'Running Text gagal disimpan.')
		return redirect('profile:admin_setting_running_text',)

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
@require_http_methods(["POST"])
def admin_index_infografis_edit(request):
	if request.method == 'POST':
		running_text = RunningText.objects.get(id = request.POST.get('id'))
		form = RunningTextForm(data=request.POST, instance=running_text)
		if form.is_valid():
			form.save()
			messages.success(request, 'Running Text berhasil disimpan.')
			return redirect('profile:admin_setting_running_text',)
		messages.error(request, 'Running Text gagal disimpan.')
		return redirect('profile:admin_setting_running_text',)


@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def create_carousel(request, jenis):
	template = ''
	context = {} 
	form = ''
	form_foto = ''

	if request.method == 'GET':
		form = CarouselForm()
		form_foto = CarouselFotoForm()
		context = {
			'title' : 'ADD Carousel',
			'form' : form,
			'form_foto' : form_foto,
			'breadcrumb' : 'Carousel',
			'jenis' : jenis,
		}

		template = 'profile/admin/setting/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		form = CarouselForm(data=request.POST)
		# ## JENIS VIDEO =================================================================
		if jenis == 'video':
			if form.is_valid():
				new_dt = form.save(commit=False)
				new_dt.video = request.FILES['video']
				new_dt.kondisi = jenis
				new_dt.save()
				form.save_m2m()

				# ## MEMBUAT THUMBNAIL DARI VIDEO ====================================
				dt_caro = Carousel.objects.get(id=new_dt.id)
				inf = settings.MEDIA_ROOT+str(dt_caro.video)
				nmfil = 'profile/images/carousel/video/thumb_'+str(new_dt.id)+'.jpg'
				outf = settings.MEDIA_ROOT+str(nmfil)
				# ## SIMPAN NAMA FILE THUMB ====================================
				dt_caro.foto = nmfil
				dt_caro.save()
				# ## ARSIPKAN SEMUA VIDEO KCUALI YG BARU ====================================
				with connections['default'].cursor() as cursor:
					cursor.execute("""UPDATE rizq_sanjaya_app_carousel SET deleted_at = %s
						WHERE kondisi = %s AND id != %s""",['now()', jenis, new_dt.id])

				ff = FFmpeg()
				try:
					ff.convert(inf, outf)
				except:
					messages.success(request, 'Carousel berhasil disimpan.')
					return redirect('profile:admin_setting_carousel')

				messages.success(request, 'Carousel berhasil disimpan.')
				return redirect('profile:admin_setting_carousel')

		elif jenis == 'slide':
			form_foto = CarouselFotoForm(data=request.POST, files=request.FILES)
			if form.is_valid() and form_foto.is_valid():
				new_dt = form.save(commit=False)
				new_dt.kondisi = jenis
				new_dt.save()
				form.save_m2m()
				
				dtx = Carousel.objects.get(id = new_dt.id)
				thumb_dtx = CarouselFotoForm(data=request.POST, files=request.FILES, instance=dtx)
				thumb_dtx.save()

				messages.success(request, 'Carousel berhasil disimpan.')
				return redirect('profile:admin_setting_carousel')

		messages.error(request, 'Carousel gagal disimpan.')
		return redirect('profile:admin_setting_create_carousel', jenis=jenis)

@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def create_destinasi(request):
	template = ''
	context = {} 
	form = ''
	form_foto = ''

	if request.method == 'GET':
		paket = Carousel.objects.all()
		form = Destinasi.objects.all()
		# foto = 5
		# for x in range(foto):
		#     print(x)
		context = {
			'title' : 'ADD Destinasi Wisata',
			'form' : form,
			'paket' : paket,
			'breadcrumb' : 'Destinasi',
		}

		template = 'profile/admin/setting/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		form = Destinasi.objects.all()
		judul = request.POST.get('nama')
		keterangan = request.POST.get('keterangan')
		fasilitas = request.POST.get('fasilitas')
		paket = request.POST.get('popular')
		foto = request.FILES.getlist('foto')
		paket_id = Carousel.objects.get(id=paket)

		if judul is not None:
			destinasi = Destinasi(nama=judul, paket=paket_id, deskripsi=keterangan, fasilitas=fasilitas)
			destinasi.save()

			for x in foto:
				destinasi_id = get_object_or_404(Destinasi, id = destinasi.id)
				print(destinasi_id)
				img = DestinasiImage()
				img.destinasi = destinasi_id
				img.images = x
				img.save()
				
				# destinasi_image = DestinasiImage.objects.create(images=img, destinasi=desti_id)
				# destinasi_image.save()
				
	
			messages.success(request, 'Destinasi Wisata berhasil disimpan.')
			return redirect('profile:admin_setting_destinasi',)

		messages.error(request, 'Destinasi Wisata gagal disimpan.')
		return render(request, 'profile/admin/setting/create.html', {'title' : 'ADD Paket Wisata','form' : form,'form_foto' : form_foto, 'breadcrumb' : 'Destinasi',})

@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def create_running_link(request):
	template = ''
	context = {} 
	form = ''
	form_foto = ''

	if request.method == 'GET':
		form = RunningLinkForm()
		form_foto = RunningLinkFotoForm()
		context = {
			'title' : 'ADD Partner Kami',
			'form' : form,
			'form_foto' : form_foto,
			'breadcrumb' : 'Partner Kami',
		}

		template = 'profile/admin/setting/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		logo = request.FILES.get('foto')
		form = RunningLink()

		is_valid = True

		if is_valid:
			form.logo = logo
			form.save()

			messages.success(request, 'Running berhasil disimpan.')
			return redirect('profile:admin_setting_running_link',)

		messages.error(request, 'Running gagal disimpan.')
		return render(request, 'profile/admin/setting/create.html', {'title' : 'ADD Running','form' : form,'form_foto' : form_foto, 'breadcrumb' : 'Running',})

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def create_pages(request):
	template = ''
	context = {} 
	form = ''
	form_foto = ''

	if request.method == 'GET':
		form = HalamanForm()
		context = {
			'title' : 'ADD Halaman',
			'form' : form,
			'breadcrumb' : 'Halaman',
		}

		template = 'profile/admin/setting/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		form = HalamanForm(data=request.POST)

		if form.is_valid():
			pages = form.save(commit=False)
			pages.save()
			messages.success(request, 'Halaman berhasil disimpan.')
			return redirect('profile:admin_setting_pages',)

		messages.error(request, 'Halaman gagal disimpan.')
		return render(request, 'profile/admin/setting/create.html', {'title' : 'ADD Halaman', 'form' : form, 'breadcrumb' : 'Halaman',})

@require_http_methods(["GET"])
def detail_pages(request, slug):
	template = 'profile/admin/setting/detail_halaman.html'
	try:
		halaman = Halaman.objects.get(slug=slug)
	except Halaman.DoesNotExist:
		halaman = None
	context = {
		'title' : halaman.nama if halaman != None else 'TIDAK DITEMUKAN',
		'halaman' : halaman,
	}
	return render(request, template, context)

@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def edit_carousel(request, id):
	template = ''
	context = {} 
	form = ''
	
	if request.method == 'GET':
		try:
			carousel = Carousel.objects.get(id=id)
			form = CarouselForm(instance=carousel)
			form_foto = CarouselFotoForm()
		except:
			carousel = None
			form = CarouselForm()
			form_foto = CarouselFotoForm()

		context = {
			'title' : 'EDIT Carousel',
			'form' : form,
			'form_foto' : form_foto,
			'edit' : 'true',
			'carousel': carousel,
			'jenis' : carousel.kondisi,
			'breadcrumb': 'Carousel',
		}
		template = 'profile/admin/setting/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		print(request.POST)
		print(request.FILES)

# 'if_jenis': ['video'], 'jenis': ['video'], 'foto': [''], 'x': [''], 'y': [''], 'width': [''], 'height': [''], 'video': [''], 
# 'if_foto': ['False'], 'judul': ['Banner Video'], 'subjudul': ['Banner Video Tanpa']}>
# <MultiValueDict: {}>

# 'if_jenis': ['slide'], 'jenis': ['slide'], 'foto': [''], 'x': [''], 'y': [''], 'width': [''], 'height': [''], 'video': [''], 
# 'if_foto': ['True'], 'judul': ['Selamat Datang'], 'subjudul': ['Di Website Resmi Dinas Pendidikan Provinsi Papua Tengah']}>
# <MultiValueDict: {}>

		jenis = request.POST.get('jenis')
		dtxs = Carousel.objects.get(id=id)

		if jenis == 'video':
			video_lama = f"{settings.MEDIA_ROOT}/{dtxs.video}"
			form = CarouselForm(data=request.POST, instance=dtxs)
			if form.is_valid():
				if bool(request.FILES):
					if bool(dtxs.video) : 
						try:
							os.remove(video_lama)
						except:
							pass

					new_dt = form.save(commit=False)
					new_dt.video = request.FILES['video']
					new_dt.kondisi = jenis
					new_dt.save()
					form.save_m2m()

					# ## MEMBUAT THUMBNAIL DARI VIDEO ====================================
					dt_caro = Carousel.objects.get(id=new_dt.id)
					inf = settings.MEDIA_ROOT+str(dt_caro.video)
					nmfil = 'profile/images/carousel/video/thumb_'+str(new_dt.id)+'.jpg'
					outf = settings.MEDIA_ROOT+str(nmfil)
					# ## SIMPAN NAMA FILE THUMB ====================================
					dt_caro.foto = nmfil
					dt_caro.save()

					ff = FFmpeg()
					try:
						ff.convert(inf, outf)
					except:
						messages.success(request, 'Carousel berhasil disimpan.')
						return redirect('profile:admin_setting_carousel')

					messages.success(request, 'Carousel berhasil disimpan.')
					return redirect('profile:admin_setting_carousel')

		if jenis == 'slide':
			foto_lama = f"{settings.MEDIA_ROOT}/{dtxs.foto}"
			if bool(request.FILES):
				form = CarouselForm(data=request.POST, files=request.FILES, instance=dtxs)
				form_foto = CarouselFotoForm(data=request.POST, files=request.FILES, instance=dtxs)
				is_valid = form.is_valid() and form_foto.is_valid()
			else:
				form = CarouselForm(data=request.POST, instance=dtxs)
				is_valid = form.is_valid()

			if is_valid:
				new_dtx = form.save(commit=False)
				new_dtx.judul = form.cleaned_data.get('judul')
				new_dtx.subjudul = form.cleaned_data.get('subjudul')
				new_dtx.save()
				form.save_m2m()

				if len(request.FILES) > 0:
					if bool(dtxs.foto) : 
						try:
							os.remove(foto_lama)
						except:
							pass
					form_foto.save()
				
				messages.success(request, 'Carousel berhasil disimpan.')
				return redirect('profile:admin_setting_carousel')

		# foto_old = bool(carousel.foto)
		# judul = request.POST.get('judul')
		# subjudul = request.POST.get('subjudul')
		# keterangan = request.POST.get('keterangan')
		# popular = request.POST.get('popular')
		# harga = request.POST.get('fix_harga')
		# diskon = request.POST.get('diskon', 0)
		# diskon = int(0 if diskon == '' else diskon)
		# fix_diskon = request.POST.get('fixdiskon', 0)
		# fix_diskon = int(0 if fix_diskon == '' else fix_diskon)

		# print(diskon)
		# print(fix_diskon)

		# if foto_old : 
		# 	path_file_lama = f"{settings.MEDIA_ROOT}/{carousel.foto}"

		# if len(request.FILES) > 0:
		# 	form = CarouselForm(data=request.POST, instance=carousel)
		# 	is_valid = form.is_valid()
		# else:
		# 	form = CarouselForm(data=request.POST, instance=carousel)
		# 	is_valid = form.is_valid()

		# is_valid = True
		# if is_valid:
		# 	new_carousel = form.save(commit=False)
		# 	if foto_old : 
		# 		if len(request.FILES) > 0:
		# 			try:
		# 				os.remove(path_file_lama)
		# 			except:
		# 				pass
		# 	carousel.judul = judul
		# 	carousel.subjudul = subjudul
		# 	# carousel.link = form.cleaned_data.get('link')
		# 	carousel.keterangan = keterangan
		# 	carousel.kondisi = popular
		# 	carousel.harga = harga
		# 	carousel.persen_diskon = diskon
		# 	# carousel.diskon = fix_diskon
		# 	if len(request.FILES) > 0:
		# 		carousel.foto = request.FILES['foto']
		# 	carousel.save()
			

		messages.error(request, 'Carousel gagal disimpan.')
		return redirect('profile:admin_setting_edit_carousel', id=id)

@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def edit_running_link(request, id):
	template = ''
	context = {} 
	form = ''
	
	if request.method == 'GET':
		try:
			running_link =RunningLink.objects.get(id=id)
			form =RunningLinkForm(instance=running_link)
			form_foto =RunningLinkFotoForm()
		except:
			running_link = None
			form = RunningLinkForm()
			form_foto = RunningLinkFotoForm()
		
		context = {
			'title' : 'EDIT Partner Kami',
			'form' : form,
			'form_foto' : form_foto,
			'edit' : 'true',
			'running_link': running_link,
			'breadcrumb': 'Partner Kami',
		}
		template = 'profile/admin/setting/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		running_link = RunningLink.objects.get(id=id)
		foto_old = bool(running_link.logo)

		if foto_old : 
			path_file_lama = f"{settings.MEDIA_ROOT}/{running_link.logo}"

		is_valid = True

		if is_valid:
			if foto_old : 
				if len(request.FILES) > 0:
					try:
						os.remove(path_file_lama)
					except:
						pass
			running_link.logo = request.FILES.get('foto')
			running_link.save()

			messages.success(request, 'Partner Kami berhasil disimpan.')
			return redirect('profile:admin_setting_running_link')

		messages.error(request, 'Partner Kami gagal disimpan.')
		return render(request, 'profile/admin/setting/create.html', {
			'title' : 'EDIT Partner Kami',
			'form' : form,
			'form_foto' : form_foto,
			'edit' : 'true',
			'running_link': running_link,
			'breadcrumb': 'Partner Kami',})

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def edit_pages(request, slug):
	template = ''
	context = {} 
	form = ''
	
	if request.method == 'GET':
		try:
			halaman = Halaman.objects.get(slug=slug)
			form = HalamanForm(instance=halaman)
		except:
			halaman = None
			form = HalamanForm()
			
		context = {
			'title' : 'EDIT Halaman',
			'form' : form,
			'edit' : 'true',
			'halaman': halaman,
			'breadcrumb': 'Halaman',
		}
		template = 'profile/admin/setting/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		halaman = Halaman.objects.get(slug=slug)
		form = HalamanForm(data=request.POST, instance=halaman)

		if form.is_valid():
			new_halaman = form.save(commit=False)
			new_halaman.nama = form.cleaned_data.get('nama')
			new_halaman.keterangan = form.cleaned_data.get('keterangan')
			new_halaman.save()
			messages.success(request, 'Halaman berhasil disimpan.')
			return redirect('profile:admin_setting_pages')

		messages.error(request, 'Halaman gagal disimpan.')
		return render(request, 'profile/admin/setting/create.html', {
			'title' : 'EDIT Halaman',
			'form' : form,
			'edit' : 'true',
			'halaman': halaman,
			'breadcrumb': 'Halaman',})

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_detail(request, slug):
	template = 'profile/admin/agenda/detail.html'
	try:
		agenda = Agenda.objects.get(slug=slug)
	except Agenda.DoesNotExist:
		agenda = None
	context = {
		'title' : agenda.name if agenda != None else 'TIDAK DITEMUKAN',
		'agenda' : agenda,
	}
	return render(request, template, context)

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def softDelete(request, jenis, id):
	message = ''
	sekarang = timezone.now()

	if jenis == 'running_text':
		try:
			running_text = RunningText.objects.get(id=id)
			running_text.deleted_at = sekarang
			running_text.save()
			message = 'success'
		except RunningText.DoesNotExist:
			message = 'error'
	if jenis == 'carousel':
		try:
			carousel = Carousel.objects.get(id=id)
			carousel.deleted_at = sekarang
			carousel.save()
			message = 'success'
		except Carousel.DoesNotExist:
			message = 'error'
	if jenis == 'link':
		try:
			link = Link.objects.get(id=id)
			link.deleted_at = sekarang
			link.save()
			message = 'success'
		except Link.DoesNotExist:
			message = 'error'
	if jenis == 'destinasi':
		try:
			pages = Destinasi.objects.get(id=id)
			pages.deleted_at = sekarang
			pages.save()
			message = 'success'
		except Destinasi.DoesNotExist:
			message = 'error'
	if jenis == 'running_link':
		try:
			running_link = RunningLink.objects.get(id=id)
			running_link.deleted_at = sekarang
			running_link.save()
			message = 'success'
		except RunningLink.DoesNotExist:
			message = 'error'

	context = {
			'message' : message,
		}

	return JsonResponse(context)

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def permanentDelete(request, jenis, id):
	message = ''

	if jenis == 'running_text':
		try:
			running_text = RunningText.objects.get(id=id)
			running_text.delete()
			message = 'success'
		except RunningText.DoesNotExist:
			message = 'error'
	if jenis == 'carousel':
		try:
			carousel = Carousel.objects.get(id=id)
			try:
				if carousel.foto != None: carousel.foto.delete()
			except:
				pass
			try:
				if carousel.video != None: carousel.video.delete()
			except:
				pass
			carousel.delete()
			message = 'success'
		except Carousel.DoesNotExist:
			message = 'error'
	if jenis == 'running_link':
		try:
			running_link = RunningLink.objects.get(id=id)
			try:
				if running_link.logo != None: running_link.logo.delete()
			except:
				pass
			running_link.delete()
			message = 'success'
		except RunningLink.DoesNotExist:
			message = 'error'
	if jenis == 'link':
		try:
			link = Link.objects.get(id=id)
			link.delete()
			message = 'success'
		except Link.DoesNotExist:
			message = 'error'
	if jenis == 'destinasi':
		try:
			pages = Destinasi.objects.get(id=id)
			pages.delete()
			message = 'success'
		except Destinasi.DoesNotExist:
			message = 'error'
	if jenis == 'category':
		try:
			category = Category.objects.get(id=id)
			category.delete()
			message = 'success'
		except Category.DoesNotExist:
			message = 'error'
		except IntegrityError:
			message = 'integrity error'
		except:
			message = 'error'
	context = {
			'message' : message,
		}
	return JsonResponse(context)
	
@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def restore(request, jenis, id):
	message = ''

	if jenis == 'running_text':
		try:
			running_text = RunningText.objects.get(id=id)
			running_text.deleted_at = None
			running_text.save()
			message = 'success'
		except RunningText.DoesNotExist:
			message = 'error'
	if jenis == 'carousel':
		try:
			carousel = Carousel.objects.get(id=id)
			carousel.deleted_at = None
			carousel.save()
			message = 'success'
		except Carousel.DoesNotExist:
			message = 'error'
	if jenis == 'link':
		try:
			link = Link.objects.get(id=id)
			link.deleted_at = None
			link.save()
			message = 'success'
		except Link.DoesNotExist:
			message = 'error'
	if jenis == 'destinasi':
		try:
			pages = Destinasi.objects.get(id=id)
			pages.deleted_at = None
			pages.save()
			message = 'success'
		except Destinasi.DoesNotExist:
			message = 'error'
	if jenis == 'running_link':
		try:
			running_link = RunningLink.objects.get(id=id)
			running_link.deleted_at = None
			running_link.save()
			message = 'success'
		except Halaman.DoesNotExist:
			message = 'error'

	context = {
			'message' : message,
		}

	return HttpResponse(context)

@require_http_methods(["GET"])
def get_agenda(request):
	agendas = Agenda.objects.filter(deleted_at=None).order_by('-created_at')
	agenda_json = []
	
	for agenda in agendas:
		mulai = str(agenda.start_date).split('-')
		mulai_agenda = f"{mulai[1]}/{mulai[2]}/{mulai[0]}"
		mulai_badge = f"{mulai[2]}/{mulai[1]}"
		if agenda.finish_date:
			selesai = str(agenda.finish_date).split('-')
			selesai_agenda = f"{selesai[1]}/{selesai[2]}/{selesai[0]}"
			selesai_badge = f"{selesai[2]}/{selesai[1]}"
		badge = 'No Time'
		if agenda.start_time:
			badge = f"{str(agenda.start_time)[:5]} - {str(agenda.finish_time)[:5]}" if agenda.finish_time else str(str(agenda.start_time)[:5])
		dict = { 
				'id': agenda.slug,
				'badge': badge,
				'name': agenda.name,
				'date':  [mulai_agenda, selesai_agenda] if agenda.finish_date else str(mulai_agenda),
				'description': agenda.description,
				'everyYear': agenda.every_year,
				'color' : agenda.color
		}
		agenda_json.append(dict)
	return HttpResponse(json.dumps({'agenda': agenda_json}))


@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def admin_index_aboutme(request, section):
	if request.method == 'GET':
		try:
			headAbout = AboutMe.objects.get(section=section, jenis='header')
		except Exception as e:
			headAbout = ''
			messages.success(request, 'Data Header belum ada, silahkan diisi.')
		
		listAbout = AboutMe.objects.filter(section=section, jenis='child').order_by('id')

		context = {
			'title' : section.title()+' - Admin',
			'headAbout' : headAbout,
			'listAbout' : listAbout,
			'breadcrumb' : section.title(),
			'section': section,
		}
		
		return render(request, 'profile/admin/setting/index.html', context)

	if request.method == 'POST':
		youtube = request.POST.get('keterangan')
		images = request.FILES.get('images')
		print(images)
		try:
			### JIKA SUDAH ADA == EDIT
			abooot = AboutMe.objects.get(section=section, jenis='header')
			abooot.judul = request.POST.get('judul')
			abooot.deskripsi = request.POST.get('deskripsi')
			abooot.youtube = youtube            
			if 'images' in request.FILES:
				abooot.images = images
				abooot.save()
			abooot.save()

			messages.success(request, 'Data berhasil disimpan.')
			return redirect('profile:admin_setting_aboutme', section=section)

		except Exception as e:
			### JIKA BELUM ADA == ADD
			form = AboutMeForm(data=request.POST)
			if form.is_valid():
				form.save()

				messages.success(request, 'Data berhasil disimpan.')
				return redirect('profile:admin_setting_aboutme', section=section)

		messages.error(request, 'Data gagal disimpan.')
		return redirect('profile:admin_setting_aboutme', section=section)


@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def create_aboutme(request, section):
	template = ''
	context = {} 
	form = '' 

	### SPLIT DATA ICON PER LIMIT ===========================================================
	# fonticon = read_icon(section)
	# limit = 3
	# total = len(fonticon)
	# hasil = []

	# for i in range(0,total,limit):
	#     hasil.append(fonticon[i:i+limit])

	if request.method == 'GET':
		form = AboutMeForm()
		context = {
			'title' : 'ADD '+section.title(),
			'form' : form,
			'breadcrumb' : section.title(),
			'section' : section,
			# 'dticon' : hasil,
		}

		template = 'profile/admin/setting/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		form = AboutMeForm(data=request.POST)
		if form.is_valid():
			AboutMe_add = form.save(commit=False)
			AboutMe_add.save()

			messages.success(request, 'Data berhasil disimpan.')
			return redirect('profile:admin_setting_aboutme', section=section)

		context = {
			'title' : 'ADD '+section.title(),
			'form' : form,
			'breadcrumb' : section.title(),
			'section' : section,
			# 'dticon' : hasil,
		}

		messages.error(request, 'Data gagal disimpan.')
		return render(request, 'profile/admin/setting/create.html', context)


@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def edit_aboutme(request, section, id):
	template = ''
	context = {} 
	form = ''

	### SPLIT DATA ICON PER LIMIT ===========================================================
	# fonticon = read_icon(section)
	# limit = 3
	# total = len(fonticon)
	# hasil = []

	# for i in range(0,total,limit):
	#     hasil.append(fonticon[i:i+limit])
	
	if request.method == 'GET':
		try:
			AboutMe_get = AboutMe.objects.get(id=id)
			form = AboutMeForm(instance=AboutMe_get)
		except:
			AboutMe_get = None
			form = AboutMeForm()
		
		context = {
			'title' : 'ADD '+section.title(),
			'form' : form,
			'breadcrumb' : section.title(),
			'section' : section,
			# 'dticon' : hasil,
			'edit' : 'true',
			'aidi' : id,
			'dataAbout' : AboutMe_get,
		}
		template = 'profile/admin/setting/create.html'
		return render(request, template, context)
	
	if request.method == 'POST':
		AboutMe_get = AboutMe.objects.get(id=id)
		form = AboutMeForm(data=request.POST, instance=AboutMe_get)

		if form.is_valid():
			AboutMe_update = form.save(commit=False)
			AboutMe_update.judul = form.cleaned_data.get('judul')
			AboutMe_update.deskripsi = form.cleaned_data.get('deskripsi')
			# AboutMe_update.icon = form.cleaned_data.get('icon')
			AboutMe_update.save()

			messages.success(request, 'Data berhasil disimpan.')
			return redirect('profile:admin_setting_aboutme', section=section)

		context = {
			'title' : 'ADD '+section.title(),
			'form' : form,
			'breadcrumb' : section.title(),
			'section' : section,
			# 'dticon' : hasil,
			'edit' : 'true',
			'aidi' : id,
			'dataAbout' : AboutMe_get,
		}

		messages.error(request, 'Data gagal disimpan.')
		return render(request, 'profile/admin/setting/create.html', context)

@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def delete_aboutme(request, section, id):
	message = ''

	try:
		AboutMe_del = AboutMe.objects.get(id=id)
		AboutMe_del.delete()
		message = 'success'
	except AboutMe.DoesNotExist:
		message = 'error'

	context = { 'message' : message, }
	return JsonResponse(context)