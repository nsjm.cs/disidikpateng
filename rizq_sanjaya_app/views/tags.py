from django.shortcuts import render, HttpResponse, HttpResponseRedirect, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News
from datetime import datetime
import os
from django.conf import settings
from ..decorators import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..helpers import querytags

# JUMLAH DATA PER PAGE ===> PAGINATIONS
jml_data = 12

@require_http_methods(["GET"])
def index(request, jenis, slug):
    news = []
    is_news = True

    if jenis == 'ppid':
        news = modul_PPID.objects.filter(tags__slug__in=[slug], deleted_at=None).order_by('-created_at')
        is_news = False
    elif jenis == 'news':
        news = News.objects.filter(tags__slug__in=[slug], deleted_at=None).order_by('-created_at')
        is_news = True

    page = request.GET.get('page', 1)
    paginator = Paginator(news, jml_data)

    try:
    	news = paginator.page(page)
    except PageNotAnInteger:
    	news = paginator.page(1)
    except EmptyPage:
    	news = paginator.page(paginator.num_pages)

    for x in news:
        if is_news:
            if os.path.isfile(settings.MEDIA_ROOT+str(x.thumbnail)):
                x.url_img = x.thumbnail.url
            else:
                x.url_img = settings.STATIC_URL+'profile/images/front/no_images.gif'
        # else:
        #     if x.foto:
        #         if os.path.isfile(settings.MEDIA_ROOT+str(x.foto)):
        #             x.url_img = x.foto.url
        #         else:
        #             x.url_img = settings.STATIC_URL+'profile/images/front/no_images.gif'
        #     else:
        #         thumbnail = x.video.split('/embed/')
        #         x.url_img = 'https://i1.ytimg.com/vi/'+str(thumbnail[1])+'/mqdefault.jpg'

    favorits = News.objects.filter(deleted_at=None).order_by('-seen')[:5]

    context = {
        'title' : f'Tags #{slug.title()}',
        'jenis' : jenis,
        'is_news' : is_news,
        'newss' : news,
        'favs' : favorits,
        'dt_tags': querytags(jenis),
    }
    return render(request, 'profile/tags/index.html', context)