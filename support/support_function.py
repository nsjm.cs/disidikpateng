from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.conf import settings
import time, math, random, string, requests
from django.conf import settings
from datetime import datetime, timedelta
import re, math, os


def admin_only():
    def decorator(func):
        def wrap(request, redirect_to = 'profile:admin_home', *args, **kwargs):
            if request.user.role == 'admin':
                return func(request, *args, **kwargs)
            else:
                messages.error(request, "Anda tidak diperbolehkan mengakses halaman ini.")
                return redirect(reverse(redirect_to))
        return wrap
    return decorator
    
def check_is_email(email):
    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}\b'
    # pass the regular expression
    # and the string into the fullmatch() method
    if(re.fullmatch(regex, email)):
        return True
    else:
        return False


def send_email_otp(recepient, token):
    msg = render_to_string('email_template/email_token_template.html', {'token':token})
    email = EmailMultiAlternatives(f'[OTP] Kode Verifikasi', msg, f'{settings.APP_TITLE_} <{settings.EMAIL_SENDER_ALIAS}>',recepient)
    email.content_subtype = "html"
    return email.send()

def send_email_pesanan_client(recepient):
    msg = render_to_string('email_template/email_pesanan_client.html', {'token':''})
    email = EmailMultiAlternatives(f'[INFORMASI] Detail Pesanan MerapiTrip', msg, f'{settings.APP_TITLE_} <{settings.EMAIL_SENDER_ALIAS}>',recepient)
    email.content_subtype = "html"
    return email.send()

def send_email_konfirmasi_dp(recepient, email_pesan, dp_pesan):
    msg = render_to_string('email_template/email_konf_dp.html', {'paket': email_pesan , 'dp_pesan': dp_pesan})
    email = EmailMultiAlternatives(
        subject=f'[INFORMASI] Detail Pesanan MerapiTrip',
        body=msg,
        from_email=f'{settings.APP_TITLE_} <{settings.EMAIL_SENDER_ALIAS}>',
        to=recepient,
        cc=['merapitrip@gmail.com']
    )
    email.content_subtype = "html"
    return email.send()

def send_email_pengaduan(recepient, catatan):
    msg = render_to_string('email_template/email_pengaduan.html', {'catatan': catatan})
    email = EmailMultiAlternatives(f'[INFORMASI] Pengaduan Dinas Pendidikan Papua Tengah', msg, f'{settings.APP_TITLE_} <{settings.EMAIL_SENDER_ALIAS}>',recepient)
    email.content_subtype = "html"
    return email.send()

def send_email_pesanan_admin(recepient):
    msg = render_to_string('email_template/email_pesanan_admin.html', {'token':''})
    email = EmailMultiAlternatives(f'[INFORMASI] Pesanan Baru MerapiTrip', msg, f'{settings.APP_TITLE_} <{settings.EMAIL_SENDER_ALIAS}>',recepient)
    email.content_subtype = "html"
    return email.send()

def send_email_pembayaran_dp(recepient):
    msg = render_to_string('email_template/email_permintaan_pembayaran_dp.html', {'token':''})
    email = EmailMultiAlternatives(f'[INFORMASI] Pembayaran DownPayment', msg, f'{settings.APP_TITLE_} <{settings.EMAIL_SENDER_ALIAS}>',recepient)
    email.content_subtype = "html"
    return email.send()
